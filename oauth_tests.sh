echo "     Should fail"
curl http://localhost:9000/api/1/oauth/token; echo

echo "     Should fail"
curl "http://localhost:9000/api/1/oauth/token?client_id=myConsumerKey&username=admin@syages.fr&password=root&grant_type=aaa"; echo

echo "         Should deliver token"
curl "http://localhost:9000/api/1/oauth/token?client_id=myConsumerKey&username=admin@syages.fr&password=root&grant_type=password"; echo

echo "     Should fail"
curl http://localhost:9000/api/1/oauth/check; echo

echo "     Should fail"
curl -H "Authorization: Bearer tokenInexistant" http://localhost:9000/api/1/oauth/check; echo

echo "         Should deliver empty JSON"
curl -H "Authorization: Bearer token0123456789abcdef" http://localhost:9000/api/1/oauth/check; echo