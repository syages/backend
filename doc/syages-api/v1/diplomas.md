# Diplomas

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /diplomas](#get-diplomas) | Retourne une liste de formation | ![][ok] ![][sec] | 
| [GET /diplomas/:diploma](#get-diplomasdiploma) | Retourne une formation par son `id` | ![][ok] ![][sec] |
| [POST /diplomas](#post-diplomas) | Créer une formation | ![][ok] ![][sec] |
| [PUT /diplomas/:diploma](#put-diplomasdiploma) | Éditer une formation | ![][ok] ![][sec] |
| [DELETE /diplomas/:diploma](#delete-diplomasdiploma) | Supprimer une formation | ![][ok] ![][sec] |
| [PATCH /diplomas/:diploma/status](#patch-diplomasdiplomastatus) | Changer le statut d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/interns](#get-diplomasdiplomainterns) | Récupérer la liste des stagiaire d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/lessons](#get-diplomasdiplomalessons) | Récupérer la liste des leçons d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/periods](#get-diplomasdiplomaperiods) | Récupérer la liste des périodes d'une formation | ![][ok] ![][sec] |



## `GET /diplomas`

Retourne une liste de formation.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/diplomas" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
   {
    "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
    "name": "DAEU-A 2015",
    "supervisor": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    },
    "status": "Active",
    "endYear": 2015,
    "startYear": 2014
  },
  ...
]
```

## `GET /diplomas/:diploma`

Retourne une formation par son `id`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/diplomas/5616bce7-a0ac-4c12-b590-f8f2a804b95e" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": "5616bce7-a0ac-4c12-b590-f8f2a804b95e",
  "version": 1,
  "name": "DAEU 2015",
  "passMark": 10,
  "status": "Active",
  "supervisor": {
    "user": {
      "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
    }
  },
  "lessons": [
    {
      "id": "4f89f5d2-4de9-42ed-abdc-e229fbc7298e",
      "version": 1,
      "course": {
        "id": "8bb9af26-6d05-4a57-ab07-a693f4655877",
        "name": "Français"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 1.0,
      "optional": false,
      "passMark": 10.0
    },
    {
      "id": "c7f72d41-d1d4-4e72-beda-f263d01dfd95",
      "version": 1,
      "course": {
        "id": "c57441d4-97e3-44a8-a782-0973b6d132f5",
        "name": "Mathématiques"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 2.5,
      "optional": true,
      "passMark": 10.0
    }
  ],
  "periods": [
    {
      "id": "4c60e47e-3deb-498f-8848-1815b6903d0d",
      "version": 1,
      "startDate": 1410472800000,
      "endDate": 1418338800000,
      "ordre": 1
    },
    {
      "id": "4c46c942-54eb-4c8b-a1ba-937312415b1b",
      "version": 1,
      "startDate": 1418425200000,
      "endDate": 1426114800000,
      "ordre": 2
    },
    {
      "id": "f73b0b70-fcff-4e84-b722-e759dd5dd750",
      "version": 1,
      "startDate": 1426201200000,
      "endDate": 1434060000000,
      "ordre": 3
    }
  ]
}
```

## `POST /diplomas`

Crée une nouvelle formation.

**Autorisation:**
- Administrator
- President
- StaffUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| name | `String` | `requis` | Nom du dipl&ocirc;me |
| passMark | `Float` | `requis` | Moyenne minimale |
| supervisor | `TeacherUser` | `requis` | Enseignant superviseur de la formation |
| lessons | `List<Lesson>` | `requis` | Une liste d'objets <a href="lessons.md">Lesson</a> |
| periods | `List<Period>` | `requis` | Une liste d'objets <a href="periods.md">Period</a> |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/diplomas" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "name": "DAEU 2015",
"passMark": 10,
  "periods": [
    {
  "ordre": 1,
      "startDate": "2014-09-12",
      "endDate": "2014-12-12"
    },
    {
  "ordre": 2,
    "startDate": "2014-12-13",
    "endDate": "2015-03-12"
      
    },
    {
  "ordre": 3,
     "startDate": "2015-03-13",
      "endDate": "2015-06-12"
    }
  ],
  "lessons": [
    {
      "course": "c57441d4-97e3-44a8-a782-0973b6d132f5",
      "teacher": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
      "optional": true,
      "coefficient": 2.5,
      "passMark": 10
    },
    {
  "course": "8bb9af26-6d05-4a57-ab07-a693f4655877",
  "teacher": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
  "optional": false,
  "coefficient": 1.0,
  "passMark": 10
    }
  ],
  "supervisor": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
}'
```

### Exemple de réponse
```json
{
  "id": "5616bce7-a0ac-4c12-b590-f8f2a804b95e",
  "version": 1,
  "name": "DAEU 2015",
  "supervisor": {
    "user": {
      "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
    }
  },
  "lessons": [
    {
      "id": "c7f72d41-d1d4-4e72-beda-f263d01dfd95",
      "version": 1,
      "course": {
        "id": "c57441d4-97e3-44a8-a782-0973b6d132f5",
        "name": "Mathématiques"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 2.5,
      "optional": true,
      "passMark": 10.0
    },
    {
      "id": "4f89f5d2-4de9-42ed-abdc-e229fbc7298e",
      "version": 1,
      "course": {
        "id": "8bb9af26-6d05-4a57-ab07-a693f4655877",
        "name": "Français"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 1.0,
      "optional": false,
      "passMark": 10.0
    }
  ],
  "status": "Active",
  "periods": [
    {
      "id": "4c60e47e-3deb-498f-8848-1815b6903d0d",
      "version": 1,
      "startDate": 1410480000000,
      "endDate": 1418342400000,
      "ordre": 1
    },
    {
      "id": "4c46c942-54eb-4c8b-a1ba-937312415b1b",
      "version": 1,
      "startDate": 1418428800000,
      "endDate": 1426118400000,
      "ordre": 2
    },
    {
      "id": "f73b0b70-fcff-4e84-b722-e759dd5dd750",
      "version": 1,
      "startDate": 1426204800000,
      "endDate": 1434067200000,
      "ordre": 3
    }
  ]
}
```

## `PUT /diplomas/:diploma`

Édite une formation identifiée par `:diploma`.
*Veuillez retourner un objet Diploma entier*

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/diplomas/5616bce7-a0ac-4c12-b590-f8f2a804b95e" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "5616bce7-a0ac-4c12-b590-f8f2a804b95e",
  "version": 1,
  "name": "DAEU 2015 - Edité",
  "passMark": 10,
  "status": "Active",
  "supervisor": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
  "lessons": [
    {
      "id": "4f89f5d2-4de9-42ed-abdc-e229fbc7298e",
      "version": 1,
      "course": "8bb9af26-6d05-4a57-ab07-a693f4655877",
      "teacher": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
      "coefficient": 2.5,
      "optional": false,
      "passMark": 10.0
    },
    {
      "id": "c7f72d41-d1d4-4e72-beda-f263d01dfd95",
      "version": 1,
      "course": "c57441d4-97e3-44a8-a782-0973b6d132f5",
      "teacher": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
      "coefficient": 2.5,
      "optional": true,
      "passMark": 10.0
    }
  ],
  "periods": [
    {
      "id": "4c60e47e-3deb-498f-8848-1815b6903d0d",
      "version": 1,
      "startDate": 1410472800000,
      "endDate": 1418338800000,
      "ordre": 1
    },
    {
      "id": "4c46c942-54eb-4c8b-a1ba-937312415b1b",
      "version": 1,
      "startDate": 1418425200000,
      "endDate": 1426114800000,
      "ordre": 2
    },
    {
      "id": "f73b0b70-fcff-4e84-b722-e759dd5dd750",
      "version": 1,
      "startDate": 1426201200000,
      "endDate": 1434060000000,
      "ordre": 3
    }
  ]
}'

```

### Exemple de réponse
```json
{
  "id": "5616bce7-a0ac-4c12-b590-f8f2a804b95e",
  "version": 2,
  "name": "DAEU 2015 - Edité",
  "passMark": 10,
  "status": "Active",
  "supervisor": {
    "user": {
      "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
    }
  },
  "lessons": [
    {
      "id": "4f89f5d2-4de9-42ed-abdc-e229fbc7298e",
      "version": 2,
      "course": {
        "id": "8bb9af26-6d05-4a57-ab07-a693f4655877",
        "name": "Français"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 2.5,
      "optional": false,
      "passMark": 10.0
    },
    {
      "id": "c7f72d41-d1d4-4e72-beda-f263d01dfd95",
      "version": 2,
      "course": {
        "id": "c57441d4-97e3-44a8-a782-0973b6d132f5",
        "name": "Mathématiques"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 2.5,
      "optional": true,
      "passMark": 10.0
    }
  ],
  "periods": [
    {
      "id": "4c60e47e-3deb-498f-8848-1815b6903d0d",
      "version": 2,
      "startDate": 1410472800000,
      "endDate": 1418338800000,
      "ordre": 1
    },
    {
      "id": "4c46c942-54eb-4c8b-a1ba-937312415b1b",
      "version": 2,
      "startDate": 1418425200000,
      "endDate": 1426114800000,
      "ordre": 2
    },
    {
      "id": "f73b0b70-fcff-4e84-b722-e759dd5dd750",
      "version": 2,
      "startDate": 1426201200000,
      "endDate": 1434060000000,
      "ordre": 3
    }
  ]
}
```

## `DELETE /diplomas/:diploma`

Supprime une formation identifiée par `:diploma`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/diplomas/5616bce7-a0ac-4c12-b590-f8f2a804b95e" \
  -H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

## `PATCH /diplomas/:diploma/status`

Changer le statut d'une formation identifiée par `:diploma`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| status | `Active` or `Inactive` | `requis` | statut de la formation |

### Exemple de requ&ecirc;te
```bash
curl -X "PATCH" "http://localhost:9000/api/1/diplomas/3740758e-3862-4579-843b-38e4d51c9e5d/status" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "status": "Inactive"
}'
```

### Exemple de r&eacute;ponse
```json
{
  "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
  "version": 2,
  "name": "DAEU-A 2015",
  "supervisor": {
    "user": {
      "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
    }
  },
  "lessons": [
    {
      "id": "4617ee93-742f-49b8-b92b-007c1fae6528",
      "version": 1,
      "course": {
        "id": "32940d51-3666-41b9-8918-764254d4d2e0",
        "name": "Droit"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 10.0,
      "optional": true,
      "passMark": null
    },
    {
      "id": "50eb38b1-2dab-4203-a134-f3747075ee1e",
      "version": 1,
      "course": {
        "id": "ab4921c7-1b31-4868-b6de-70d3dff69436",
        "name": "Sciences Sociales"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0"
        }
      },
      "coefficient": 10.0,
      "optional": true,
      "passMark": null
    },
    ...
  ],
  "status": "Inactive",
  "periods": [
    {
      "id": "08b5c909-1a17-48d4-8ca8-5ec5ca4e1028",
      "version": 1,
      "startDate": 1409608800000,
      "endDate": 1417734000000,
      "ordre": 1
    },
    {
      "id": "32db28b7-9e70-48b0-a38d-7b5530e76451",
      "version": 1,
      "startDate": 1417993200000,
      "endDate": 1425596400000,
      "ordre": 2
    },
    {
      "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
      "version": 1,
      "startDate": 1425855600000,
      "endDate": 1434060000000,
      "ordre": 3
    }
  ]
}
```

## `GET /diplomas/:diploma/interns`

Retourne la liste des stagiaire suivant la formation `:diploma`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/diplomas/3740758e-3862-4579-843b-38e4d51c9e5d/interns" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "bedbebf4-a04b-4387-8ebf-acaf988e7a6b",
    "version": 1,
    "user": {
      "id": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
      "firstName": "Demo3",
      "lastName": "Stagiaire"
    }
  },
  {
    "id": "5c11d9e6-5f5b-40d4-bce7-0809324a482f",
    "version": 1,
    "user": {
      "id": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
      "firstName": "Demo2",
      "lastName": "Stagiaire"
    }
  },
  {
    "id": "522f02e2-ea82-4804-9542-53afc736e3f7",
    "version": 1,
    "user": {
      "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
      "firstName": "Demo1",
      "lastName": "Stagiaire"
    }
  },
  ...
]
```

## `GET /diplomas/:diploma/lessons`

Retourne la liste des lessons de la formation `:diploma`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/diplomas/3740758e-3862-4579-843b-38e4d51c9e5d/lessons" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "50eb38b1-2dab-4203-a134-f3747075ee1e",
    "version": 1,
    "course": {
      "id": "ab4921c7-1b31-4868-b6de-70d3dff69436",
      "name": "Sciences Sociales"
    },
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": true,
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  {
    "id": "21a9a9bf-72d1-42bc-8346-94c1dec9b5df",
    "version": 1,
    "course": {
      "id": "8ac87b84-0a5e-4251-a5e1-10da7bcfd07c",
      "name": "Philosophie"
    },
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": true,
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  {
    "id": "e942111c-15c3-4720-8ae4-fde20e4e0190",
    "version": 1,
    "course": {
      "id": "306237d2-3f49-429c-b8e2-619ad9cb1d3f",
      "name": "Géographie"
    },
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": true,
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  ...
]
```

## `GET /diplomas/:diploma/periods`

Retourne la liste des périodes de la formation `:diploma`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/diplomas/3740758e-3862-4579-843b-38e4d51c9e5d/periods" \
  -H "Authorization: <Authorization>"

```

### Exemple de réponse
```json
[
  {
    "id": "08b5c909-1a17-48d4-8ca8-5ec5ca4e1028",
    "version": 1,
    "ordre": 1,
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "endDate": 1417734000000,
    "startDate": 1409608800000
  },
  {
    "id": "32db28b7-9e70-48b0-a38d-7b5530e76451",
    "version": 1,
    "ordre": 2,
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "endDate": 1425596400000,
    "startDate": 1417993200000
  },
  {
    "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
    "version": 1,
    "ordre": 3,
    "diploma": {
      "id": "3740758e-3862-4579-843b-38e4d51c9e5d",
      "name": "DAEU-A 2015"
    },
    "endDate": 1434060000000,
    "startDate": 1425855600000
  }
]
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png