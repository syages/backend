# Notes

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /marks](#get-marks) | Récupérer une liste de notes | ![][ok] ![][sec] |
| [GET /marks/:marks](#get-marksmark) | Récupérer une note par son identifiant `:mark` | ![][ok] ![][sec] |
| [POST /marks](#post-marks) | Créer des notes pour une évaluation | ![][ok] ![][sec] |
| [PUT /marks/:mark](#put-marksmark) | Éditer une note | ![][ok] ![][sec] |
| [DELETE /marks/:mark](#delete-marksmark) | Supprimer une note | ![][ok] ![][sec] |

## `GET /marks`

Retourne une liste des notes.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/marks" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 2,
    "version": 1,
    "intern": {
      "user": {
        "id": 46,
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 16.5,
    "evaluation": {
      "id": 61,
      "name": "EvaluationTest"
    }
  },
  {
    "id": 3,
    "version": 1,
    "intern": {
      "user": {
        "id": 47,
        "firstName": "Demo3",
        "lastName": "Stagiaire"
      }
    },
    "status": "Absent",
    "mark": null,
    "evaluation": {
      "id": 61,
      "name": "EvaluationTest"
    }
  },
  ...
]
```

## `GET /marks/:marks`

Retourne la note `:mark`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/marks/1" \
	-H "Authorization: <Authorization>"
```
### Exemple de réponse
```json
{
  "id": 1,
  "version": 1,
  "intern": {
    "user": {
      "id": 45,
      "firstName": "Demo1",
      "lastName": "Stagiaire"
    }
  },
  "status": "Present",
  "mark": 12.0
}
```

## `POST /marks`

Créer une note.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| intern | `Integer` | `requis` | Stagiaire noté |
| mark | `Float` | `optionnel` | Note du stagiaire |
| status | `EvalStatus` | `requis` | Statut du stagiaire |
| evaluation | `Evaluation` | `requis` | Evaluation à laquelle est rattachée la note |

EvalStatus:
- Present ("Présent")
- Absent ("Abs")
- Excuse ("Exc") 

Répéter ce modèle autant de fois nécessaire.

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/marks" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
    "intern": 45,
    "status": "Present",
    "mark": 9,
    "evaluation": 61
  }'
```

### Exemple de réponse
```json
{
  "id": 21,
  "version": 1,
  "intern": {
    "user": {
      "id": 45,
      "firstName": "Demo1",
      "lastName": "Stagiaire"
    }
  },
  "status": "Present",
  "mark": 9.0
}
```

## `PUT /marks/:mark`

Édite une note `:note`.
*Veillez à retourner un objet Mark entier*

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/marks/1" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "id": 1,
  "version": 1,
  "intern": 45,
  "status": "Present",
  "mark": 13.0
}'
```

### Exemple de réponse
```json
{
  "id": 1,
  "version": 2,
  "intern": {
    "user": {
      "id": 45,
      "firstName": "Demo1",
      "lastName": "Stagiaire"
    }
  },
  "status": "Present",
  "mark": 13.0
}
```

## `DELETE /marks/:mark`

Supprime la note `:note`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/marks/1" \
	-H "Authorization: <Authorization>"
```

### Réponse

En cas du succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png