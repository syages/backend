# Absences dites *notées*

***

Il s'agit des absences saisies par les enseignants ou le staff du DAEU.

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /notedAbsences](#get-notedabsences) | Récupérer une liste d'absence | ![][ok] ![][sec] |
| [POST /notedAbsences](#post-notedabsences) | Créer un nouvelle absence notée | ![][ok] ![][sec] |
| [PUT /notedAbsences/:absence](#put-notedabsences) | Éditer un absence notée | ![][ok] ![][sec] |
| [DELETE /notedAbsences/:absence](#delete-notedabsences) | Supprimer une absence notée | ![][ok] ![][sec] |
| [PATCH /notedAbsences/:absence/status](#patch-notedabsencesabsencestatus) | Changer le statut d'une absence notée | ![][ok] ![][sec] |

## `GET /notedAbsences`

Retourne une liste d'absences *notées*.

**Autorisation:**
- Enseignant
- Président
- Staff administratif
- Administrateur

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/notedAbsences" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 61,
    "version": 3,
    "date": "2015-02-09",
    "timeOfDay": "Morning",
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 65
      }
    },
    "observerUser": {
      "id": 62
    }
  }
]
```

## `POST /notedAbsences`

Créer une absence *notée*.

**Autorisation:**
- Enseignant
- Président
- Staff administratif

### Éléments du Json

| Name | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| date | `Date` | `requis` | Date de l'absence |
| timeOfDay | `Morning/Afternoon`| `requis` | Moment de la journée |
| intern | `InternUser` | `requis` | Stagiaire concerné par l'absence |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/notedAbsences" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "date": "2015-02-09",
  "timeOfDay": "Morning",
  "intern": 65
}'
```
### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.


## `PUT /notedAbsences/:absence`

Éditer une absence *notée*.
*Veuillez retourner un objet NotedAbsence entier*

**Autorisation:**
- Enseignant
- Président
- Staff administratif

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/declaredAbsences/81" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
    "id": 81,
    "version": 1,
    "startDate": "2015-02-09",
    "timeOfDayStart": "Morning",
    "endDate": "2015-02-10",
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "Death",
    "commentary": null,
    "intern": {
      "user": {
        "id": 145
      }
    }
  }'
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

## `DELETE /notedAbsences/:absence`

Supprimer une absence *notée*.

**Autorisation:**
- Enseignant
- Président
- Staff administratif

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/declaredAbsences/141" \
	-H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

## `PATCH /notedAbsences/:absence/status`

Change le statut d'une absence *notée*.

**Autorisation:**
- Enseignant
- Président
- Staff administratif

### Éléments du Json
| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| status | `NotedAbsenceStatus` | `requis` | Statut de l'absence *notée* |

NotedAbsenceStatus:
- Justified
- Unjustified

### Exemple de requête
```bash
curl -X "PATCH" "http://localhost:9000/api/1/notedAbsences/61/status" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "status": "Unjustified"
}'
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png