# Informations

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /informations](#get-informations) | Récupérer une liste de messages  | ![][ok] ![][sec] |
| [GET /informations/:info](#get-informationsinfo) | Récupérer un message par son `id` | ![][ok] ![][sec] |
| [POST /informations](#post-informations) | Créer un nouveau message | ![][ok] ![][sec] |
| [PUT /informations/:info](#put-informationsinfo) | Éditer un message | ![][ok] ![][sec] |
| [DELETE /informations/:info](#delete-informationsinfo) | Supprimer un message | ![][ok] ![][sec] |

## `GET /informations`

Retourne une liste d'informations.

**Autorisation**

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/informations" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "d329e6a6-9b32-4e53-ade3-bdef14829f29",
    "version": 1,
    "author": {
      "id": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
      "firstName": "Timothée",
      "lastName": "Barbot"
    },
    "title": "Information d'inscription",
    "posted": 1424872671661
  }
]
```

## `GET /informations/:info`

Retourne une informations par son identifiant `:info`.

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/informations/d329e6a6-9b32-4e53-ade3-bdef14829f29" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": "d329e6a6-9b32-4e53-ade3-bdef14829f29",
  "version": 1,
  "author": {
    "id": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
    "firstName": "Timothée",
    "lastName": "Barbot",
    "pictureUrl": null
  },
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015",
  "posted": 1424872671661,
  "title": "Information d'inscription"
}
```

## `POST /informations`

Crée une nouvelle information.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

## Parameters

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| title  | `String` | `requis` | Titre du message |
| post   | `String` | `requis` | Contenu du message |

## Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/informations" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015",
  "title": "Information d\'inscription"
}'
```

## Exemple de réponse
```json
{
  "id": "d329e6a6-9b32-4e53-ade3-bdef14829f29",
  "version": 1,
  "author": {
    "id": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
    "firstName": "Timothée",
    "lastName": "Barbot",
    "pictureUrl": null
  },
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015",
  "posted": 1424872671661,
  "title": "Information d'inscription"
}
```

## `PUT /informations/:info`

Édite une information identifiée par `:info`

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser (uniquement celles dont il est l'auteur)


### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/informations/d329e6a6-9b32-4e53-ade3-bdef14829f29" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "d329e6a6-9b32-4e53-ade3-bdef14829f29",
  "version": 1,
  "author": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015 - Tenez vous pret !",
  "posted": 1424872671661,
  "title": "Information d\'inscription"
}'
```

### Exemple de réponse
```json
{
  "id": "d329e6a6-9b32-4e53-ade3-bdef14829f29",
  "version": 1,
  "author": {
    "id": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
    "firstName": "Timothée",
    "lastName": "Barbot",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015 - Tenez vous pret !",
  "posted": 1424872671661,
  "title": "Information d'inscription"
}
```

## `DELETE /informations/:info`

Supprime une information identifiée par `:info`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser (uniquement celle dont il est l'auteur)


### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/informations/d329e6a6-9b32-4e53-ade3-bdef14829f29" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse

En cas de réussite, la réponse a un statut `204 - No Content`

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png