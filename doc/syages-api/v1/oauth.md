# Authentification (OAuth)

***

Cette API utilise le protocole [OAuth 2.0](http://oauth.net).
Il existe différents workflow permettant l'obtention un jeton d'accès. Pour savoir quel workflow utiliser, rendez-vous à la section [workflows](#workflows).

Avant toute chose, vous devez détenir un couple (clé,secret) pour identifier votre application auprès de l'API.  
Consultez l'administrateur du système pour plus d'informations à ce sujet.

Voici une liste de ressources intéressantes si vous souhaitez en savoir plus sur le fonctionnement du protocole OAuth:

* [Digital Ocean - An introduction to OAuth 2](https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2)
* [Aaron Parecki - OAuth 2 Simplified](https://aaronparecki.com/articles/2012/07/29/1/oauth2-simplified)

Note: Le support des _scopes_ et _refresh tokens_ n'est pas prévu pour le moment.

| Endpoint | Description | Retourne (selon Authorization) | Statut |
| --- | --- | --- | --- |
| GET /oauth/auth | Endpoint /auth | | ![ok] |
| GET /oauth/revoke | Révoquer un jeton d'accès | 200 OK / 404 Not Found | ![ok] |
| GET /oauth/token | Endpoint /token | | ![ok] |
| GET /oauth/validate | Vérifier la validité d'un jeton d'accès | 200 {expiration_date} / 401 {error} | ![ok] |

---

# Workflows

Le protocole OAuth 2.0 propose différents workflows pour l'obtention d'un jeton d'accès:

* Web Server-side application: Ce workflow est destiné aux application web avec serveur (PHP, Java, ...).  
	* __Étape 1__: Générer un code d'authentification.  
		Endpoint: `auth`  
		response_type: `code`  
		Requires: `client_id`, `redirect_uri`  
		Optional: `state`  
		Observations: Le Resource Owner sera redirigé jusqu'au Consumer par l'URI `<redirect_uri>?code=<auth_code>` ou `<redirect_uri>?error=access_denied`. Le code est valide 5 minutes ^1.
		
		Exemple requete:  
		```
			/api/1/oauth/auth?response_type=code&client_id=<clé de l'application>&redirect_uri=http%3A%2F%2Fmonapp.com%2Fcallback
		```  
		Exemple réponse:  
		```
			302 Found http://monapp.com/callback?code=6510939cf0f2fbe712e82ac2`
		```  
	* __Étape 2__: Transformer le code d'authentification en jeton d'accès.  
		Endpoint: `token`  
		grant_type: `authorization_code`  
		Requires: `client_id`, `client_secret`, `code`, `redirect_uri`  
		Observations: Le Resource Owner sera redirigé juqu'au Consumer par l'URI `<redirect_uri>`. Le corps de la requête contient le jeton, le type d'authentification, l'expiration et potentiellement d'autres informations. Le jeton n'a pas d'expiration ^1, il peut être révoqué par l'application tierce elle même ou par l'utilisateur, dans les préférences de son compte.  
		
		Exemple requête:  
		```
			api/1/oauth/token?grant_type=authorization_code&client_id=<clé de l'application>&client_secret=<secret de l'application>&redirect_uri=http%3A%2F%2Fmonapp.com%2Fcallback&code=6510939cf0f2fbe712e82ac2
		```  
		Exemple réponse:  
		```
			302 Found http://monapp.com/callback
			
			{
				"access_token": "7bebd08ab0427349d65e89df843db8a9",
				"token_type": "Bearer",
				"expires_in": 1423322155
			}
		```  
* Web Client-side application: Ce workflow est destiné aux application web client (JavaScript), où il est impossible de préserver le secret.
	* __Étape 1__: Générer le jeton d'accès.  
		Endpoint: `auth`  
		response_type: `token`  
		Requires: `client_id`, `redirect_uri`  
		Optional: `state`  
		Observations: Le Resource Owner sera redirigé jusqu'au Consumer par l'URI `<redirect_uri>#token=<access_token>` ou `<redirect_uri>#error=access_denied`. Le jeton est valide 15 minutes ^1 ^2.
		
		Exemple requête:  
		```
			api/1/oauth/auth?response_type=code&client_id=<clé de l'application>&redirect_uri=http%3A%2F%2Fmonapp.com%2Fcallback
		```  
		Exemple réponse:  
		```
			http://monapp.com/callback#token=7bebd08ab0427349d65e89df843db8a9
		```  
		Notes concernant la sécurité: Nous recommendons fortement les développeurs d'application web coté client d'utiliser un proxy pour effectuer leurs requêtes. Ce proxy permettra:  
		* de conserver la clé de l'application à l'abri de ceux qui seraient tentés de fouiner dans le code JavaScript de votre application. La clé sera rajoutée lors des requêtes la nécessitant par le proxy.
		* de crypter le jeton d'accès par un algorithme symétrique, luttant ainsi contre le vol de cookies/localStorage/sessionStorage.
		* de traquer l'origine des requêtes sur le panneau developpement de Syages (sans proxy les requêtes auront pour origine les IP des utilisateurs finaux, ce qui rend la traque impossible).
* Native application: Ce workflow est destiné aux application natives (desktop ou mobile) et nécessite l'utilisation d'un schéma d'URI personnalisé.
	* Le client dispose de l'application native Syages ^2:
		Requête:  
		
		```
			syagesauth://authorize?response_type=token&client_id=<clé de l'application>&redirect_uri=monappli%3A%2F%2Fcallback
		```  
		response_type: `token`  
		Requires: `client_id`, `redirect_uri`  
		Optional: `state`  
		Observations: Le Resource Owner sera redirigé jusqu'au Consumer par l'URI `<redirect_uri>#token=<access_token>` ou `<redirect_uri>#error=access_denied`. Le jeton est valide 15 minutes ^1 ^3.  
		L'URI ne peut être de schéma HTTP ou HTTPS, il faut un schéma personalisé et enregistré dans la configuration de votre application sur Syages.  
		Réponse:  
		
		```
			monappli://callback?token=4d090423de89dfb8a98ab347d678beb5
		```  
	* Le client ne dispose pas de l'application native Syages: suivez les instructions de l'étape _Web Client-side_ avec un redirect_uri utilisant un schéma URI personnalisé.
* Implicit workflow: Ce workflow est reservé aux applications ayant obtenu un droit exceptionnel au `grant_type password`. La documentation de ce workflow ne sera pas évoquée ici.

---

# Utiliser le jeton d'accès

Lors de l'envoi d'une requête à l'API, vous devez préciser l'en-tête suivant:
```
	Authorization: <token_type> <access_token>
```
Pour toutes les applications n'utilisant pas le workflow _Web Server-side application_, le token_type est `Bearer`.

---

# Contribuer

Développer une API inviolable pour autant de support est impossible. L'erreur est humaine et le protocole OAuth a ses limites.
Nous comptons sur les développeurs utilisant cette API pour nous aider à combler les éventuelles back-door.
Si vous découvrez une faille de sécurité, merci de contacter l'équipe de développement ou un administrateur système.

---
^1: Cette durée est soumise à des changements sans réserve et donnée à titre indicatif.  
^2: Cette durée est reconduite lors d'une requête, pour une durée maximale de 6 heures.   
^4: L'application native Syages n'est pas encore disponible.  

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png