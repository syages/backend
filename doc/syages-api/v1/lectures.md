# Lectures

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /lectures](#get-lectures)| Récupérer une liste de lectures | ![][ok] ![][sec] |
| [GET /lectures/:lecture](#get-lectureslecture)| Récupérer une Lecture par son `id` | ![][ok] ![][sec] |
| [POST /lectures](#post-lectures)| Créer une nouvelle Lecture | ![][ok] ![][sec] |
| [PUT /lectures/:lecture](#put-lectureslecture)| Éditer une Lecture | ![][ok] ![][sec] |
| [DELETE /lectures/:lecture](#delete-lectureslecture)| Supprimer une Lecture | ![][ok] ![][sec] |
| [GET /lectures/:lecture/absences](#get-lectureslectureabsences) | Récupérer la liste des absences pour cette lecture | ![][ok] ![][sec] |
| [POST /lectures/:lecture/absences](#post-lectureslectureabsences)| Éditer les absences *notées* d'une Lecture | ![][ok] ![][sec] |

## `GET /lectures`

Retourne une liste de lectures.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lectures" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "lesson": {
      "id": 2,
      "course": {
        "name": "Français"
      }
    },
    "date": "2015-02-15T13:30:00.000Z",
    "classroom": "R301"
  },
  ...
]
```

## `GET /lectures/:lecture`

Retourne une lecture identifiée par `:lecture`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lectures/1" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": 1,
  "version": 1,
  "lesson": {
    "id": 2,
    "course": {
      "name": "Français"
    }
  },
  "date": "2015-02-15T13:30:00.000Z",
  "classroom": "R301",
  "notedAbsences": []
}
```

## `POST /lectures`

Crée une nouvelle lecture.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Elements du JSON

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| date | `Epoch milliseconds/Date` | `requis` | Date de la Lecture |
| lesson | `Lesson` | `requis` | Leçon à laquelle est liée la Lecture |
| classroom | `String` | `optionnel` | Salle de la Lecture |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/lectures" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "date": "2015-02-15T13:30:00+0000",
  "lesson": 2,
  "classroom": "R301"}'
```

### Exemple de réponse
```json
{
  "id": 1,
  "version": 1,
  "lesson": {
    "id": 2,
    "course": {
      "name": "Français"
    }
  },
  "date": "2015-02-15T13:30:00.000Z",
  "classroom": "R301",
  "notedAbsences": []
}
```

## `PUT /lectures/:lecture`

Édite la lecture `:lecture`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/lectures/1" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": 1,
  "version": 1,
  "lesson": 2,
  "date": "2015-02-15T14:30:00.000Z",
  "classroom": "R306"
}'
```

### Exemple de réponse
```json
{
  "id": 1,
  "version": 2,
  "lesson": {
    "id": 2,
    "course": {
      "name": "Français"
    }
  },
  "date": "2015-02-15T14:30:00.000Z",
  "classroom": "R306",
  "notedAbsences": []
}
```

## `DELETE /lectures/:lecture`

Supprime une lecture identifiée par `:lecture`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/lectures/1" \
  -H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

## `GET /lectures/:lecture/absences`

Retourne la liste des absences pour la lecture `:lecture`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lectures/21/absences" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 5
      }
    },
    "observerUser": {
      "id": 2
    },
    "date": "2015-02-15",
    "timeOfDay": "Après-midi"
  },
  ...
]
```

## `POST /lectures/:lecture/absences`

Crée des absences pour la lecture `:lecture`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Paramètre du JSON

Le JSON peut soit être une liste soit être une valeur représentant un stagiaire.  
La liste permet d'ajouter et retirer/modifier les absences pour la lecture `:lecture`.  
La valeur unique permet d'ajouter une absences pour le stagiaire identifié.  

### Exemple de requête

#### Liste
```bash
curl -X "POST" "http://localhost:9000/api/1/lectures/21/absences" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'[
  {
    "id": 1,
    "version": 1,
    "status": "Unjustified",
    "intern": 5,
    "observerUser": 2,
    "date": "2015-02-15",
    "timeOfDay": "Après-midi"
  },
  {
    "id": 21,
    "version": 1,
    "status": "Unjustified",
    "intern": 6,
    "observerUser": 2,
    "date": "2015-02-15",
    "timeOfDay": "Après-midi"
  },
  7
]'
```

#### Valeur unique
```bash
curl -X "POST" "http://localhost:9000/api/1/lectures/21/absences" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d "5"
```

### Exemple de réponse

#### Liste
```json
[
  {
    "id": 1,
    "version": 1,
    "intern": {
      "user": {
        "id": 5,
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    }
  },
  {
    "id": 21,
    "version": 1,
    "intern": {
      "user": {
        "id": 6,
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    }
  },
  {
    "id": 22,
    "version": 1,
    "intern": {
      "user": {
        "id": 7,
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    }
  }
]
```

#### Valeur unique
```json
[
  {
    "id": 21,
    "version": 1,
    "intern": {
      "user": {
        "id": 5,
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    }
  }
]
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png