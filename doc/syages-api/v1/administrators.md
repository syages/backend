# Administrateurs

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /administrators](#get-administrators) | Récupérer la liste des administrateurs | ![][ok] ![][sec] |
| [GET /administrators/:administrator](#get-administratorsadministrator) | Récupérer un administrateur | ![][ok] ![][sec] |
| [POST /administrators](#post-administrators) | Créer un nouvel administrateur | ![][ok] ![][sec] |
| [PUT /administrators/:administrator](#put-administratorsadministrator) | Éditer un administrateur | ![][ok] ![][sec] |
| [DELETE /administrators/:administrator](#delete-administratorsadministrator) | Supprimer un administrateur | ![][ok] ![][sec] |

## `GET /administrators`

Retourne une liste d'administrateurs.

**Autorisation:**
- Administrateur


### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/administrators" \
	-H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
[
  {
    "id": "d8b91623-1a36-4285-acf3-1c8f88827de5",
    "version": 1,
    "user": {
      "id": "79e67976-66e3-434e-a85b-47a1bfd6fd3c",
      "version": 1,
      "email": "bidon.aurelien@gmail.com",
      "firstName": "Aurelien",
      "flagType": "Administrator",
      "lastName": "Bidon",
      "pictureUrl": "/static/defaultavatar256.png"
    },
    "number": null,
    "office": null
  },
  {
    "id": "529bc10f-3e62-47c6-bf25-12f428da51cd",
    "version": 1,
    "user": {
      "id": "052c311d-70ef-42a6-bfe1-ec0b37198f5d",
      "version": 1,
      "email": "timo.b35@gmail.com",
      "firstName": "Timothée",
      "flagType": "Administrator",
      "lastName": "Barbot",
      "pictureUrl": "/static/defaultavatar256.png"
    },
    "number": null,
    "office": null
  },
  ...
]
```

## `GET /administrators/:administrator`

Retourne l'administrateur identifié par `:administrateur`.

**Autorisation:**
- Administrateur

### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/courses/262" \
  -H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
{
  "id": "d8b91623-1a36-4285-acf3-1c8f88827de5",
  "version": 1,
  "user": {
    "id": "79e67976-66e3-434e-a85b-47a1bfd6fd3c",
    "version": 1,
    "email": "bidon.aurelien@gmail.com",
    "firstName": "Aurelien",
    "flagType": "Administrator",
    "lastName": "Bidon",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null
}
```

## `POST /administrators`

Cr&eacute;er un nouvel administrateur.

**Autorisation:**
- Administrateur

### &Eacute;l&eacute;ments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| email | `String` | `requis` | Email de l'administrateur |
| firstName | `String` | `requis` | Prénom de l'administrateur |
| lastName | `String` | `requis` | Nom de l'administrateur |
| number | `String` | `optionnel` | Numéro de téléphone de l'administrateur |
| office | `String` | `optionnel` | Bureau de l'administrateur |
 
 
### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/administrators" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "user": {
    "email": "administrator@syages.fr",
    "firstName": "administrator",
    "lastName": "Demo"
  }
}'

```

### Exemple de réponse
```json
{
  "id": "6c158ec0-73f6-4e75-8011-22a9b96df50d",
  "version": 1,
  "user": {
    "id": "3bfefe5d-8d46-4aa6-b952-7fff20393c1d",
    "version": 1,
    "email": "administrator@syages.fr",
    "firstName": "administrator",
    "flagType": "Administrator",
    "lastName": "Demo",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null
}
```

## `PUT /administrators/:administrator`

&Eacute;dite l'administrateur identifiée par `:administrateur`.
*Il faut retourner l'objet en entier*

**Autorisation:**
- Administrateur

### Exemple de requ&ecirc;te
```bash
curl -X "PUT" "http://localhost:9000/api/1/administrators/79e67976-66e3-434e-a85b-47a1bfd6fd3c" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "id": "d8b91623-1a36-4285-acf3-1c8f88827de5",
  "version": 1,
  "user": {
    "id": "79e67976-66e3-434e-a85b-47a1bfd6fd3c",
    "version": 1,
    "email": "bidon.aurelien+test@gmail.com",
    "firstName": "Aurelien",
    "flagType": "Administrator",
    "lastName": "Bidon",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null
}'
```

### Exemple de r&eacute;ponse
```json
{
  "id": "d8b91623-1a36-4285-acf3-1c8f88827de5",
  "version": 2,
  "user": {
    "id": "79e67976-66e3-434e-a85b-47a1bfd6fd3c",
    "version": 2,
    "email": "bidon.aurelien+test@gmail.com",
    "firstName": "Aurelien",
    "flagType": "Administrator",
    "lastName": "Bidon",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null
}
```

## `DELETE /administrators/:administrator`

Supprime l'administrateur identifié par `:administrateur`.

**Autorisation:**
- Administrateur

### Exemple de requ&ecirc;te
```bash
curl -X "DELETE" "http://localhost:9000/api/1/administrators/3bfefe5d-8d46-4aa6-b952-7fff20393c1d" \
	-H "Authorization: <Authorization>"
```

### R&eacute;ponse

En cas de succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png