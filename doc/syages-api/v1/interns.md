# Interns

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /interns ](#get-interns)| Récupérer une liste de stagiaires | ![][ok] ![][sec] | 
| [GET /interns/:intern](#get-internsintern)| Récupérer un stagiaire par son `id` | ![][ok] ![][sec] |
| [POST /interns](#post-interns) | Créer un nouveau stagiaire | ![][ok] ![][sec] |
| [PUT /interns/:intern](#put-internsintern) | Éditer un stagiaire | ![][ok] ![][sec] |
| [DELETE /interns/:intern](#delete-internsintern) | Supprimer un stagiaire | ![][ok] ![][sec]  |
| [GET /interns/:intern/isFirst](#get-internsinternisfirst) | Récupérer si il s'agit de la 1ere connexion d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/infos](#get-internsinterninfos) | Récupérer les informations de profil d'un stagiaire | ![][ok] ![][sec] |
| [POST /interns/intern/infos](#post-internsinterninfos) | Ajouter des informations de profil à un stagiaire | ![][ok] ![][sec] |
| [PUT /interns/:intern/infos](#put-internsinterninfos) | Éditer les informations de profil d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/marks](#get-internsinternmarks) | Récupérer la liste des notes d'un stagiaire | ![][ok] ![][sec]  |
| [GET /interns/:intern/lessons](#get-internsinternlessons) | Récupérer la liste des leçons d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/notedAbsences](#get-internsinternnotedabsences) | Récupérer une liste d'absences *notées* d'un stagiaire| ![][ok] ![][sec] |
| [GET /interns/:intern/declaredAbsences](#get-internsinterndeclaredabsences) | Récupérer une liste d'absence *déclarée* d'un stagiaire| ![][ok] ![][sec]  |
| [GET /me/notedAbsences](#get-menotedabsences) | Récupérer une liste d'absences *notées* d'un stagiaire| ![][ok] ![][sec] |
| [GET /me/declaredAbsences](#get-medeclaredabsences) | Récupérer une liste d'absence *déclarée* d'un stagiaire| ![][ok] ![][sec] |


## `GET /interns`

Retourne une liste de stagiaires.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Paramètres de la requête

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| limit  | `Integer`| `optionnel` | Nombre d'objets à récupérer. 25 par défault, 100 au maximum |
| offset | `Integer` | `optionnel` | Numéro de la page à récupérer. 0 par défaut |
| sort | [Sort](../Readme.md#tri) | `optionnel` | Choix de l'ordre |
| fields | [Fields](../Readme.md#champs) | `optionnel` | Champ à afficher dans le Json retourné |

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 161,
    "version": 1,
    "user": {
      "id": 165,
      "version": 2,
      "firstName": "Demo",
      "lastName": "Stagiaire",
      "email": "intern@syages.fr"
    },
    "dateOfBirth": "1993-07-22",
    "followedDiplomas": [
      {
        "id": 161,
        "version": 1,
        "paid": true,
        "diploma": {
          "id": 161,
          "name": "DAEU-A 2015"
        },
        "optionalLessons": [
          {
            "id": 161,
            "course": {
              "name": "Mathématiques"
            }
          }
        ]
      }
    ]
  },
  {
    "id": 181,
    "version": 1,
    "user": {
      "id": 181,
      "version": 1,
      "firstName": "Tom",
      "lastName": "Test",
      "email": "tom+iut@syages.fr"
    },
    "dateOfBirth": "1993-07-22",
    "followedDiplomas": [
      {
        "id": 181,
        "version": 1,
        "paid": null,
        "diploma": {
          "id": 161,
          "name": "DAEU-A 2015"
        },
        "optionalLessons": [
          {
            "id": 161,
            "course": {
              "name": "Mathématiques"
            }
          }
        ]
      }
    ]
  },
  ...
]
```

## `GET /interns/:intern`

Retourne un stagiaire identifié par `:intern`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/interns/241"
```

### Exemple de réponse

```json
{
  "id": 241,
  "version": 2,
  "user": {
    "id": 241,
    "version": 2,
    "firstName": "Tom",
    "lastName": "Test",
    "email": "tom.test@syages.fr"
  },
  "dateOfBirth": "1993-07-22",
  "followedDiplomas": [
    {
      "id": 241,
      "version": 2,
      "paid": true,
      "status": "Undergoing",
      "diploma": {
        "id": 161,
        "name": "DAEU-A 2015"
      },
      "optionalLessons": [
        {
          "id": 161,
          "course": {
            "name": "Mathématiques"
          }
        }
      ]
    }
  ]
}
```

## `POST /interns`

Crée un nouveau stagiaire.

**Autorisation:**
- Administrator
- President
- StaffUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| firstName | `String` | `requis` | Prénom du stagiaire |
| lastName | `String` | `requis` | Nom du stagiaire |
| email | `String` | `requis` | Email du stagiaire | 
| dateOfBirth | `Epoch milliseconds/Date` | `requis`| Date de naissance |
| followedDiplomas | `List<FollowedDiploma>` | `optionnel` | Liste de formations suivies par le stagiaire |

**FollowedDiploma :**
>
- diploma: 
&nbsp;&nbsp;&nbsp;&nbsp;`id` d'une formation.
- lessons:
&nbsp;&nbsp;&nbsp;&nbsp;`List<Lesson>` contenant les leçons optionnelles suivies par le stagiaire.
- paid:
&nbsp;&nbsp;&nbsp;&nbsp;`boolean` décrivant si le stagiaire a payé ses frais d'inscriptions.
>

### Exemple de requête

```bash
curl -X "POST" "http://localhost:9000/api/1/interns" \
  -H "Content-Type: application/json" \
  -d $'{
  "user": {
    "firstName": "Tom",
    "lastName": "Test",
    "email": "tom+test@syages.fr",
    "password": "root"
  },
  "dateOfBirth": "1993-07-22",
  "followedDiplomas": [{
    "diploma": 161,
    "optionalLessons": [161],
    "paid": true
  }]
}'
```

### Exemple de réponse

```json
{
  "id": 221,
  "version": 1,
  "user": {
    "id": 221,
    "version": 1,
    "firstName": "Tom",
    "lastName": "Test",
    "email": "tom+test@syages.fr"
  },
  "dateOfBirth": "1993-07-22",
  "followedDiplomas": [
    {
      "id": 221,
      "version": 1,
      "paid": true,
      "diploma": {
        "id": 161,
        "name": "DAEU-A 2015"
      },
      "optionalLessons": [
        {
          "id": 161
        }
      ]
    }
  ]
}
```

## `PUT /interns/:intern`

Édite le stagiare `:intern`.
*Veuillez retourner un objet Intern entier*

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête

```bash
curl -X "PUT" "http://localhost:9000/api/1/interns/221" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": 221,
  "version": 1,
  "user": {
    "id": 221,
    "version": 1,
    "firstName": "Tom",
    "lastName": "TestEdit",
    "email": "tom+test@syages.fr"
  },
  "dateOfBirth": "1993-07-22",
  "followedDiplomas": [
    {
      "id": 221,
      "version": 1,
      "paid": false,
      "diploma": {
        "id": 161,
        "name": "DAEU-A 2015"
      },
      "optionalLessons": [
        {
          "id": 161
        }
      ]
    }
  ]
}'
```

### Exemple de réponse

```json
{
  "id": 221,
  "version": 2,
  "user": {
    "id": 221,
    "version": 2,
    "firstName": "Tom",
    "lastName": "TestEdit",
    "email": "tom+test@syages.fr"
  },
  "dateOfBirth": "1993-07-22",
  "followedDiplomas": [
    {
      "id": 221,
      "version": 2,
      "paid": false,
      "diploma": {
        "id": 161,
        "name": "DAEU-A 2015"
      },
      "optionalLessons": [
        {
          "id": 161
        }
      ]
    }
  ]
}
```

## `DELETE /interns/:intern`

Supprime le stagiaire `:stagiaire`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête

```bash
curl -X "DELETE" "http://localhost:9000/api/1/interns/105"
```

### Exemple de réponse

```json
```

## `GET /interns/:intern/isFirst`

Retourne un booléen indiquant si il s'agit de la 1ère connexion du stagiaire `:intern`.

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/intern/105/isFirst"
```

### Exemple de réponse
```json
{
	"first": true
}
```

## `GET /interns/:intern/infos`

Retourne les informations de profil du stagiaire `:stagiaire`.

**Autorisation:**
- InternUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns/105/infos"
```

### Exemple de réponse
```json
{
  "id": 1,
  "version": 1,
  "lastSchool": "St Martin",
  "lastSchoolYear": 1357513200000,
  "professionalProject": "blablablabla",
  "worked": true,
  "job": "Poissonnier"
}
```

## `POST /interns/:intern/infos`

Ajoute des informations de profil au stagiaire `:intern`.

**Autorisation:**
- Administrator
- InternUser

### Paramètres du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| lastSchool | `String` | `optionnel` | Dernier établissement du stagiaire |
| lastSchoolYear | `Date | Epoch millis` | `optionnel` | Dernière année d'enseignement |
| professionalProject | `String` | `optionnel` | Projet professionnel du stagiaire |
| worked | `Boolean` | `optionnel` | Est ce que le stagiaire a travaillé ? |
| job | `String` | `optionnel` | Si `worked` , quel(s) emploi(s) ? |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/interns/105/infos" \
  -H "Content-Type: application/json" \
  -d $'{
  "lastSchool": "St Martin",
  "lastSchoolYear": 1357553871000,
  "professionalProject": "blablablabla",
  "worked": true,
  "job": "Poissonnier"
}'
```

### Réponse
```json
```

## `PUT /interns/:intern/infos`

Édite les informations du stagiaire `:intern`.

**Autorisation:**
- Administrator
- President
- StaffUser
- InternUser

## Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/interns/168/infos" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": 41,
  "version": 1,
  "job": "Poissonnier",
  "lastSchool": "Jules Guesde",
  "lastSchoolYear": 1357513200000,
  "professionalProject": "blablablabla",
  "worked": true
}'
```

### Exemple de réponse
```json
{
  "id": 41,
  "version": 2,
  "job": "Poissonnier",
  "lastSchool": "Jules Guesde",
  "lastSchoolYear": 1357513200000,
  "professionalProject": "blablablabla",
  "worked": true
}
```

## `GET /interns/:intern/marks`

Retourne les notes du stagiaire `:intern` pour sa formation en cours.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns/65/marks"
```

### Exemple de réponse
```json
[
  {
    "diploma": {
      "id": 61,
      "name": "DAEU-A 2015",
      "status": "Active",
      "passMark": 10
    },
    "periods": [
      {
        "period": {
          "id": 61,
          "ordre": 1,
          "startDate": "2014-09-02",
          "endDate": "2014-12-05"
        },
        "marks": [
          {
            "lesson": {
              "id": 62,
              "course": {
                "id": 122,
                "name": "Français"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 63,
              "course": {
                "id": 123,
                "name": "Anglais"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 61,
              "course": {
                "id": 121,
                "name": "Mathématiques"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          }
        ]
      },
      {
        "period": {
          "id": 62,
          "ordre": 2,
          "startDate": "2014-12-08",
          "endDate": "2015-03-06"
        },
        "marks": [
          {
            "lesson": {
              "id": 62,
              "course": {
                "id": 122,
                "name": "Français"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 63,
              "course": {
                "id": 123,
                "name": "Anglais"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 61,
              "course": {
                "id": 121,
                "name": "Mathématiques"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": [
              {
                "id": 41,
                "mark": 12.0,
                "status": "Present",
                "evaluation": {
                  "name": "EvaluationTest",
                  "date": "2015-02-05",
                  "coefficient": 2.5,
                  "type": "Final"
                }
              }
            ]
          }
        ]
      },
      {
        "period": {
          "id": 63,
          "ordre": 3,
          "startDate": "2015-03-09",
          "endDate": "2015-06-12"
        },
        "marks": [
          {
            "lesson": {
              "id": 62,
              "course": {
                "id": 122,
                "name": "Français"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 63,
              "course": {
                "id": 123,
                "name": "Anglais"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          },
          {
            "lesson": {
              "id": 61,
              "course": {
                "id": 121,
                "name": "Mathématiques"
              },
              "passMark": null,
              "coefficient": 10.0
            },
            "evaluations": []
          }
        ]
      }
    ]
  }
]
```

## `GET /interns/:intern/lessons`

Retourne une liste de leçons suivies par le stagiaire `:intern` dans sa formation en cours.

**Autorisation:**
- Administrator
- InternUser

## Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns/5/lessons"
```
## Exemple de réponse
```json
[
  {
    "id": 22,
    "version": 1,
    "coefficient": 10.0,
    "course": {
      "id": 42,
      "version": 1,
      "domain": {
        "id": 21,
        "version": 1,
        "name": "Langues"
      },
      "name": "Français"
    },
    "optional": false,
    "passMark": null
  },
  {
    "id": 21,
    "version": 1,
    "coefficient": 10.0,
    "course": {
      "id": 41,
      "version": 1,
      "domain": {
        "id": 22,
        "version": 1,
        "name": "Mathématiques et Sciences de la Nature"
      },
      "name": "Mathématiques"
    },
    "optional": true,
    "passMark": null
  },
  ...
]
```

# `GET /interns/:intern/notedabsences`

Retourne une liste d'absences *notées* pour le stagiaire `:intern`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

## Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns/105/notedAbsences" \
  -H "Authorization: <Authorization>"
```

## Exemple de réponse
```json
[
  {
    "id": 101,
    "version": 1,
    "date": "2015-02-09",
    "timeOfDay": "Morning",
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 105
      }
    },
    "observerUser": {
      "id": 102
    }
  }
]
```


# `GET /interns/:intern/declaredabsences`

Retourne une liste d'absences *déclarées* pour le stagiaire `:intern`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

## Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/interns/105/declaredAbsences" \
  -H "Authorization: <Authorization>"
```

## Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "startDate": "2015-02-09",
    "timeOfDayStart": "Morning",
    "endDate": "2015-02-10",
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "PublicTransportProblem",
    "commentary": null,
    "intern": {
      "user": {
        "id": 105
      }
    }
  }
]
```

# `GET /me/notedabsences`

Retourne une liste d'absences *notées* pour le stagiaire.

Autorisation:
- Stagiaire

## Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/notedAbsences" \
  -H "Authorization: <Authorization>"
```

## Exemple de réponse
```json
[
  {
    "id": 101,
    "version": 1,
    "date": "2015-02-09",
    "timeOfDay": "Morning",
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 105
      }
    },
    "observerUser": {
      "id": 102
    }
  }
]
```

# `GET /me/declaredabsences`

Retourne une liste d'absences *déclarées* pour le stagiaire.

Autorisation:
- Stagiaire

## Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/declaredAbsences" \
  -H "Authorization: <Authorization>"
```

## Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "startDate": "2015-02-09",
    "timeOfDayStart": "Morning",
    "endDate": "2015-02-10",
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "PublicTransportProblem",
    "commentary": null,
    "intern": {
      "user": {
        "id": 105
      }
    }
  }
]
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png