# Utilisateurs

***

Les différents types d'utilisateurs sont:
- Administrator: Administrateur du site.
- Intern: Stagiaire.
- President: Président du DAEU.
- Staff: Staff administratif.
- Teacher: Enseignant.


| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /users ](#get-users)| Récupérer une liste d'utilisateurs | ![][ok] |
| [GET /user/:user](#get-usersuser)| Récupérer un utilisateur par son id | ![][ok] |
| [GET /users/:user/picture](#get-usersuserpicture) | Récupérer l'image d'un utilisateur | ![][ok] |
| [POST /users/:user/picture](#post-usersuserpicture) | Changer l'image d'un utilisateur | ![][ok] |



## `GET /users`

Retourne une liste d'utilisateurs

### Parameters

| Name | Type | Requis? | Description|
| --- | :---: | :---: | --- |
| limit  | `Integer`| `optionnel`|  |
| offset | `Integer` | `optionnel`| |

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/users"
```

### Exemple de réponse
```json
[
  {
    "email": "bidon.aurelien@gmail.com",
    "firstName": "Aurelien",
    "lastName": "Bidon",
    "id": 61,
    "flagType": "Administrative"
  },
  {
    "email": "timo.b35@gmail.com",
    "firstName": "Timothée",
    "lastName": "Barbot",
    "id": 62,
    "flagType": "Administrative"
  }
]
```

## `GET /user/:user`

Retourne un utilisateur par son `id`

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/user/22"
```

### Exemple de réponse
```json
{
  "email": "timo.b35@gmail.com",
  "firstName": "Timothée",
  "lastName": "Barbot",
  "id": 22,
  "flagType": "Administrative"
}
```

## `GET /users/:user/picture`

Retourne l'image d'un utilisateur.



## `POST /users/:user/picture`

Change l'image d'un utilisateur

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png