# Présidents

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /presidents](#get-presidents) | Récupérer la liste des présidents | ![][ok] ![][sec] |
| [GET /presidents/:president](#get-presidentspresident) | Récupérer un président | ![][ok] ![][sec] |
| [POST /presidents](#post-presidents) | Créer un nouveau président | ![][ok] ![][sec] |
| [PUT /presidents/:president](#put-presidentspresident) | Éditer un président | ![][ok] ![][sec] |
| [DELETE /presidents/:president](#delete-presidentspresident) | Supprimer un président | ![][ok] ![][sec] |

## `GET /presidents`

Retourne une liste de président.

**Autorisation:**
- Administrateur
- Président


### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/presidents" \
  -H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
[
  {
    "id": "0093ed3d-d58e-41cd-9443-27bf9da91e1f",
    "version": 1,
    "user": {
      "id": "a280c30c-b811-4e4a-9dea-df00aca58a82",
      "version": 1,
      "email": "president@syages.fr",
      "firstName": "president",
      "flagType": "President",
      "lastName": "Demo",
      "pictureUrl": "/static/defaultavatar256.png"
    },
    "number": null,
    "office": null,
    "day": null,
    "hour": null,
    "recurrence": null
  },
  {
    "id": "47bf8528-d231-4cc4-8f41-262e2af4fc4c",
    "version": 1,
    "user": {
      "id": "499e1791-4633-4365-ba7b-374538f88de8",
      "version": 1,
      "email": "president2@syages.fr",
      "firstName": "Pierre",
      "flagType": "President",
      "lastName": "Dupont",
      "pictureUrl": "/static/defaultavatar256.png"
    },
    "number": null,
    "office": null,
    "day": 1,
    "hour": "14:33:17",
    "recurrence": 7
  }
]
```

## `GET /presidents/:president`

Retourne le président identifié par `:president`.

**Autorisation:**
- Administrateur
- Président

### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/presidents/a4690fbf-448d-4b9b-ae0a-793a859799d4" \
  -H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
{
  "id": "46e12573-47f2-423d-8d00-845d4f838415",
  "version": 1,
  "user": {
    "id": "a4690fbf-448d-4b9b-ae0a-793a859799d4",
    "version": 1,
    "email": "president@syages.fr",
    "firstName": "president",
    "flagType": "President",
    "lastName": "Demo",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null,
  "day": null,
  "hour": null,
  "recurrence": null
}
```

## `POST /presidents`

Cr&eacute;er un nouveau président.

**Autorisation:**
- Administrateur
- Président

### &Eacute;l&eacute;ments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| email | `String` | `requis` | Email de l'administrateur |
| firstName | `String` | `requis` | Prénom de l'administrateur |
| lastName | `String` | `requis` | Nom de l'administrateur |
| number | `String` | `optionnel` | Numéro de téléphone de l'administrateur |
| office | `String` | `optionnel` | Bureau de l'administrateur |
| day | `Integer` | `optionnel` | Jour de la semaine pour la sauvegarde de la BDD |
| hour | `String` | `optionnel` | Heure pour la sauvegarde de la BDD |
| recurrence | `Integer` | `optionnel` | Multiple de 7 indiquant la récurrence des sauvegarde de la BDD |
 
 
### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/presidents" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "user": {
    "firstName": "Pierre",
    "lastName": "Dupont",
    "email": "president2@syages.fr"
  },
  "day": "1",
  "hour": "14:33:53",
  "recurrence": "7"
}'
```

### Exemple de réponse
```json
{
  "id": "47bf8528-d231-4cc4-8f41-262e2af4fc4c",
  "version": 1,
  "user": {
    "id": "499e1791-4633-4365-ba7b-374538f88de8",
    "version": 1,
    "email": "president2@syages.fr",
    "firstName": "Pierre",
    "flagType": "President",
    "lastName": "Dupont",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null,
  "day": 1,
  "hour": "14:33:17",
  "recurrence": 7
}
```

## `PUT /presidents/:president`

&Eacute;dite le président identifiée par `:president`.
*Il faut retourner l'objet en entier*

**Autorisation:**
- Administrateur
- Président

### Exemple de requ&ecirc;te
```bash
curl -X "PUT" "http://localhost:9000/api/1/presidents/a4690fbf-448d-4b9b-ae0a-793a859799d4" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "46e12573-47f2-423d-8d00-845d4f838415",
  "version": 1,
  "user": {
    "id": "a4690fbf-448d-4b9b-ae0a-793a859799d4",
    "version": 1,
    "email": "president@syages.fr",
    "firstName": "president",
    "flagType": "President",
    "lastName": "DemoTest",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null,
  "day": 1,
  "hour": null,
  "recurrence": null
}'
```

### Exemple de r&eacute;ponse
```json
{
  "id": "46e12573-47f2-423d-8d00-845d4f838415",
  "version": 1,
  "user": {
    "id": "a4690fbf-448d-4b9b-ae0a-793a859799d4",
    "version": 1,
    "email": "president@syages.fr",
    "firstName": "president",
    "flagType": "President",
    "lastName": "DemoTest",
    "pictureUrl": "/static/defaultavatar256.png"
  },
  "number": null,
  "office": null,
  "day": 1,
  "hour": null,
  "recurrence": null
}
```

## `DELETE /presidents/:president`

Supprime le président identifié par `:president`.

**Autorisation:**
- Administrateur

### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/presidents/a4690fbf-448d-4b9b-ae0a-793a859799d4" \
  -H "Authorization: <Authorization>"
```

### R&eacute;ponse

En cas de succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png