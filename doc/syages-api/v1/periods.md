# Périodes

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /periods](#get-periods) | Récupérer la liste des périodes | ![][ok] ![][sec] |
| [GET /periods/:period](#get-periodsperiod) | Récupérer une période par son identifiant `:period` | ![][ok] ![][sec] |
| [POST /periods](#post-periods) | Créer une nouvelle période | ![][ok] ![][sec] |
| [PUT /periods/:period](#put-periodsperiod) | Éditer une période existante | ![][ok] ![][sec] |
| [DELETE /periods/:period](#delete-periodsperiod) | Supprimer une période existante | ![][ok] ![][sec] |
| [GET /periods/:period/evaluations](#get-periodsperiodevaluations) | Récupérer la liste des évaluations d'une période | ![][ok] ![][sec] |

## `GET /periods`

Retourne la liste des périodes.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/periods" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 43,
    "version": 1,
    "ordre": 3,
    "diploma": {
      "id": 41,
      "name": "DAEU-A 2015"
    },
    "endDate": "2015-06-12",
    "startDate": "2015-03-09"
  },
  {
    "id": 61,
    "version": 1,
    "ordre": 1,
    "diploma": {
      "id": 61,
      "name": "DAEU 2015"
    },
    "endDate": "2014-12-12",
    "startDate": "2014-09-12"
  },
  {
    "id": 62,
    "version": 1,
    "ordre": 2,
    "diploma": {
      "id": 61,
      "name": "DAEU 2015"
    },
    "endDate": "2015-03-12",
    "startDate": "2014-12-13"
  },
  ...
]
```


## `GET /periods/:period`

Retourne la période `:period`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/periods/42" \
	-H "Authorization: <Authorization>"
```


### Exemple de réponse
```json
{
  "id": 42,
  "version": 4,
  "ordre": 2,
  "diploma": {
    "id": 41,
    "name": "DAEU-A 2015"
  },
  "endDate": "2015-03-06",
  "startDate": "2014-12-11"
}
```

## `POST /periods`

Crée une nouvelles période.

**Autorisation:**
- Administrator

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/periods" \
	-H "Authorization: <Authorization
	>" \
	-H "Content-Type: application/json" \
	-d $'{
  "ordre": 1,
  "diploma": 41,
  "endDate": "2015-12-06",
  "startDate": "2014-09-09"
}'
```


### Exemple de réponse
```json
{
  "id": 101,
  "version": 1,
  "ordre": 1,
  "diploma": {
    "id": 41,
    "name": "DAEU-A 2015"
  },
  "endDate": "2015-12-06",
  "startDate": "2014-09-09"
}
```


## `PUT /periods/:period`

Édite la période `:period`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/periods/42" \
	-H "Authorization: <Authorization>" \
	-H "Content-Type: application/json" \
	-d $'{
  "id": 42,
  "version": 3,
  "ordre": 2,
  "diploma": 41,
  "endDate": "2015-03-06",
  "startDate": "2014-12-11"
}'
```


### Exemple de réponse
```json
{
  "id": 42,
  "version": 4,
  "ordre": 2,
  "diploma": {
    "id": 41,
    "name": "DAEU-A 2015"
  },
  "endDate": "2015-03-06",
  "startDate": "2014-12-11"
}
```

## `DELETE /periods/:period`

Supprime la période `:period`.

**Autorisation:**
- Administrator

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/periods/41" \
	-H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.



## `GET /periods/:period/evaluations`

Retourne la liste des évaluations de la période `:period`.

**Auorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/periods/42/evaluations" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 21,
    "version": 1,
    "name": "EvaluationTest",
    "date": "2015-02-05",
    "coefficient": 2.5,
    "visible": true,
    "type": "Final",
    "lesson": {
      "id": 41
    },
    "period": {
      "id": 42,
      "startDate": "2014-12-11",
      "endDate": "2015-03-06",
      "ordre": 2
    }
  },
  {
    "id": 1,
    "version": 1,
    "name": "EvaluationTest",
    "date": "2015-02-05",
    "coefficient": 2.5,
    "visible": true,
    "type": "Final",
    "lesson": {
      "id": 41
    },
    "period": {
      "id": 42,
      "startDate": "2014-12-11",
      "endDate": "2015-03-06",
      "ordre": 2
    }
  },
  ...
]
```




[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png