# Utilisateur connecté

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /me](#get-me) | Récupérer les informations de l'utilisateur | ![][ok] ![][sec] |
| [GET /me/lessons](#get-melessons) | Récupérer les leçons de l'utilisateur (Enseignant seulement) | ![][ok] ![][sec] |
| [GET /me/notedAbsences](#get-menotedabsences) | Récupérer les absences *notées* d'un stagiaire (Stagiaire uniquement) | ![][ok] ![][sec] |
| [GET /me/declaredAbsences](#get-medeclaredabsences) | Récupérer les absences *déclarées* d'une stagiaire (Stagiaire uniquement) | ![][ok] ![][sec] |
| [GET /me/tokens](#get-metokens) | Récupérer les tokens d'autorisations d'un utilisateur | ![][ok] ![][sec] |
| [PATCH /me/password](#patch-mepassword) | Changer le mot de passe de l'utilisateur | ![][ni] ![][sec] |
| [PATCH /me/picture](#patch-mepicture) | Changer la photo de l'utilisateur | ![][ni] ![][sec] |


## `GET /me`

Retourne l'utilisateur connecté

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me" \
	-H "Authorization: <Authorization>"
```
### Exemple de réponse

```json
{
  "id": 45,
  "version": 2,
  "email": "intern1@syages.fr",
  "firstName": "Demo1",
  "flagType": "Intern",
  "lastName": "Stagiaire",
  "pictureUrl": null
}
```

## `GET /me/lessons`

Retourne la liste des leçons de l'utilisateur connecté.

**Autorisation:**
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/lessons" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 201,
    "version": 1,
    "course": {
      "id": 101,
      "name": "Mathématiques"
    },
    "diploma": {
      "id": 101,
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": true,
    "teacher": {
      "user": {
        "id": 48,
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  {
    "id": 202,
    "version": 1,
    "course": {
      "id": 102,
      "name": "Français"
    },
    "diploma": {
      "id": 101,
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": false,
    "teacher": {
      "user": {
        "id": 48,
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  ...
]
```

## `GET /me/notedAbsences`

Retourne la liste des absences *notées* de l'utilisateur connecté.

**Autorisation:**
- InternUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/notedAbsences" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 41,
    "version": 1,
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 45
      }
    },
    "observerUser": {
      "id": 42
    },
    "date": "2015-02-06",
    "timeOfDay": "Après-midi"
  },
  {
    "id": 42,
    "version": 1,
    "status": "Unjustified",
    "intern": {
      "user": {
        "id": 45
      }
    },
    "observerUser": {
      "id": 42
    },
    "date": "2015-02-15",
    "timeOfDay": "Après-midi"
  },
  ...
]
```

## `GET /me/declaredAbsences`

Retourne la liste des absences *déclarées* de l'utilisateur connecté.

**Autorisation:**
- InternUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/declaredAbsences" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "startDate": "2015-02-05",
    "timeOfDayStart": "Morning",
    "endDate": "2015-02-06",
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "PublicTransportProblem",
    "commentary": null,
    "intern": {
      "user": {
        "id": 45
      }
    }
  },
  ...
]
```

## `GET /me/tokens`

Retourne la liste des tokens d'autorisation de l'utilisateur connecté.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/me/tokens" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "code": "266d77ade86560cdc6a3f235d81eb9ef",
    "consumer": {
      "author": "Syages",
      "name": "Syages Website"
    }
  }
]
```

## `PATCH /me/password`

## `PATCH /me/picture`

Change la photo de l'utilisateur connecté.

**Autorisation**

### Exemple de requête

### Exemple de réponse

