# Mati&egrave;res

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /courses](#get-courses) | R&eacute;cup&eacute;rer une liste de mati&egrave;res | ![][ok] ![][sec] |
| [GET /courses/:course](#get-coursescourse) | R&eacute;cup&eacute;rer une mati&egrave;re par son id | ![][ok] ![][sec] |
| [POST /courses](#post-courses) | Cr&eacute;er une nouvelle mati&egrave;re | ![][ok] ![][sec] |
| [PUT /courses/:course](#put-coursescourse) | &Eacute;diter une mati&egrave;re | ![][ok] ![][sec] |
| [DELETE /courses/:course](#delete-coursescourse) | Supprimer une mati&egrave;re | ![][ok] ![][sec] |

## `GET /courses`

Retourne une liste de mati&egrave;res.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/courses" \
  -H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
[
  {
    "id": "c57441d4-97e3-44a8-a782-0973b6d132f5",
    "version": 1,
    "name": "Mathématiques",
    "domain": {
      "id": "13f14ba0-541e-4a7d-a92f-dc08c39b2138",
      "name": "Mathématiques et Sciences de la Nature"
    }
  },
  {
    "id": "8bb9af26-6d05-4a57-ab07-a693f4655877",
    "version": 1,
    "name": "Français",
    "domain": {
      "id": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4",
      "name": "Langues"
    }
  },
  {
    "id": "35f840ee-22ba-494c-870b-64a71ba083be",
    "version": 1,
    "name": "Anglais",
    "domain": {
      "id": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4",
      "name": "Langues"
    }
  },
  ...
]
```

## `GET /courses/:course`

Retourne une mati&egrave;re par son identifiant `:course`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requ&ecirc;te
```bash
curl -X "GET" "http://localhost:9000/api/1/courses/c57441d4-97e3-44a8-a782-0973b6d132f5" \
  -H "Authorization: <Authorization>"
```

### Exemple de r&eacute;ponse
```json
{
  "id": "c57441d4-97e3-44a8-a782-0973b6d132f5",
  "version": 1,
  "name": "Mathématiques",
  "domain": {
    "id": "13f14ba0-541e-4a7d-a92f-dc08c39b2138",
    "name": "Mathématiques et Sciences de la Nature"
  }
}
```

## `POST /courses`

Cr&eacute;er une nouvelle mati&egrave;re.

**Autorisation:**
- Administrator
- President
- StaffUser

### &Eacute;l&eacute;ments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| name | `String` | `requis` | Nom de la mati&egrave;re |
| domain | `Integer` | `optionnel` | Domaine en relation avec la mati&egrave;re |

 
### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/courses" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "name": "Truc",
  "domain": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4"
}'
```

### Exemple de réponse
```json
{
  "id": "16019548-1a00-4b61-8548-b785df7d015e",
  "version": 1,
  "name": "Truc",
  "domain": {
    "id": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4",
    "name": "Langues"
  }
}
```

## `PUT /courses/:course`

&Eacute;dite la mati&egrave;re identifiée par `:course`.
*Il faut retourner l'objet en entier*

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requ&ecirc;te
```bash
curl -X "PUT" "http://localhost:9000/api/1/courses/16019548-1a00-4b61-8548-b785df7d015e" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "16019548-1a00-4b61-8548-b785df7d015e",
  "version": 1,
  "name": "Turc",
  "domain": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4"
}'
```

### Exemple de r&eacute;ponse
```json
{
  "id": "16019548-1a00-4b61-8548-b785df7d015e",
  "version": 2,
  "name": "Turc",
  "domain": {
    "id": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4",
    "name": "Langues"
  }
}
```

## `DELETE /course/:id`

Supprime la mati&egrave;re identifiée par `:course`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requ&ecirc;te
```bash
curl -X "DELETE" "http://localhost:9000/api/1/courses/16019548-1a00-4b61-8548-b785df7d015e" \
  -H "Authorization: <Authorization>"
```

### R&eacute;ponse
En cas de succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png