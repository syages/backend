# Evaluations

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /evaluations](#get-evaluations) | Récupérer une liste d'évaluations | ![][ok] ![][sec] |
| [GET /evaluations/:evaluation](#get-evaluationsevaluation) | Récupérer une évaluation | ![][ok] ![][sec] |
| [POST /evaluations](#post-evaluations) | Créer une nouvelle évaluation | ![][ok] ![][sec] |
| [PUT /evaluations/:evaluation](#put-evaluationsevaluation) | Éditer une évaluation | ![][ok] ![][sec] |
| [DELETE /evaluations/:evaluation](#delete-evaluationsevaluation) | Supprimer une évaluation | ![][ok] ![][sec] |
| [GET /evaluations/:evaluation/marks](#get-evaluationsevaluationmarks) | Récupérer les notes d'une évaluation| ![][ok] ![][sec] |
| [POST /evaluations/:evaluation/marks](#post-evaluationsevaluationmarks) | Créer les notes d'une évaluation | ![][ok] ![][sec] |
| [PUT /evaluations/:evaluation/marks](#put-evaluationsevaluationmarks) | Éditer les notes d'une évaluation | ![][ok] ![][sec] | 

## `GET /evaluations`

Retourne la liste des évaluations.

**Autorisation:**
- Administration
- President

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/evaluations" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "7ae37de1-1cfe-49f9-bf63-17081a463316",
    "version": 1,
    "name": "EvaluationTest",
    "date": 1423090800000,
    "coefficient": 2.5,
    "gradingScale": 20,
    "visible": true,
    "type": "Final",
    "lesson": {
      "id": "21a9a9bf-72d1-42bc-8346-94c1dec9b5df",
      "course": {
        "id": "8ac87b84-0a5e-4251-a5e1-10da7bcfd07c",
        "name": "Philosophie"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
          "firstName": "David",
          "lastName": "Hébert"
        }
      }
    },
    "period": {
      "id": "32db28b7-9e70-48b0-a38d-7b5530e76451",
      "startDate": 1417993200000,
      "endDate": 1425596400000,
      "ordre": 2
    }
  },
  {
    "id": "525e25d7-c756-4405-b52d-0e138776e55a",
    "version": 1,
    "name": "EvaluationTest",
    "date": 1428012000000,
    "coefficient": 2.5,
    "gradingScale": 20,
    "visible": true,
    "type": "Final",
    "lesson": {
      "id": "21a9a9bf-72d1-42bc-8346-94c1dec9b5df",
      "course": {
        "id": "8ac87b84-0a5e-4251-a5e1-10da7bcfd07c",
        "name": "Philosophie"
      },
      "teacher": {
        "user": {
          "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
          "firstName": "David",
          "lastName": "Hébert"
        }
      }
    },
    "period": {
      "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
      "startDate": 1425855600000,
      "endDate": 1434060000000,
      "ordre": 3
    }
  }
]
```


## `GET /evaluations/:evaluation`

Retourne une evaluation par son `id`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/evaluations/525e25d7-c756-4405-b52d-0e138776e55a" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": "525e25d7-c756-4405-b52d-0e138776e55a",
  "version": 1,
  "name": "EvaluationTest",
  "date": 1428012000000,
  "coefficient": 2.5,
  "gradingScale": 20,
  "visible": true,
  "type": "Final",
  "lesson": {
    "id": "21a9a9bf-72d1-42bc-8346-94c1dec9b5df",
    "course": {
      "id": "8ac87b84-0a5e-4251-a5e1-10da7bcfd07c",
      "name": "Philosophie"
    },
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  "period": {
    "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
    "startDate": 1425855600000,
    "endDate": 1434060000000,
    "ordre": 3
  }
}
```

## `POST /evaluations`

Crée une évaluation.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| coefficient | `Float` | `requis` | Coefficient de l'évaluation |
| date | `Date/Epoch millis`| `requis` | Epoch time for the date of the evaluation |
| name | `String` | `requis` | Nom de l'évaluation |
| type | `Final/Continue` | `requis` | Type de l'évaluation |
| visible | `Boolean` | `optionnel` | Évaluation visible pour les stagiaires ? |
| lesson | `Integer` | `requis` | Leçon liée à cette évaluation |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/evaluations" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "name": "Evaluation Géo",
  "coefficient": 1.0,
  "gradingScale": 20,
  "type": "Continue",
  "date": "2015-04-03",
  "visible": true,
  "lesson": "e942111c-15c3-4720-8ae4-fde20e4e0190"
}'
```

### Exemple de réponse
```json
{
  "id": "f1d5dac1-bbd2-488e-8417-945501daa71a",
  "version": 1,
  "name": "Evaluation Géo",
  "date": 1428012000000,
  "coefficient": 1.0,
  "gradingScale": 20,
  "visible": true,
  "type": "Continue",
  "lesson": {
    "id": "e942111c-15c3-4720-8ae4-fde20e4e0190",
    "course": {
      "id": "306237d2-3f49-429c-b8e2-619ad9cb1d3f",
      "name": "Géographie"
    },
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  "period": {
    "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
    "startDate": 1425855600000,
    "endDate": 1434060000000,
    "ordre": 3
  }
}
```

## `PUT /evaluations/:evaluation`

Édite l'évaluation `:evaluation`.
*Veuillez retourner un objet Evaluation entier*

**Autorisation:**
- Administrator
- President
- TeacherUser (uniquement si il s'agit d'une évaluation d'une leçon qu'il enseigne)

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/evaluations/f1d5dac1-bbd2-488e-8417-945501daa71a" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "f1d5dac1-bbd2-488e-8417-945501daa71a",
  "version": 1,
  "name": "Evaluation Géo",
  "date": 1428012000000,
  "coefficient": 2.0,
  "gradingScale": 20,
  "visible": true,
  "type": "Final",
  "lesson": "e942111c-15c3-4720-8ae4-fde20e4e0190"
}'
```

### Exemple de réponse
```json
{
  "id": "f1d5dac1-bbd2-488e-8417-945501daa71a",
  "version": 1,
  "name": "Evaluation Géo",
  "date": 1428012000000,
  "coefficient": 1.0,
  "gradingScale": 20,
  "visible": true,
  "type": "Continue",
  "lesson": {
    "id": "e942111c-15c3-4720-8ae4-fde20e4e0190",
    "course": {
      "id": "306237d2-3f49-429c-b8e2-619ad9cb1d3f",
      "name": "Géographie"
    },
    "teacher": {
      "user": {
        "id": "bb46b3ac-50be-4ae5-bf1e-709829f1c5b0",
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  "period": {
    "id": "8e7efbeb-61c0-4846-a154-99717fd9ac4a",
    "startDate": 1425855600000,
    "endDate": 1434060000000,
    "ordre": 3
  }
}
```

## `DELETE /evaluations/:evaluation`

Supprime l'évaluation `:evaluation`.

**Autorisation:**
- Administrator
- President
- TeacherUser (uniquement si il s'agit d'une évaluation d'une leçon qu'il enseigne)

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/evaluations/92418f4e-106d-480b-8f93-a1abdc2a1fb4" \
  -H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`


## `GET /evaluations/:evaluation/marks`

Retourne une liste de notes pour l'évaluation `:evaluation`.

**Autorisation:**
- Administrator
- TeacherUser
- President

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/evaluations/f1d5dac1-bbd2-488e-8417-945501daa71a/marks" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "c281b4a7-9707-4390-83f2-5784c1632dae",
    "version": 1,
    "intern": {
      "user": {
        "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
        "firstName": "Demo1",
        "lastName": "Stagiaire"
      }
    },
    "status": "Absent",
    "mark": null
  },
  {
    "id": "877b8a58-a646-46c3-bfb1-26fa257a2905",
    "version": 1,
    "intern": {
      "user": {
        "id": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 16.5
  },
  {
    "id": "2c0e223b-790a-4f86-a22d-cb301d4faee8",
    "version": 1,
    "intern": {
      "user": {
        "id": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
        "firstName": "Demo3",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 12.0
  },
  ...
]
```

## `POST /evaluations/:evaluation/marks`

Crée une liste de notes pour l'évaluation `:evaluation`.

**Autorisation:**
- Administrator
- TeacherUser
- President

### Éléments du JSON

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| mark | `Float` | `optionnel` | Note du stagiaire |
| intern | `InternUser` | `requis` | Stagiaire |
| status | `Status` | `requis` | Statut du stagiaire pour cette évaluation |

*Status*
- Present
- Absent
- Excuse

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/evaluations/f1d5dac1-bbd2-488e-8417-945501daa71a/marks" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'[
  {
    "mark": 12,
    "intern": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
    "status": "Present"
  },
  {
    "mark": 16.5,
    "intern": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
    "status": "Present"
  },
  {
    "mark": null,
    "intern": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
    "status": "Absent"
  },
  ...
]'
```

### Exemple de réponse
```json
[
  {
    "id": "2c0e223b-790a-4f86-a22d-cb301d4faee8",
    "version": 1,
    "intern": {
      "user": {
        "id": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
        "firstName": "Demo3",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 12.0
  },
  {
    "id": "877b8a58-a646-46c3-bfb1-26fa257a2905",
    "version": 1,
    "intern": {
      "user": {
        "id": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 16.5
  },
  {
    "id": "c281b4a7-9707-4390-83f2-5784c1632dae",
    "version": 1,
    "intern": {
      "user": {
        "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
        "firstName": "Demo1",
        "lastName": "Stagiaire"
      }
    },
    "status": "Absent",
    "mark": null
  },
  ...
]
```

## `PUT /evaluations/:evaluation/marks`

Édite les notes de l'évaluations `:evaluation`.

**Autorisation:**
- Administrator
- President
- TeacherUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/evaluations/f1d5dac1-bbd2-488e-8417-945501daa71a/marks" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'[
  {
    "id": "c281b4a7-9707-4390-83f2-5784c1632dae",
    "version": 1,
    "intern": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
    "status": "Excuse",
    "mark": null
  },
  {
    "id": "877b8a58-a646-46c3-bfb1-26fa257a2905",
    "version": 1,
    "intern": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
    "status": "Present",
    "mark": 16.5
  },
  {
    "id": "2c0e223b-790a-4f86-a22d-cb301d4faee8",
    "version": 1,
    "intern": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
    "status": "Absent"
  },
  ...
]'
```

### Exemple de réponse
```json
[
  {
    "id": "c281b4a7-9707-4390-83f2-5784c1632dae",
    "version": 1,
    "intern": {
      "user": {
        "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08",
        "firstName": "Demo1",
        "lastName": "Stagiaire"
      }
    },
    "status": "Excuse",
    "mark": null
  },
  {
    "id": "877b8a58-a646-46c3-bfb1-26fa257a2905",
    "version": 1,
    "intern": {
      "user": {
        "id": "315e50a0-cf69-4727-bd0a-f9253b0b528a",
        "firstName": "Demo2",
        "lastName": "Stagiaire"
      }
    },
    "status": "Present",
    "mark": 16.5
  },
  {
    "id": "2c0e223b-790a-4f86-a22d-cb301d4faee8",
    "version": 1,
    "intern": {
      "user": {
        "id": "040bef93-09ac-4f9a-95cc-93ef5c6767fd",
        "firstName": "Demo3",
        "lastName": "Stagiaire"
      }
    },
    "status": "Absent",
    "mark": null
  },
  ...
]
```


[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png