# Staff

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /staff](#get-staff) | Récupérer une liste de membres du staff | ![][ok] ![][sec] |
| [GET /staff/:staff](#get-staffstaff) | Récupérer un membre du staff | ![][ok] ![][sec] |
| [POST /staff](#post-staff) | Créer un membre du staff | ![][ok] ![][sec] |
| [PUT /staff/:staff](#put-staffstaff) | Éditer un membre du staff | ![][ok] ![][sec] |
| [DELETE /staff/:staff](#delete-staffstaff) | Supprimer un membre du staff | ![][ok] ![][sec] |

## `GET /staff`

Retourne une liste de membres du staff.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/staff" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse

```json
[
  {
    "id": 41,
    "version": 1,
    "user": {
      "id": 44,
      "version": 1,
      "firstName": "Demo",
      "lastName": "Staff",
      "email": "staff@syages.fr",
      "pictureUrl": null
    },
    "number": "0102030405",
    "office": "A101"
  },
  ...
]
```

## `GET /staff/:staff`

Retourne le membre du staff `:staff`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/staff/141" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse

```json
{
  "id": 41,
  "version": 1,
  "user": {
    "id": 44,
    "version": 1,
    "firstName": "Demo",
    "lastName": "Staff",
    "email": "staff@syages.fr",
    "pictureUrl": null
  },
  "number": "0102030405",
  "office": "A101"
}
```

## `POST /staff`

Crée un nouveau membre du staff.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| firstName | `String` | `requis` | Prénom du membre du staff |
| lastName | `String` | `requis` | Nom du membre du staff |
| email | `String` | `requis` | Email du membre du staff |
| office | `String`| `optionnel` | Bureau du membre du staff |
| number | `String` | `optionnel` | Numéro du membre du staff |

### Exemple de requête

```bash
curl -X "POST" "http://localhost:9000/api/1/staff" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "user": {
    "email": "test.staff@syages.fr",
    "firstName": "test",
    "lastName": "Staff"
  },
  "number": "0102030477",
  "office": "B333"
}'
```

### Exemple de réponse

```json
{
  "id": 61,
  "version": 1,
  "user": {
    "id": 61,
    "version": 1,
    "firstName": "test",
    "lastName": "Staff",
    "email": "test.staff@syages.fr",
    "pictureUrl": null
  },
  "number": "0102030477",
  "office": "B333"
}
```

## `PUT /staff/:staff`

Édite le membre du staff `:staff`.
*Veuillez retourner un objet Staff entier*

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête

```bash
curl -X "PUT" "http://localhost:9000/api/1/staff/61" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": 61,
  "version": 1,
  "user": {
    "id": 61,
    "version": 1,
    "firstName": "testEdit",
    "lastName": "Staff",
    "email": "test.staff@syages.fr",
    "pictureUrl": null
  },
  "number": "0102030477",
  "office": "B333"
}'
```

### Exemple de réponse

```json
{
  "id": 61,
  "version": 2,
  "user": {
    "id": 61,
    "version": 2,
    "firstName": "testEdit",
    "lastName": "Staff",
    "email": "test.staff@syages.fr",
    "pictureUrl": null
  },
  "number": "0102030477",
  "office": "B333"
}
```

## `DELETE /staff/:staff`

Supprime le membre du staff `:staff`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête

```bash
curl -X "DELETE" "http://localhost:9000/api/1/staff/61" \
  -H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.


[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png