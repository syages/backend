# Rapports

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /reports](#get-reports) | Récupérer une liste de rapports | ![][ok] ![][sec] |
| [GET /reports/closed](#get-reportsclosed) | Récupérer une liste de rapport *fermés* | ![][ok] ![][sec] |
| [GET /reports/opened](#get-reportsopened) | Récupérer une liste de rapport *ouverts* | ![][ok] ![][sec] |
| [GET /reports/:report](#get-reportsreport) | Récupérer un rapport | ![][ok] ![][sec] |
| [POST /reports](#post-reports) | Créer un nouveau rapport | ![][ok] ![][sec] |
| [PATCH /reports/:report](#patch-reportsreport) | Éditer le statut d'un rapport | ![][ok] ![][sec] |
| [DELETE /reports/:report](#delete-reportsreport) | Supprimer un rapport | ![][ok] ![][sec] |

## `GET /reports`

Retourne la liste des rapports.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/reports" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "user": {
      "id": 26
    },
    "controller": "blablabla",
    "type": "Bug",
    "status": "Opened"
  },
  {
    "id": 2,
    "version": 2,
    "user": {
      "id": 26
    },
    "controller": "blablabla",
    "type": "Bug",
    "status": "Closed"
  }
]
```

## `GET /reports/closed`

Retourne la liste des rapports *fermés*.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/reports/closed" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1"
```

### Exemple de réponse
```json
[
  {
    "id": 2,
    "version": 2,
    "user": {
      "id": 26
    },
    "controller": "blablabla",
    "type": "Bug",
    "status": "Closed"
  }
]
```

## `GET /reports/opened`

Retourne la liste des rapports *ouverts*.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/reports/opened" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "user": {
      "id": 26
    },
    "controller": "blablabla",
    "type": "Bug",
    "status": "Opened"
  }
]
```



## `GET /reports/:report`

Retourne un rapport par son `id`.

**Autorisation**

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/reports/2" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1"
```

### Exemple de requête
```json
{
  "id": 2,
  "version": 1,
  "createdAt": 1423955335893,
  "commentary": "Ca bug nan ?",
  "user": {
    "id": 26,
    "version": 1,
    "email": "teacher@syages.fr",
    "firstName": "David",
    "flagType": "Teacher",
    "lastName": "Hébert",
    "pictureUrl": "/api/1/users/26/picture"
  },
  "context": "blablabla",
  "controller": "blablabla",
  "route": "blablabla",
  "type": "Bug",
  "bugType": "Critical",
  "status": "Opened"
}
```

## `POST /reports`

Créer un rapport.

**Autorisation**

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| context | `String` | `requis` | Contexte du rapport |
| controller | `String`| `requis` | Controlleur du rapport |
| route | `String` | `requis` | Route du rapport |
| type | `type` | `requis` | Type du rapport |
| priorirty | `priority` | `optionnel` | Type de bug du rapport |
| commentary | `Integer` | `requis` | Commentaire |

type:
- Bug
- Message

priority:
- Critical
- Normal
- Minor

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/reports" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "context":  "{\\n\\t\\"model\\": {\\n\\t\\t\\"field\\": \\"aaa\\"\\n\\t}\\n}",
  "controller": "blablabla",
  "route": "blablabla",
  "type": "Bug",
  "priority": "Critical",
  "commentary": "Ca bug nan ?"
}'
```

### Exemple de réponse
```json
{
  "id": 102,
  "version": 1,
  "priority": "Critical",
  "commentary": "Ca bug nan ?",
  "context": "{\n\t\"model\": {\n\t\t\"field\": \"aaa\"\n\t}\n}",
  "controller": "blablabla",
  "createdAt": 1424344673417,
  "route": "blablabla",
  "status": "Opened",
  "type": "Bug",
  "user": {
    "id": 162,
    "version": 1,
    "email": "timo.b35@gmail.com",
    "firstName": "Timothée",
    "flagType": "Administrator",
    "lastName": "Barbot",
    "pictureUrl": null
  },
  "userInfo": null
}
```

## `PATCH /reports/:report`

Éditer le statut d'un rapport.

**Autorisation**

### Exemple de requête
```bash
curl -X "PATCH" "http://localhost:9000/api/1/reports/2" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1" \
  -H "Content-Type: application/json" \
  -d $'{
  "status": "Closed"
}'
```

### Exemple de réponse
```json
{
  "id": 2,
  "version": 2,
  "createdAt": 1423955335893,
  "commentary": "Ca bug nan ?",
  "user": {
    "id": 26,
    "version": 1,
    "email": "teacher@syages.fr",
    "firstName": "David",
    "flagType": "Teacher",
    "lastName": "Hébert",
    "pictureUrl": "/api/1/users/26/picture"
  },
  "context": "blablabla",
  "controller": "blablabla",
  "route": "blablabla",
  "type": "Bug",
  "bugType": "Critical",
  "status": "Closed"
}
```

## `DELETE /reports/:report`

Supprimer un rapport.

**Autorisation**

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/reports/2" \
  -H "Authorization: Bearer 9045555d9dab4a36a1e74f4b3806d1d1"
```

### Réponse

En cas de succès, la réponse a pour statut `204 - No Content`

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png