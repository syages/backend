# Le&ccedil;ons

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /lessons](#get-lessons) | Récupérer la liste des leçons | ![][ok] ![][sec] |
| [GET /lessons/:lesson](#get-lessonslesson) | Récupérer un cours | ![][ok] ![][sec] |
| [POST /lessons](#post-lessons) | Créer une nouvelle leçon | ![][ok] ![][sec] | 
| [PUT /lessons/:lesson](#put-lessonslesson) | &Eacute;diter un cours | ![][ok] ![][sec] |
| [GET /lessons/:lesson/interns](#get-lessonslessoninterns) | Récupérer la liste des stagiaires d'un cours | ![][ok] ![][sec] |
| [GET /lessons/:lesson/lectures](#get-lessonslessonlectures) | Récupérer la liste des lectures d'une leçons | ![][ok] ![][sec] |
| [GET /lessons/:lesson/evaluations](#get-lessonslessonevaluations) | Récupérer la liste des évaluation d'une leçons | ![][ok] ![][sec] |

## `GET /lessons`

Retourne une liste de leçons.

**Autorisation:**
- Administrator

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lessons" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "course": {
      "id": 1,
      "name": "Mathématiques"
    },
    "diploma": {
      "id": 1,
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": true,
    "teacher": {
      "user": {
        "id": 8,
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  {
    "id": 2,
    "version": 1,
    "course": {
      "id": 2,
      "name": "Français"
    },
    "diploma": {
      "id": 1,
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": false,
    "teacher": {
      "user": {
        "id": 8,
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  {
    "id": 3,
    "version": 1,
    "course": {
      "id": 3,
      "name": "Anglais"
    },
    "diploma": {
      "id": 1,
      "name": "DAEU-A 2015"
    },
    "passMark": null,
    "coefficient": 10.0,
    "optional": false,
    "teacher": {
      "user": {
        "id": 8,
        "firstName": "David",
        "lastName": "Hébert"
      }
    }
  },
  ...
]
```

## `GET /lessons/:lesson`

Retourne une leçon identifiée par `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lessons/2" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": 2,
  "version": 1,
  "course": {
    "id": 2,
    "name": "Français"
  },
  "diploma": {
    "id": 1,
    "name": "DAEU-A 2015"
  },
  "passMark": null,
  "coefficient": 10.0,
  "optional": false,
  "teacher": {
    "user": {
      "id": 8,
      "firstName": "David",
      "lastName": "Hébert"
    }
  }
}
```

## `POST /lessons/:lesson`

Crée une nouvelle leçon.

**Autorisation:**
- Administrator
- President
- StaffUser

### Éléments du JSON

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| course | `Course` | `requis` | matière associée |
| diploma | `Diploma` | `requis` | formation associée |
| teacher | `TeacherUser` | `optionnel` | Enseignant associé |
| passMark | `Float` | `requis` | Note de validation |
| coefficient | `Float` | `requis` | Coefficient de la leçon |
| optional | `Boolean` | `requis` | La leçon est optionnelle ? |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/lessons" \
	-H "Authorization: Bearer b6eed463c521402ad014f5558923b5f1" \
	-H "Content-Type: application/json" \
	-d $'{
  "course": 69,
  "diploma": 41,
  "teacher": 28,
  "passMark": 10,
  "coefficient": 1.5,
  "optional": true
}'
```

### Exemple de réponse
```json
{
  "id": 121,
  "version": 1,
  "course": {
    "id": 69,
    "name": "Physique"
  },
  "diploma": {
    "id": 41,
    "name": "DAEU-A 2015"
  },
  "passMark": 10.0,
  "coefficient": 1.5,
  "optional": true,
  "teacher": {
    "user": {
      "id": 28,
      "firstName": "David",
      "lastName": "Hébert"
    }
  }
}
```

## `PUT /lessons/:lesson`

Édite la leçon `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/lessons/181" \
	-H "Authorization: Bearer b6eed463c521402ad014f5558923b5f1" \
	-H "Content-Type: application/json" \
	-d $'{
  "id": 181,
  "version": 2,
  "course": 68,
  "diploma": 81,
  "passMark": 10.0,
  "coefficient": 1.5,
  "optional": false,
  "teacher": 28
}'
```

### Exemple de réponse
```json
{
  "id": 181,
  "version": 3,
  "course": {
    "id": 68,
    "name": "Droit"
  },
  "diploma": {
    "id": 81,
    "name": "DAEU 2015"
  },
  "passMark": 10.0,
  "coefficient": 1.5,
  "optional": false,
  "teacher": {
    "user": {
      "id": 28,
      "firstName": "David",
      "lastName": "Hébert"
    }
  }
}
```

## `DELETE /lessons/:lesson`

Supprime la leçon `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/lessons/121" \
	-H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.


## `GET /lessons/:lesson/interns`

Retourne la liste des stagiaire suivant la leçon `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lessons/41/interns" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 21,
    "version": 1,
    "user": {
      "id": 25,
      "firstName": "Demo2",
      "lastName": "Stagiaire"
    }
  },
  ...
]
```

## `GET /lessons/:lesson/lectures`

Retourne la liste des lectures appartenant à la leçon `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lessons/41/lectures" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 41,
    "version": 1,
    "lesson": {
      "id": 41,
      "course": {
        "name": "Mathématiques"
      }
    },
    "date": "2015-02-15T13:30:00.000Z",
    "classroom": "R301"
  },
  ...
]
```

## `GET /lessons/:lesson/evaluations`

Retourne la liste des evaluations appartenant à la leçon `:lesson`.

**Autorisation:**
- Administrator
- President
- StaffUser
- TeacherUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/lessons/41/evaluations" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": 1,
    "version": 1,
    "name": "EvaluationTest",
    "date": "2015-02-05",
    "coefficient": 2.5,
    "visible": true,
    "type": "Final",
    "lesson": {
      "id": 41
    }
  },
  ...
]
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png