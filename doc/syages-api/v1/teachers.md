# Enseignants

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /teachers](#get-teachers) | Récupérer la liste des enseignants | ![][ok] |
| [GET /teachers/:teacher](#get-teachersteacher) | Récupérer un enseignants par son id | ![][ok] |
| [POST /teachers](#post-teachers) | Créer un nouvel enseignant | ![][ok] |
| [PUT /teachers/:teacher](#put-teachersteacher) | Editer un enseignant | ![][ok] |
| [PATCH /teachers/:teacher](#patch-teachersteacher) | Editer un enseignant *partiellement* | ![][ni] |
| [DELETE /teachers/:teacher](#delete-teachersteacher) | Supprimer un enseignant | ![][ok] |
| [GET /teachers/domains/:domain](#get-teachersdomainsdomain) | Liste d'enseignants pour un domain | ![][ok] |
| [GET /me/lessons]() | Liste des cours d'un enseignant | ![][ni] |

## `GET /teachers`

Retourne une liste d'enseignants.

### Paramètres de la requête

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| limit  | `Integer`| `optionnel` | Nombre d'objets à récupérer. 25 par défault, 100 au maximum |
| offset | `Integer` | `optionnel` | Numéro de la page à récupérer. 0 par défaut |
| sort | [Sort](../Readme.md#tri) | `optionnel` | Choix de l'ordre |
| fields | [Fields](..Readme.md#champs) | `optionnel` | Champ à afficher dans le Json retourné |

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/teachers"
```

### Example de réponse

```json
[
  {
    "id": 101,
    "version": 1,
    "user": {
      "id": 106,
      "version": 1,
      "email": "teacher@syages.fr",
      "firstName": "David",
      "lastName": "Hébert",
      "flagType": "Teacher",
      "pictureUrl": "/api/1/user/106/picture"
    },
    "number": "0123456789",
    "office": "T007",
    "domains": [
      {
        "id": 101,
        "version": 1,
        "name": "Langues"
      }
    ]
  }
]
```

## `GET /teachers/:teacher`

Retourne un enseignant par son `id`

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/teachers/106"
```

### Example de réponse

```json
{
  "id": 101,
  "version": 1,
  "user": {
    "id": 106,
    "version": 1,
    "email": "teacher@syages.fr",
    "firstName": "David",
    "lastName": "Hébert",
    "flagType": "Teacher",
    "pictureUrl": "/api/1/user/106/picture"
  },
  "number": "0123456789",
  "office": "T007",
  "domains": [
    {
      "id": 101,
      "version": 1,
      "name": "Langues"
    }
  ]
}
```

## `POST /teachers`

Créer un nouvel enseignant

### Éléments du Json

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| firstName | `String` | `requis` | Prénom de l'enseignant |
| lastName| `String` | `requis` | Nom de l'enseignant | 
| email | `String` | `requis` | Email de l'enseignant |
| password | `String` | `optionnel` | Mot de passe de l'enseignant |
| office | `String` | `optionnel` | Bureau de l'enseignant |
| number | `String` | `optionnel` | Numéro de l'enseignant |
| domains | `List<Integer>` | `optionnel` | Liste d'`id` de Domain | 

### Exemple de requête

```bash
curl -X "POST" "http://localhost:9000/api/1/teachers" \
	-H "Content-Type: application/json" \
	-d $'{
  "office": "B300",
  "number": "0123456789",
  "user": {
    "firstName": "Tom",
	"lastName": "Test",
    "email": "tom+iut@syages.fr",
    "password": "root"
  },
  "domains": [
    101,
    102  ]
}'
```

### Exemple de réponse
```json
{
  "id": 102,
  "version": 1,
  "user": {
    "id": 107,
    "version": 1,
    "email": "tom+iut@syages.fr",
    "firstName": "Tom",
    "lastName": "Test",
    "flagType": "Teacher",
    "pictureUrl": "/api/1/user/107/picture"
  },
  "number": "0123456789",
  "office": "B300",
  "domains": [
    {
      "id": 101,
      "version": 2,
      "name": "Langues"
    },
    {
      "id": 102,
      "version": 2,
      "name": "Mathématiques et Sciences de la Nature"
    }
  ]
}
```

## `PUT /teachers/:teacher`

Éditer un enseignant

*Veuillez retourner un objet Teacher entier*

### Exemple de requête

```bash
curl -X "PUT" "http://localhost:9000/api/1/teachers/107" \
	-H "Content-Type: application/json" \
	-d $'{
  "id": 102,
  "version": 1,
  "user": {
    "id": 107,
    "version": 1,
    "email": "tom+iut@syages.fr",
    "firstName": "Tom",
    "lastName": "TestEdit",
    "flagType": "Teacher",
    "pictureUrl": "/api/1/user/107/picture"
  },
  "number": "0123456789",
  "office": "B300",
  "domains": [
    101
  ]
}'
```

### Exemple de réponse

```json
{
  "id": 102,
  "version": 2,
  "user": {
    "id": 107,
    "version": 2,
    "email": "tom+iut@syages.fr",
    "firstName": "Tom",
    "lastName": "TestEdit",
    "flagType": "Teacher",
    "pictureUrl": "/api/1/user/107/picture"
  },
  "number": "0123456789",
  "office": "B300",
  "domains": [
    {
      "id": 101,
      "version": 3,
      "name": "Langues"
    }
  ]
}
```

## `PATCH /teachers/:teacher`

Éditer un enseignant *partiellement*

### Exemple de requête

```bash
```

### Exemple de réponse

```json
```

## `DELETE /teachers/:teacher`

Supprimer un enseignant

### Exemple de requête

```bash
curl -X "DELETE" "http://localhost:9000/api/1/teachers/107"
```

### Exemple de réponse

En cas de succès, une réponse au statut 204 - No Content est retournée.


## `GET /teachers/domains/:domain`

Retourne une liste d'enseignant pour le domaine choisi.

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/teachers/domains/121"
```


### Exemple de réponse
```json
[
  {
    "id": 121,
    "version": 1,
    "user": {
      "id": 126,
      "version": 1,
      "firstName": "David",
      "lastName": "Hébert",
      "email": "teacher@syages.fr",
      "pictureUrl": null
    },
    "number": "0123456789",
    "office": "T007",
    "lessons": [
      {
        "id": 121,
        "course": {
          "name": "Mathématiques"
        }
      },
      {
        "id": 122,
        "course": {
          "name": "Français"
        }
      },
      {
        "id": 123,
        "course": {
          "name": "Anglais"
        }
      }
    ]
  }
]
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png