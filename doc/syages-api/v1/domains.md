# Domains

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /domains](#get-domains) | Récupérer une liste de domaines | ![][ok] ![][sec] |
| [GET /domains/:domain](#get-domainsdomain) | Récupérer un domaine par son `id` | ![][ok] ![][sec] |
| [POST /domains](#post-domains) | Créer un nouveau domaine | ![][ok] ![][sec] |
| [PUT /domains/:domain](#put-domainsdomain) | Éditer un domaine | ![][ok] ![][sec] |
| [DELETE /domains/:domain](#delete-domainsdomain) | Supprimer un domaine | ![][ok] ![][sec] |

## `GET /domains`

Retourne une liste de domaines.

**Autorisation:**
- Administrator
- President
- StaffUser

### Paramêtres de la requête

| Name | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| limit | `Integer`| `optionnel` | Nombre d'objets à récupérer. 25 par défault, 100 au maximum |
| offset | `Integer` | `optionnel` | Numéro de la page à récupérer. 0 par défaut |
| sort | [Sort](../Readme.md#tri) | `optionnel` | Choix de l'ordre |
| fields | [Fields](..Readme.md#champs) | `optionnel` | Champ à afficher dans le Json retourné |

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/domains" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "267ad6cc-d94f-4b8e-a79f-5bdfba8d2ef4",
    "version": 1,
    "name": "Langues"
  },
  {
    "id": "13f14ba0-541e-4a7d-a92f-dc08c39b2138",
    "version": 1,
    "name": "Mathématiques et Sciences de la Nature"
  },
  {
    "id": "367bfb7b-319a-417c-a38a-0765a27eee9b",
    "version": 1,
    "name": "Sciences humaines et sociales"
  }
]
```

## `GET /domains/:domain`

Retourne un domaine par son `id`.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/domains/367bfb7b-319a-417c-a38a-0765a27eee9b" \
  -H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
  "id": "367bfb7b-319a-417c-a38a-0765a27eee9b",
  "version": 1,
  "name": "Sciences humaines et sociales"
}
```

## `POST /domains`

Crée un nouveau domaine.

**Autorisation:**
- Administrator
- President
- StaffUser

### Json Elements

| Name | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| name | `String` | `requis` | Nom du domaine |

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/domains" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "name": "Education Physiqu"
}'
```

### Exemple de réponse
```json
{
  "id": "23ba21d6-7013-45b2-ae7e-25efd7277000",
  "version": 1,
  "name": "Education Physiqu"
}
```

## `PUT /domains/:domain`

Édite un domaine.
*Veuillez retourner un objet Domain entier*

**Autorisation:**
- Administrator
- President
- StaffUser

### Example Request
```bash
curl -X "PUT" "http://localhost:9000/api/1/domains/23ba21d6-7013-45b2-ae7e-25efd7277000" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "id": "23ba21d6-7013-45b2-ae7e-25efd7277000",
  "version": 1,
  "name": "Education Physique"
}'
```

### Example Response
```json
{
  "id": "23ba21d6-7013-45b2-ae7e-25efd7277000",
  "version": 2,
  "name": "Education Physique"
}
```

## `DELETE /domains/:domain`

Supprime un domaine.

**Autorisation:**
- Administrator
- President
- StaffUser

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/domains/23ba21d6-7013-45b2-ae7e-25efd7277000" \
  -H "Authorization: <Authorization>"
```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png