# Absences *déclarées*

***

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /declaredAbsences](#get-declaredabsences) | Récupérer une liste d'absences *déclarées* | ![][ok] ![][sec] |
| [GET /declaredAbsences/:absence](#get-declaredabsencesabsence) | Récupérer une absence *déclarée* par son `id` | ![][ok] ![][sec] |
| [POST /declaredAbsences](#post-declaredabsences) | Créer une absence *déclarée* | ![][ok] ![][sec] |
| [PUT /declaredAbsences/:absences](#put-declaredabsencesabsence) | Éditer une absence *déclarée* | ![][ok] ![][sec] |
| [DELETE /declaredAbsences/:absences](#delete-declaredabsencesabsence) | Supprimer une absence *déclarée* | ![][ok] ![][sec] |
| [PATCH /declaredAbsences/:absences/status](#patch-declaredabsencesabsencestatus) | Changer le status d'une absence *déclarée* | ![][ok] ![][sec] |


## `GET /declaredAbsences`

Retourne une liste d'absences.
 
**Autorisation:**
- Staff
- Administrateur
- Président

### Paramètres de la requête

| Nom | Type | Requis? | Description | Statut |
| --- | :---: | :---: | --- | --- |
| limit  | `Integer`| `optionnel` | Nombre d'objets à récupérer. 25 par défault, 100 au maximum | ![][ni] |
| offset | `Integer` | `optionnel` | Numéro de la page à récupérer. 0 par défaut | ![][ni] |
| sort | [Sort](../Readme.md#tri) | `optionnel` | Choix de l'ordre | ![][ni] |
| fields | [Fields](..Readme.md#champs) | `optionnel` | Champ à afficher dans le Json retourné | ![][ni] |

### Exemple de requête

```bash
curl -X "GET" "http://localhost:9000/api/1/declaredAbsences" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
[
  {
    "id": "07d838ba-709f-4f18-89ed-a7622ec5b8ab",
    "version": 1,
    "startDate": 1423090800000,
    "timeOfDayStart": "Morning",
    "endDate": 1423177200000,
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "PublicTransportProblem",
    "commentary": null,
    "intern": {
      "user": {
        "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
      }
    }
  },
  ...
]
```

## `GET /declaredAbsences/:absence`

Retourne une absence *déclarée* par son `id`.

**Authorisation:**
- Administrateur
- Staff
- Président

### Exemple de requête
```bash
curl -X "GET" "http://localhost:9000/api/1/declaredAbsences/07d838ba-709f-4f18-89ed-a7622ec5b8ab" \
	-H "Authorization: <Authorization>"
```

### Exemple de réponse
```json
{
    "id": "07d838ba-709f-4f18-89ed-a7622ec5b8ab",
    "version": 1,
    "startDate": 1423090800000,
    "timeOfDayStart": "Morning",
    "endDate": 1423177200000,
    "timeOfDayEnd": "Afternoon",
    "status": "ToProcess",
    "reason": "PublicTransportProblem",
    "commentary": null,
    "intern": {
      "user": {
        "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
      }
    }
  }
```

## `POST /declaredAbsences`

Créer une absence *déclarée*.

**Authorisation:**
- InternUser

### Éléments du Json

| Name | Type | Required? | Description |
| --- | :---: | :---: | --- |
| startDate | `Date` | `requis` | Date de début d'absence |
| endDate |`Date` | `requis` | Date de fin d'absence |
| timeOfDayStart | `Morning/Afternoon`| `requis` | Moment du début d'absence |
| timeOfDayEnd | `Morning/Afternoon` |`requis` | Moment de fin d'absence|
| reason | `Reason`| `requis` | Raison de l'absence |
| commentary | `String`| `optionnel` | Commentaires si raison = Autres|

Reason : 
- Death ("Décès")
- MedicalCertificate ("Certificat médical")
- DrivingTestNotification ("Convocation permis de conduire")
- TheoryDrivingTestNotification ("Convocation code de la route")
- Summons ("Convocation administrative")
- PublicTransportProblem ("Problème de transport en commun")
- ReligiousHoliday ("Fête religieuse")
- Other ("Autre")

### Exemple de requête
```bash
curl -X "POST" "http://localhost:9000/api/1/declaredAbsences" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "startDate": "2015-02-05",
  "timeOfDayStart": "Morning",
  "endDate": "2015-02-06",
  "timeOfDayEnd": "Afternoon",
  "reason": "PublicTransportProblem"
}'
```

### Exemple de réponse
```json
{
  "id": "07d838ba-709f-4f18-89ed-a7622ec5b8ab",
  "version": 1,
  "startDate": 1423094400000,
  "timeOfDayStart": "Morning",
  "endDate": 1423180800000,
  "timeOfDayEnd": "Afternoon",
  "status": "ToProcess",
  "reason": "PublicTransportProblem",
  "commentary": null,
  "intern": {
    "user": {
      "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
    }
  }
}
```


## `PUT /declaredAbsences/:absence`

Édite une absence *déclarée*.

**Autorisation:**
- Stagiaire

### Exemple de requête
```bash
curl -X "PUT" "http://localhost:9000/api/1/declaredAbsences/2c16b196-b4ce-444e-9af8-cc32842742b5" \
  -H "Authorization: Bearer 5ef7235cb392ea3eff07d70336a01e08" \
  -H "Content-Type: application/json" \
  -d $'{
    "id": "2c16b196-b4ce-444e-9af8-cc32842742b5",
    "version": 3,
    "startDate": 1423090800000,
    "timeOfDayStart": "Morning",
    "endDate": 1423177200000,
    "timeOfDayEnd": "Afternoon",
    "status": "Accepted",
    "reason": "Death",
    "commentary": "Mon poisson rouge est mort. Sniff :(",
    "intern": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
  }'
```

### Exemple de réponse
```json
{
  "id": "2c16b196-b4ce-444e-9af8-cc32842742b5",
  "version": 4,
  "startDate": 1423090800000,
  "timeOfDayStart": "Morning",
  "endDate": 1423177200000,
  "timeOfDayEnd": "Afternoon",
  "status": "Accepted",
  "reason": "Death",
  "commentary": "Mon poisson rouge est mort. Sniff :(",
  "intern": {
    "user": {
      "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
    }
  }
}
```

## `DELETE /declaredAbsences/:absence`

Supprime une absence *déclarée*.

**Authorisation:**
- Staff
- Président

### Exemple de requête
```bash
curl -X "DELETE" "http://localhost:9000/api/1/declaredAbsences/6e133630-ef40-4c23-a885-da832ef7f599" \
  -H "Authorization: <Authorization>"

```

### Réponse

En cas de succès, la réponse a le statut `204 - No Content`.

## `PATCH /declaredAbsences/:absence/status`

Change le statut d'une absence *déclarée*.

**Authorisation:**
- Administrateur 
- Staff
- Président

### Éléments du JSON

| Nom | Type | Requis? | Description |
| --- | :---: | :---: | --- |
| status | `DeclaredAbsenceStatus` | `requis` | Statut de l'absence *déclarée* |

DeclaredAbsenceStatus:
- ToProcess (Default)
- Accepted
- Refused

### Exemple de requête
```bash
curl -X "PATCH" "http://localhost:9000/api/1/declaredAbsences/2c16b196-b4ce-444e-9af8-cc32842742b5/status" \
  -H "Authorization: <Authorization>" \
  -H "Content-Type: application/json" \
  -d $'{
  "status": "Accepted"
}'
```

### Exemple de réponse
```json
{
  "id": "2c16b196-b4ce-444e-9af8-cc32842742b5",
  "version": 3,
  "startDate": 1423090800000,
  "timeOfDayStart": "Morning",
  "endDate": 1423177200000,
  "timeOfDayEnd": "Afternoon",
  "status": "Accepted",
  "reason": "Death",
  "commentary": "Mon poisson rouge est mort.",
  "intern": {
    "user": {
      "id": "f14c7c52-c5d7-4351-9dad-ed5ab969db08"
    }
  }
}
```

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png