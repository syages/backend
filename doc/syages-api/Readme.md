# Documentation API Syages [[EN]](en/Readme.md)

***

## Versions et Type MIME

La version stable courante est la version `v1`.

Vous pouvez acceder à l'API via:  
`https://hostname/api/v1/` suivi de l'endpoint de la requête

Les données envoyées ou reçues doivent être formatées en `JSON`.

## Pagination 

L'API utilise plusieurs entêtes pour vous informer sur la pagination:

- Entête `Link`:
	Il retourne entre deux et 4 liens:
		- `first` vous indique où se trouve la première page.
		- `next` vous indique où se trouve la prochaine page.
		- `prev` vous indique où se trouve la page précédente.
		- `last` vous indique où se trouve la dernière page.

- Entête `Content-Range`: 
	Cette entête vous informe sur le rang des données retournés ainsi que le nombre total de données, sous la forme suivante : `début-fin/total`.

- Entête `X-Total-Count`:
	Cette entête vous indique le nombre total d'éléments qu'il y a dans la base de données.

## Parametres de requètes

Vous pouvez ajouter plusieurs paramètres dans votre requête selon les cas.  
Les paramètres `sort`, `limit`, et `offset` peuvent être utilisé lors de demande de listes.  
Le paramêtre `field` peut être utilisé dans toutes les requêtes `GET` pour sélectionner les champs que vous voulez recevoir dans le JSON.

### Champs
Vous pouvez utiliser la query `fields` pour ne récupérer que les champs qui vous interessent.

```bash
curl -X "GET" "http://localhost:9000/users?fields=(lastName,firstName)"
```

### Tri

Nous mettons à votre disposition le paramêtre de requêtes `sort` pour nous indiquer comment doivent être triés les résultats dans la réponse.
Pour indiquant un ordre descendant, il suffit d'ajoute un `-` devant l'attribut visé.
Par exemple, pour obtenir un liste triée par date décroissante puis nom croissant, vous écrirez : `sort=-date,name`.

### Limite et Décalages

Nous prenons en compte les paramêtres `limit` et `offset` pour représenter la pagination. 
- `limit` : nombre d'éléments dans votre résultats.
- `offset` : page à partir de laquelle on sélectionne les résultats.

Par exemple, pour obtenir les éléments entre les positions **15** et **30**, vous écrirez : `limit=15&offset=1`.

## Limitation des requêtes

<!-- TODO -->

## Erreurs

Toutes les erreurs suivent le format suivant et leur réponse utilise le code Http correspondant.  

```json
{
	"code": 2054,
	"message": "Validation Error",
	"description": "Le prénom ne doit contenir que des lettres."
}
```

Le `code` est unique à une erreur. Il est ainsi possible de chercher plus d'informations sur celle-ci dans la doc.


## Type de réponses

### GET

En cas d'erreur:
- `204 - Not Found` : la ressource n'existe pas.

En cas de succès:
- `200 - Ok`

### POST

En cas d'erreur:
- `204 - Not Found` : la ressource n'existe pas.
- `400 - BadRequest` : la requete n'est pas valide.

En cas de succès:
- `201 - Created`

### PUT

En cas d'erreur:
- `204 - Not Found` : la ressource n'existe pas.
- `400 - BadRequest` : la requete n'est pas valide.

En cas de succès:
- `200 - Ok`

### DELETE

En cas d'erreur:
- `204 - Not Found` : la ressource n'existe pas.
- `400 - BadRequest` : la requete n'est pas valide.

En cas de succès:
- `200 - Ok`

### PATCH

En cas d'erreur:
- `204 - Not Found` : la ressource n'existe pas.
- `400 - BadRequest` : la requete n'est pas valide.

En cas de succès:
- `200 - Ok`


## [Authentification](v1/oauth.md)

Cette API utilise le protocole [OAuth 2.0](http://oauth.net).
Il existe différents workflow permettant l'obtention un jeton d'accès. Pour savoir quel workflow utiliser, rendez-vous sur la [page de documentation de la partie OAuth](v1/oauth.md#workflows)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /oauth/auth](v1/oauth.md#get-auth) | Endpoint /auth | ![][ok] |
| [GET /oauth/revoke](v1/oauth.md#get-revoke) | Révoquer un jeton d'accès | ![][ok] |
| [GET /oauth/token](v1/oauth.md#get-token) | Endpoint /token | ![][ok] |
| [GET /oauth/validate](v1/oauth.md#get-validate) | Vérifier la validité d'un jeton d'accès | ![][ok] |

***

## [Matières](v1/courses.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /courses](v1/courses.md#get-courses) | R&eacute;cup&eacute;rer une liste de mati&egrave;res | ![][ok] ![][sec] |
| [GET /courses/:course](v1/courses.md#get-coursescourse) | R&eacute;cup&eacute;rer une mati&egrave;re par son id | ![][ok] ![][sec] |
| [POST /courses](v1/courses.md#post-courses) | Cr&eacute;er une nouvelle mati&egrave;re | ![][ok] ![][sec] |
| [PUT /courses/:course](v1/courses.md#put-coursescourse) | &Eacute;diter une mati&egrave;re | ![][ok] ![][sec] |
| [DELETE /courses/:course](v1/courses.md#delete-coursescourse) | Supprimer une mati&egrave;re | ![][ok] ![][sec] |


## [Absences déclarées](v1/declaredAbsences.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /declaredAbsences](v1/declaredAbsences.md#get-declaredabsences) | Récupérer une liste d'absences *déclarées* | ![][ok] ![][sec] |
| [GET /declaredAbsences/:absence](v1/declaredAbsences.md#get-declaredabsencesabsence) | Récupérer une absence *déclarée* par son `id` | ![][ok] ![][sec] |
| [POST /declaredAbsences](v1/declaredAbsences.md#post-declaredabsences) | Créer une absence *déclarée* | ![][ok] ![][sec] |
| [PUT /declaredAbsences/:absences](v1/declaredAbsences.md#put-declaredabsencesabsence) | Éditer une absence *déclarée* | ![][ok] ![][sec] |
| [DELETE /declaredAbsences/:absences](v1/declaredAbsences.md#delete-declaredabsencesabsence) | Supprimer une absence *déclarée* | ![][ok] ![][sec] |
| [PATCH /declaredAbsences/:absences/status](v1/declaredAbsences.md#patch-declaredabsencesabsencestatus) | Changer le status d'une absence *déclarée* | ![][ok] ![][sec] |

## [Administrateurs](v1/administrators.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /administrators](v1/administrators.md#get-administrators) | Récupérer la liste des administrateurs | ![][ok] ![][sec] |
| [GET /administrators/:administrator](v1/administrators.md#get-administratorsadministrator) | Récupérer un administrateur | ![][ok] ![][sec] |
| [POST /administrators](v1/administrators.md#post-administrators) | Créer un nouvel administrateur | ![][ok] ![][sec] |
| [PUT /administrators/:administrator](v1/administrators.md#put-administratorsadministrator) | Éditer un administrateur | ![][ok] ![][sec] |
| [DELETE /administrators/:administrator](v1/administrators.md#delete-administratorsadministrator) | Supprimer un administrateur | ![][ok] ![][sec] |


## [Diplomas](v1/diplomas.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /diplomas](v1/diplomas.md#get-diplomas) | Retourne une liste de formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma](v1/diplomas.md#get-diplomasdiploma) | Retourne une formation par son `id` | ![][ok] ![][sec] |
| [POST /diplomas](v1/diplomas.md#post-diplomas) | Créer une formation | ![][ok] ![][sec] |
| [PUT /diplomas/:diploma](v1/diplomas.md#put-diplomasdiploma) | Éditer une formation | ![][ok] ![][sec] |
| [DELETE /diplomas/:diploma](v1/diplomas.md#delete-diplomasdiploma) | Supprimer une formation | ![][ok] ![][sec] |
| [PATCH /diplomas/:diploma/status](v1/diplomas.md#patch-diplomasdiplomastatus) | Changer le statut d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/interns](#get-diplomasdiplomainterns) | Récupérer la liste des stagiaire d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/lessons](#get-diplomasdiplomalessons) | Récupérer la liste des leçons d'une formation | ![][ok] ![][sec] |
| [GET /diplomas/:diploma/periods](#get-diplomasdiplomaperiods) | Récupérer la liste des périodes d'une formation | ![][ok] ![][sec] |


## [Domains](v1/domains.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /domains](v1/domains.mdv1/domains.mdv1/domains.md#get-domains) | Récupérer une liste de domaines | ![][ok] ![][sec] |
| [GET /domains/:domain](v1/domains.mdv1/domains.md#get-domainsdomain) | Récupérer un domaine par son `id` | ![][ok] ![][sec] |
| [POST /domains](v1/domains.md#post-domains) | Créer un nouveau domaine | ![][ok] ![][sec] |
| [PUT /domains/:domain](v1/domains.md#put-domainsdomain) | Éditer un domaine | ![][ok] ![][sec] |
| [DELETE /domains/:domain](v1/domains.md#delete-domainsdomain) | Supprimer un domaine | ![][ok] ![][sec] |


## [Evaluations](v1/evaluations.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /evaluations](v1/evaluations.md#get-diplomasdiplomalessonslessonevaluations) | Récupérer une liste d'évaluations | ![][ok] ![][sec] |
| [GET /evaluations/:evaluation](v1/evaluations.md#get-evaluationsevaluation) | Récupérer une évaluation | ![][ok] ![][sec] |
| [POST /evaluations](v1/evaluations.md#post-evaluations) | Créer une nouvelle évaluation | ![][ok] ![][sec] |
| [PUT /evaluations/:evaluation](v1/evaluations.md#put-evaluationsevaluation) | Éditer une évaluation | ![][ok] ![][sec] |
| [DELETE /evaluations/:evaluation](v1/evaluations.md#delete-evaluationsevaluation) | Supprimer une évaluation | ![][ok] |
| [GET /evaluations/:evaluation/marks](v1/evaluations.md#get-evaluationsevaluationmarks) | Récupérer les notes d'une évaluation| ![][ok] ![][sec] |
| [POST /evaluations/:evaluation/marks](v1/evaluations.md#post-evaluationsevaluationmarks) | Créer les notes d'une évaluation | ![][ok] ![][sec] |
| [PUT /evaluations/:evaluation/marks](v1/evaluations.md#put-evaluationsevaluationmarks) | Éditer les notes d'une évaluation | ![][ok] ![][sec] | 


## [Informations](v1/informations.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /informations](v1/informations.md#get-informations) | Récupérer une liste de messages  | ![][ok] ![][sec] |
| [GET /informations/:info](v1/informations.md#get-informationsinfo) | Récupérer un message par son `id` | ![][ok] ![][sec]  |
| [POST /informations](v1/informations.md#post-informations) | Créer un nouveau message | ![][ok] ![][sec] |
| [PUT /informations/:info](v1/informations.md#put-informationsinfo) | Éditer un message | ![][ok] ![][sec] |
| [DELETE /informations/:info](v1/informations.md#delete-informationsinfo) | Supprimer un message | ![][ok] ![][sec] |


## [Interns](v1/interns.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /interns ](v1/interns.md#get-interns)| Récupérer une liste de stagiaires | ![][ok] ![][sec] | 
| [GET /interns/:intern](v1/interns.md#get-internsintern)| Récupérer un stagiaire par son `id` | ![][ok] ![][sec] |
| [POST /interns](v1/interns.md#post-interns) | Créer un nouveau stagiaire | ![][ok] ![][sec] |
| [PUT /interns/:intern](v1/interns.md#put-internsintern) | Éditer un stagiaire | ![][ok] ![][sec] |
| [DELETE /interns/:intern](v1/interns.md#delete-internsintern) | Supprimer un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/isFirst](v1/interns.md#get-internsinternisfirst) | Récupérer si il s'agit de la 1ere connexion d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/infos](v1/interns.md#get-internsinterninfos) | Récupérer les informations de profil d'un stagiaire | ![][ok] ![][sec] |
| [POST /interns/intern/infos](v1/interns.md#post-internsinterninfos) | Ajouter des informations de profil à un stagiaire | ![][ok] ![][sec] |
| [PUT /interns/:intern/infos](v1/interns.mdv1/interns.mdv1/interns.md#put-internsinterninfos) | Éditer les informations de profil d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/lessons](v1/interns.mdv1/interns.md#get-internsinternlessons) | Récupérer la liste des leçons d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/marks](v1/interns.md#get-internsinternmarks) | Récupérer la liste des notes d'un stagiaire | ![][ok] ![][sec] |
| [GET /interns/:intern/notedAbsences](v1/interns.md#get-internsinternnotedabsences) | Récupérer une liste d'absences *notées* d'un stagiaire| ![][ok] ![][sec] |
| [GET /interns/:intern/declaredAbsences](v1/interns.md#get-internsinterndeclaredabsences) | Récupérer une liste d'absence *déclarée* d'un stagiaire| ![][ok] ![][sec] |
| [GET /me/notedAbsences](v1/interns.md#get-menotedabsences) | Récupérer une liste d'absences *notées* d'un stagiaire| ![][ok] ![][sec] |
| [GET /me/declaredAbsences](v1/interns.md#get-medeclaredabsences) | Récupérer une liste d'absence *déclarée* d'un stagiaire| ![][ok] ![][sec] |


## [Lectures](v1/lectures.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /lectures](v1/lectures.md#get-lectures)| Récupérer une liste de lectures | ![][ok] ![][sec] |
| [GET /lectures/:lecture](v1/lectures.md#get-lectureslecture)| Récupérer une Lecture par son `id` | ![][ok] ![][sec] |
| [POST /lectures](v1/lectures.md#post-lectures)| Créer une nouvelle Lecture | ![][ok] ![][sec] |
| [PUT /lectures/:lecture](v1/lectures.md#put-lectureslecture)| Éditer une Lecture | ![][ok] ![][sec] |
| [DELETE /lectures/:lecture](v1/lectures.md#delete-lectureslecture)| Supprimer une Lecture | ![][ok] ![][sec] |
| [GET /lectures/:lecture/absences](v1/lectures.md#get-lectureslectureabsences) | Récupérer la liste des absences pour cette lecture | ![][ok] ![][sec] |
| [POST /lectures/:lecture/absences](v1/lectures.md#post-lectureslectureabsences)| Éditer les absences *notées* d'une Lecture | ![][ok] ![][sec] |


## [Le&ccedil;ons](v1/lessons.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /lessons](v1/lessons.md#get-lessons) | Récupérer la liste des leçons | ![][ok] ![][sec] |
| [GET /lessons/:lesson](v1/lessons.md#get-diplomasdiplomalessonslesson) | Récupérer un cours | ![][ok] ![][sec] |
| [POST /lessons](v1/lessons.md#post-lessons) | Créer une nouvelle leçon | ![][ok] ![][sec] | 
| [PUT /lessons/:lesson](v1/lessons.md#put-diplomasdiplomalessonslesson) | &Eacute;diter un cours | ![][ok] ![][sec] |
| [GET /lessons/:lesson/interns](v1/lessons.md#get-lessonslessoninterns) | Récupérer la liste des stagiaires d'un cours | ![][ok] ![][sec] |
| [GET /lessons/:lesson/lectures](v1/lessons.md#get-lessonslessonlectures) | Récupérer la liste des lectures d'une leçons | ![][ok] ![][sec] |
| [GET /lessons/:lesson/evaluations](v1/lessons.md#get-lessonslessonevaluations) | Récupérer la liste des évaluation d'une leçons | ![][ok] ![][sec] |


## [Notes](v1/marks.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /marks](#get-marks) | Récupérer une liste de notes | ![][ok] ![][sec] |
| [GET /marks/:marks](#get-marksmark) | Récupérer une note par son identifiant `:mark` | ![][ok] ![][sec] |
| [POST /marks](#post-marks) | Créer des notes pour une évaluation | ![][ok] ![][sec] |
| [PUT /marks/:mark](#put-marksmark) | Éditer une note | ![][ok] ![][sec] |
| [DELETE /marks/:mark](#delete-marksmark) | Supprimer une note | ![][ok] ![][sec] |

## [notedAbsences](v1/notedAbsences.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /notedAbsences](v1/notedAbsences.md#get-notedabsences) | Récupérer une liste d'absence | ![][ok] ![][sec] |
| [POST /notedAbsences](v1/notedAbsences.md#post-notedabsences) | Créer un nouvelle absence notée | ![][ok] ![][sec] |
| [PUT /notedAbsences/:absence](v1/notedAbsences.md#put-notedabsences) | Éditer un absence notée | ![][ok] ![][sec] |
| [DELETE /notedAbsences/:absence](v1/notedAbsences.md#delete-notedabsences) | Supprimer une absence notée | ![][ok] ![][sec] |
| [PATCH /notedAbsences/:absence/status](v1/notedAbsences.md#patch-notedabsencesabsencestatus) | Changer le statut d'une absence notée | ![][ok] ![][sec] |

## [Periods](v1/periods.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /periods](v1/periods.md#get-periods) | Récupérer la liste des périodes | ![][ok] ![][sec] |
| [GET /periods/:period](v1/periods.md#get-periodsperiod) | Récupérer une période par son identifiant `:period` | ![][ok] ![][sec] |
| [POST /periods](v1/periods.md#post-periods) | Créer une nouvelle période | ![][ok] ![][sec] |
| [PUT /periods/:period](v1/periods.md#put-periodsperiod) | Éditer une période existante | ![][ok] ![][sec] |
| [DELETE /periods/:period](v1/periods.md#delete-periodsperiod) | Supprimer une période existante | ![][ok] ![][sec] |
| [GET /periods/:period/evaluations](v1/periods.md#get-periodsperiodevaluations) | Récupérer la liste des évaluations d'une période | ![][ok] ![][sec] |

## [Présidents](v1/presidents.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /presidents](v1/presidents.md#get-presidents) | Récupérer la liste des présidents | ![][ok] ![][sec] |
| [GET /presidents/:president](v1/presidents.md#get-presidentspresident) | Récupérer un président | ![][ok] ![][sec] |
| [POST /presidents](v1/presidents.md#post-presidents) | Créer un nouveau président | ![][ok] ![][sec] |
| [PUT /presidents/:president](v1/presidents.md#put-presidentspresident) | Éditer un président | ![][ok] ![][sec] |
| [DELETE /presidents/:president](v1/presidents.md#delete-presidentspresident) | Supprimer un président | ![][ok] ![][sec] |

## [Rapports](v1/reports.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /reports](v1/reports.md#get-reports) | Récupérer une liste de rapports | ![][ok] ![][sec] |
| [GET /reports/closed](v1/reports.md#get-reportsclosed) | Récupérer une liste de rapport *fermés* | ![][ok] ![][sec] |
| [GET /reports/opened](v1/reports.md#get-reportsopened) | Récupérer une liste de rapport *ouverts* | ![][ok] ![][sec] |
| [GET /reports/:report](v1/reports.md#get-reportsreport) | Récupérer un rapport | ![][ok] ![][sec] |
| [POST /reports](v1/reports.md#post-reports) | Créer un nouveau rapport | ![][ok] ![][sec] |
| [PATCH /reports/:report](v1/reports.md#patch-reportsreport) | Éditer le statut d'un rapport | ![][ok] ![][sec] |
| [DELETE /reports/:report](v1/reports.md#delete-reportsreport) | Supprimer un rapport | ![][ok] ![][sec] |


## [Staff](v1/staff.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /staff](v1/staff.md#get-staff) | Récupérer une liste de membres du staff | ![][ok] ![][sec] |
| [GET /staff/:staff](v1/staff.md#get-staffstaff) | Récupérer un membre du staff | ![][ok] ![][sec] |
| [POST /staff](v1/staff.md#post-staff) | Créer un membre du staff | ![][ok] ![][sec] |
| [PUT /staff/:staff](v1/staff.md#put-staffstaff) | Éditer un membre du staff | ![][ok] ![][sec] |
| [DELETE /staff/:staff](v1/staff.md#delete-staffstaff) | Supprimer un membre du staff | ![][ok] ![][sec] |


## [Teachers](v1/teachers.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /teachers](v1/teachers.md#get-teachers) | Récupérer la liste des enseignants | ![][ok] |
| [GET /teachers/:teacher](v1/teachers.md#get-teachersteacher) | Récupérer un enseignants par son id | ![][ok] |
| [POST /teachers](v1/teachers.md#post-teachers) | Créer un nouvel enseignant | ![][ok] |
| [PUT /teachers/:teacher](v1/teachers.md#put-teachersteacher) | Editer un enseignant | ![][ok] |
| [PATCH /teachers/:teacher](v1/teachers.md#patch-teachersteacher) | Editer un enseignant *partiellement* | ![][ni] |
| [DELETE /teachers/:teacher](v1/teachers.md#delete-teachersteacher) | Supprimer un enseignant | ![][ok] |
| [GET /teachers/domains/:domain](v1/teachers.md#get-teachersdomainsdomain) | Liste d'enseignants pour un domain | ![][ok] |
| [GET /me/lessons]() | Liste des cours d'un enseignant | ![][ni] |


## [Users](v1/users.md)

| Endpoint | Description | Statut |
| --- | --- | --- |
| [GET /me](#get-me)| Récupérer l'utilisateur connecté | ![][ok] |
| [GET /users ](#get-users)| Récupérer une liste d'utilisateurs | ![][ok] |
| [GET /user/:user](#get-usersuser)| Récupérer un utilisateur par son id | ![][ok] |
| [GET /users/:user/picture](#get-usersuserpicture) | Récupérer l'image d'un utilisateur | ![][ok] |
| [POST /users/:user/picture](#post-usersuserpicture) | Changer l'image d'un utilisateur | ![][ok] |

[ok]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/2705.png
[ni]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/274c.png
[wip]: https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f6a7.png
[sec]:https://raw.githubusercontent.com/twitter/twemoji/gh-pages/16x16/1f512.png