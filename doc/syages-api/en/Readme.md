# Syages API Documentation

***

## API Versions and MIME Types

The current stable API version is v1.

You can access the api through

`/api/v1`

## Paging 

We use the `Link` header to inform you about paging. We also use a custom header `x-total-count` to give you the total count of elements in a table in the database.

## Query Parameters

You can use query parameters to `sort`, filter by `field`, or search inside the results.
For list results, you can use `limit` and `offset` to choose a specific range for your data.

## Rate Limits

<!-- TODO -->

## Errors

All error responses are in the following format, delivered with the corresponding status code:
```json
{
	"code": 2054,
	"message": "Validation Error",
	"description": "The first name of an user can't contain number"
}
```
The `code` will be a specific code for each type of error, so you can look it up in the doc to find a more specifi explanation.

## Authentification

We use OAuth ...


***

## [Absences](v1/absences.md)

| Endpoint | Description |
| --- | --- |
| [GET /interns/:intern/absences](v1/absences.md#get-absences) | Get a list of absences of an intern |
| [GET /interns/:intern/absences/:absence](v1/absences.md#get-absencesabsence) | Get an absence by its id |
| [POST /interns/:intern/absences](v1/absences.md#post-absences) | Create a new absence for an intern  |
| [PUT /interns/:intern/absences/:absence](v1/absences.md#put-absencesabsence) | Edit an absence |
| [DELETE /interns/:intern/absences/:absence](v1/absences.md#delete-absencesabsence) | Delete an absence |


## [Courses](v1/courses.md)

| Endpoint | Description |
| --- | --- |
| [GET /courses](v1/courses.md#get-courses) | Get courses list |
| [GET /courses/:course](v1/courses.md#get-coursescourse) | Get a course by its id |
| [POST /courses](v1/courses.md#post-coursescourse) | Create a new course |
| [PUT /courses/:course](v1/courses.md#put-coursescourse) | Edit a course |
| [DELETE /courses/:course](v1/courses.md#delete-coursescourse) | Delete a course |


## [Diplomas](v1/diplomas.md)

| Endpoint | Description |
| --- | --- |
| [GET /diplomas](v1/diplomas.md#get-diplomas) | Get diplomas list |
| [GET /diplomas/:diploma](v1/diplomas.md#get-diplomasdiploma) | Get a diploma by its id |
| [POST /diplomas](v1/diplomas.md#post-diplomasdiploma) | Create a new diploma |
| [PUT /diplomas/:diploma](v1/diplomas.md#put-diplomasdiploma) | Edit a diploma  |
| [DELETE /diplomas/:diploma](v1/diplomas.md#delete-diplomasdiploma) | Delete a diploma |


## [Domains](v1/domains.md)

| Endpoint | Description |
| --- | --- |
| [GET /domains](v1/domains.md#get-domains) | Get domains list |
| [GET /domains/:id](v1/domains.md#get-domainsdomain) | Get a domain by its id |
| [POST /domains](v1/domains.md#post-domainsdomain) | Create a new domain |
| [PUT /domains/:domain](v1/domains.md#put-domainsdomain) | Edit a domain |
| [DELETE /domains/:domain](v1/domains.md#delete-domainsdomain) | Delete a domain |


## [Evaluations](v1/evaluations.md)

| Endpoint | Description |
| --- | --- |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations](v1/evaluations.md#get-evaluations) | Get a list of evaluation |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](v1/evaluations.md#get-evaluationsid) | Get an evaluation by its id  |
| [POST /diplomas/:diploma/lessons/:lesson/evaluations](v1/evaluations.md#post-evaluationsnew) | Create a new evaluation |
| [PUT /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](v1/evaluations.md#put-evaluationsid) | Edit an evaluation |
| [DELETE /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](v1/evaluations.md#delete-evaluationsid) | Delete an evaluation |

## [Marks](v1/marks.md)

| Endpoint | Description |
| --- | --- |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation/marks](v1/marks.md#get-marks)| Get a list of marks |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation/marks/mark](v1/marks/.md#get-marksmark) | Get a mark by its id |
| [POST /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation/marks](v1/marks.md#post-marks) | Post a list of marks |
| [PUT /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation/marks/:mark](v1/marks.md#put-marksmark) | Edit a list of marks   |
| [DELETE /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation/marks/:mark](v1/marks.md#delete-marksmark) | Delete a list of marks |

## [Interns](v1/interns.md)

| Endpoint | Description |
| --- | --- |
| [GET /interns](v1/interns.md#get-interns) | Get interns list |
| [GET /interns/:intern](v1/interns.md#get-internsintern) | Get an intern by its id |
| [POST /interns](v1/interns.md#post-interns)| Create a new intern |
| [PUT /interns/:intern](v1/interns.md#put-internsintern) | Edit an intern |
| [DELETE /interns/:intern](v1/interns.md#delete-internsintern) | Delete an intern |
| [GET /interns/:intern/lessons](v1/interns.md#get-internsinternlessons) | Get list of DiplomaCourse of an intern |
| [GET /interns/:intern/lessons/:lesson/marks](v1/interns.md#get-internsinternlessonslessonmarks) | Get a list of marks of a DiplomaCourse of an intern |

## [Lessons](v1/lessons.md)

| Endpoint | Description |
| --- | --- |
| [GET /diplomas/:diploma/lessons](v1/lessons.md#get-diplomasdiplomalessons) | Get a list of lessons inside a Diploma |
| [GET /diplomas/:diploma/lessons/:lesson ](v1/lessons.md#get-diplomasdiplomalessonslesson)| Get a lesson inside a Diploma |
| [PUT /diplomas/:diploma/lessons/:lesson](v1/lessons.md#put-diplomadiplomalessonslesson) | Edit a lesson |

## [Messages](v1/messages.md)

| Endpoint | Description |
| --- | --- |
| [GET /messages](v1/messages.md#get-messages) | Get a list of messages |
| [GET /messages/:message](v1/messages.md#get-messagesid) | Get a message by its id |
| [POST /messages](v1/messages.md#post-messages) | Create a new message |

## [OAuth](v1/oauth.md)
 
| Endpoint | Description |
| --- | --- |
| | |

## [Periods](v1/periods.md)

| Endpoint | Description |
| --- | --- |
| [GET /diplomas/:diploma/periods](v1/periods.md#get-diplomasdiplomaperiods) | Get a list of periods inside a Diploma |
| [GET /diplomas/:diploma/periods/:period](v1/periods.md#get-diplomasdiplomaperiodperiods) | Get a period inside a Diploma |
| [PUT /diplomas/:diploma/periods/:period](v1/periods.md#put-diplomasdiplomaperiodperiods) | Edit a period |

## [Staff](v1/staff.md)

| Endpoint | Description |
| --- | --- |
| [GET /staff](v1/staff.md#get-staff) | Get staff list |
| [GET /staff/:staff](v1/staff.md#get-staffstaff) | Get a staff by its id |
| [POST /staff](v1/staff.md#post-staff) | Create a new staff |
| [PUT /staff/:staff](v1/staff.md#put-staffstaff) | Edit a staff |
| [DELETE /staff/:staff](v1/staff.md#delete-staffstaff) | Delete a staff |


## [Teachers](v1/teachers.md)

| Endpoint | Description |
| --- | --- |
| [GET /teachers](v1/teachers.md#get-teachers) | Get teachers list |
| [GET /teachers/:teacher](v1/teachers.md#get-teachersteacher) | Get a teacher by its id |
| [POST /teachers](v1/teachers.md#post-teachers) | Create a new teacher |
| [PUT /teachers/:teacher](v1/teachers.md#put-teachersteacher) | Edit a teacher |
| [DELETE /teachers/:teacher](v1/teachers.md#delete-teachersteacher) | Delete a teacher |


## [Users](v1/users.md)

| Endpoint | Description |
| --- | --- |
| [GET /me](v1/users.md#get-me) | Get logged user |
| [GET /users ](v1/users.md#get-users) | Get a list of users|
| [GET /users/:user](v1/users.md#get-userid) | Get a user by its id |






















