# Teachers

***

| Endpoint | Description |
| --- | --- |
| [GET /teachers](#get-teachers) | Get the list of teachers |
| [GET /teachers/:id](#get-teachersid) | Get a teacher by his id  |
| [POST /teachers](#post-teachers) | Create a new teacher |
| [PUT /teachers/:id](#put-teachersid) | Edit a teacher |
| [PATCH /teachers/:id](#patch-teachersid) | Edit a teacher **partially** |
| [DELETE /teachers/:id](#delete-teachersid) | Delete a teacher |

## `GET /teachers`

Returns a list of teachers

### Parameters

| Name   | Required? | Type    | Description                                                        |
| ------ |:---------:|:-------:| ------------------------------------------------------------------ |
| limit  | Optional  | Integer | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | Optional  | Integer | Object offset for pagination. Default is 0.                        |

### Example Request

```bash
```

### Example Response

```json
```

## `GET /teachers/:id`

Returns a teacher by his id

### Example Request

```bash
```

### Example Response

```json

```

## `POST /teachers`

Create a new teacher

### Json Elements

| Name | Type | Required? | Description |
| --- |:---:|:---:| --- |
| firstName | String | Required | firstName of the teacher |
| lastName| String | Required | lastName of the teacher | 
| email | String/Email | Required | email of the teacher |
| office | String | Optional | office of the teacher |
| number | String | Optional | office phone number |
| domains | List&lt;Domain&gt; | Optional | List of domain taught by the teacher | 

### Example Request

```bash
```

### Example Response

```json
```

## `PUT /teachers/:id`

Edit a teacher

*Be sure to return a full Teacher Object*

### Example Request

```bash
```

### Example Response

```json
```

## `PATCH /teachers/:id`

Edit a teacher partially

### Example Request

```bash
```

### Example Response

```json
```

## `DELETE /teachers/:id`

Delete a teacher

### Example Request

```bash
```

### Example Response

```json
``
