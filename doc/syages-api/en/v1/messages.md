# Messages

***

| Endpoint                                       | Description             |
| ---------------------------------------------- | -----------------------:|
| [GET /api/1/messages](#get-messages)           | Get a list of messages  |
| [GET /api/1/message/:message](#get-messagesid) | Get a message by its id |
| [POST /api/1/message/new](#post-messagesnew)   | Create a new message    |

## `GET /api/1/messages`

## `GET /api/1/message/:message`

## `POST /api/1/message/new`

## Parameters

| Name   | Required? | Type   | Description                         |
| ------ |:---------:|:------:| -----------------------------------:|
| title  | Yes       | String | Title of the message                |
| author | Yes       | User   | User object representing the author |
| post   | Yes       | Text   | Content of the message              |

## Example Request
```bash
curl -X "POST" "http://localhost:9000/api/1/message/new" \
	-H "Content-Type: application/json" \
	-d $'{
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015",
  "title": "Information d\'inscription",
  "author": {"email":"bidon.aurelien@gmail.com","firstName":"Aurelien","lastName":"Bidon","id":21,"flagType":"Administrative"}}'
```

## Example Response
```json
{
  "id": 21,
  "post": "Bonjour, Je souhaite vous informer que les inscriptions seront ouvertes à partir du 15 mai 2015",
  "title": "Information d'inscription",
  "author": {
    "email": "bidon.aurelien@gmail.com",
    "firstName": "Aurelien",
    "lastName": "Bidon",
    "id": 41,
    "flagType": "Administrative"
  }
}
```

