# Domains

***

| Endpoint | Description |
| --- | --- |
| [GET /domains](#get-domains) | Get domains list |
| [GET /domains/:domain](#get-domainsdomain) | Get a domain by his id |
| [POST /domains](#post-domains) | Create a new domain |
| [PUT /domains/:domain](#put-domainsdomain) | Edit a domain |
| [DELETE /domains/:domain](#delete-domainsdomain) | Delete a domain |

## `GET /domains`

Returns domains list

### Parameters

| Name   | Type    | Required? | Description |
| --- |:---:|:---:| --- |
| limit | `Integer` | `optional` | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | `Integer` | `optional` | Object offset for pagination. Default is 0. |

### Example Request
```bash
```

### Example Response
```json
```

## `GET /domains/:domain`

Returns an domain by his id

### Example Request
```bash
```

### Example Response
```json
```

## `POST /domains`

Create a new domain

### Json Elements

| Name | Type | Required? | Description |
| --- | :---: | :---: | --- |
| name | `String` | `Required` | Name of the domain |

### Example Request
```bash
```

### Example Response
```json

```

## `PUT /domains/:domain`

Edit a domain

*Be sure to return a full domain Object*

### Example Request
```bash
```

### Example Response
```json

```

## `DELETE /domains/:domain`

Delete a domain

### Example Request
```bash

```

### Example Response
```json

```
