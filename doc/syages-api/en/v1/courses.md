# Courses

***

| Endpoint | Description |
| --- | ---:|
| [GET /courses](#get-courses) | Get courses list |
| [GET /courses/:course](#get-coursescourse) | Get a course by his id  |
| [POST /courses](#post-courses) | Create a new course |
| [PUT /courses/:course](#put-coursescourse) | Edit a course |
| [DELETE /courses/:course](#delete-coursesid) | Delete a course |

## `GET /courses`

Returns courses list

### Parameters

| Name | Type  | Required? | Description |
| --- |:---:|:---:| --- |
| limit  | `Integer` | `optional` | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | `Integer` | `optional` | Object offset for pagination. Default is 0. |

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/courses"
```

### Example Response
```json
```

## `GET /courses/:course`

Returns an course by his id

### Example Request
```bash
```

### Example Response
```json
```

## `POST /courses`

Create a new course

### Json Elements

### Example Request
```bash
```

### Example Response
```json
```

## `PUT /courses/:course`

Edit a course
*Be sure to return a full course Object*

### Example Request
```bash
```

### Example Response
```json
```

## `DELETE /course/:id`

Delete a course

### Example Request
```bash
```

### Example Response
```json
``
