# Users

***

| Endpoint                           | Description            |
| ---------------------------------- | ----------------------:|
| [GET /me](#get-me)                 | Get logged user object |
| [GET /users ](#get-users)          | Get user list          |
| [GET /user/:id](#get-userid)       | Get a user by his id   |
| [DELETE /user/:id](#delete-userid) | Delete a user          |

## `GET /me`

Returns a user object
*__Authenticated__*

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/me" \
	-H "Authorization: Bearer 9d6bfa28f7bdebb9455fabd303653d66"
```
### Example Response

```json
{
  "email": "timo.b35@gmail.com",
  "firstName": "Timothée",
  "lastName": "Barbot",
  "id": 182,
  "flagType": "Administrative"
}
```

## `GET /users`

Returns users list

### Parameters

| Name   | Required? | Type    | Description                                                        |
| ------ |:---------:|:-------:| ------------------------------------------------------------------:|
| limit  | optional  | integer | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | optional  | integer | Object offset for pagination. Default is 0.                        |

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/users"
```

### Example Response
```json
[
  {
    "email": "bidon.aurelien@gmail.com",
    "firstName": "Aurelien",
    "lastName": "Bidon",
    "id": 61,
    "flagType": "Administrative"
  },
  {
    "email": "timo.b35@gmail.com",
    "firstName": "Timothée",
    "lastName": "Barbot",
    "id": 62,
    "flagType": "Administrative"
  }
]
```

## `GET /user/:id`

Returns a user by his id

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/user/22"
```

### Example Response
```json
{
  "email": "timo.b35@gmail.com",
  "firstName": "Timothée",
  "lastName": "Barbot",
  "id": 22,
  "flagType": "Administrative"
}
```

## `DELETE /user/:id`

Delete a user

### Example Request
```bash
curl -X "DELETE" "http://localhost:9000/api/1/user/22"
```

### Example Response
```json
{
  "success": "user with id 22 deleted."
}
```