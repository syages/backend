# DiplomaCourses

***

| Endpoint                                                                          | Description                                   |
| --------------------------------------------------------------------------------- | ---------------------------------------------:|
| [GET /diploma/:diploma_id/courses](#get-me)                                       | Get a list of DiplomaCourses inside a Diploma |
| [GET /diploma/:diploma_id/course/:course_id ](#get-users)                         | Get a DiplomaCourse inside a Diploma          |
| [PUT /diploma/:diploma_id/course/:course_id](#put-diplomadiplomaidcoursecourseid) | Edit a DiplomaCourse                          |

## `GET /diploma/:diploma_id/courses`

Returns a list of DiplomaCourses inside a Diploma

### Parameters

| Name      | Required ? | Type | Description                     |
| --------- |:----------:|:----:| -------------------------------:|
| diplomaId | Yes        | long | An id that refers to a diploma. |
| courseId  | Yes        | long | An id that refers to a course.  |

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/diploma/12/courses"
```

### Example Response
```json
```

## `GET /diploma/:diploma_id/course/:course_id`

Returns a DiplomaCourse

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/diploma/12/course/42"
```

### Example Response
```json
```

## `PUT /diploma/:diploma_id/course/:course_id`

Edit a DiplomaCourse

### Json Elements

| Name          | Type        | Description                                   |
| ------------- |:-----------:| ---------------------------------------------:|
| course        | Course      | Course used for this DiplomaCourse            |
| teacher       | TeacherUser | Teacher for the course                        |
| optional      | boolean     | Is the course optional ?                      |
| coefficient   | Float       | Coefficient of the course on the general mark |
| validationMoy | Float       | Minimum mark to pass the course               |

### Example Request
```bash
curl -X "PUT" "http://localhost:9000/api/1/diploma/12/course/42"
```

### Example Response

