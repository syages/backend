# Interns

***

| Endpoint | Description |
| --- | --- |
| [GET /interns ](#get-interns)| Get interns list |
| [GET /interns/:intern](#get-internsintern)| Get an intern by his id |
| [POST /interns](#post-interns) | Create a new intern |
| [PUT /interns/:intern](#put-internsintern) | Edit an intern |
| [DELETE /interns/:intern](#delete-internsintern) | Delete an intern |
| [GET /interns/:intern/lessons](#get-internsinternclessons) | Get list of Lesson of an intern |
| [GET /interns/:intern/lesson/:lesson/marks](#get-internsinternlessonslessonmarks) | Get a list of marks of a Lesson of an intern |

## `GET /interns`

Returns interns list

### Parameters

| Name | Required? | Type | Description |
| --- |:---:|:---:| ---|
| limit | optional | integer | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | optional | integer | Object offset for pagination. Default is 0. |

### Example Request

```bash
```

### Example Response

```json
```

## `GET /interns/:intern`

Returns an intern by his id

### Example Request

```bash
```

### Example Response

```json
```

## `POST /interns`

Create a new intern

### Json Elements

| Name | Type | Required? | Description |
| --- | :---: | :---: | --- |
| | | | |


### Example Request

```bash
```

### Example Response

```json
```

## `PUT /interns/:intern`

Edit an intern
*Be sure to return a full intern Object*

### Example Request

```bash
```

### Example Response

```json
```

## `DELETE /interns/:intern`

Delete an intern

### Example Request

```bash
```

### Example Response

```json
```

## `GET /interns/:intern/lessons`

Get list of Lesson of an intern

### Example Request

```bash
```

### Example Response

```json
```

## `GET /interns/:intern/lessons/:lesson/marks`

Get a list of marks of a Lesson of an intern

### Example Request

```bash
```

### Example Response

```json
```