# EvaluationMarks

***

| Endpoint 																			         | Description	          |
| ------------------------------------------------------------------------------------------ | ----------------------:|
| [GET /diploma/:diploma/course/:course/evaluation/:evaluation/marks](#get-marks)            | Get a list of marks    |
| [POST /diploma/:diploma/course/:course/evaluation/:evaluation/mark/new](#post-marksnew)    | Post a list of marks   |
| [PUT /diploma/:diploma/course/:course/evaluation/:evaluation/mark/:id](#put-marksid)       | Edit a list of marks   |
| [DELETE /diploma/:diploma/course/:course/evaluation/:evaluation/mark/:id](#delete-marksid) | Delete a list of marks |

## `GET /diploma/:diploma/course/:course/evaluation/:evaluation/marks`

Returns a list of marks

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/diploma/1/course/10/evaluation/3/marks"
```

### Example Response
```json
```

## `POST /diploma/:diploma/course/:course/evaluation/:evaluation/mark/new`

Post a new list of marks

### Json Elements

### Example Request
```bash
curl -X "POST" "http://localhost:9000/api/1/diploma/1/course/10/evaluation/3/mark/new"
```

### Example Response
```json
```

## `PUT /diploma/:diploma/course/:course/evaluation/:evaluation/mark/:id`

Edit a list of marks
*Be sure to return a full evaluationMark Object*

### Example Request
```bash
curl -X "PUT" "http://localhost:9000/api/1//diploma/1/course/10/evaluation/3/mark/1"
```

### Example Response
```json
```

## `DELETE /diploma/:diploma/course/:course/evaluation/:evaluation/mark/:id`

Delete a list of marks

### Example Request
```bash
curl -X "DELETE" "http://localhost:9000/api/1/diploma/1/course/10/evaluation/3/mark/1"
```

### Example Response
```json
``

