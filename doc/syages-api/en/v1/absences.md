# Absences

***

| Endpoint | Description |
| --- | ---:|
| [GET /interns/:intern/absences](#get-internsinternabsences) | Get a list of absences of an intern |
| [POST /interns/:intern/absences](#post-internsinternabsences) | Create a new absence for an intern |
| [PUT /interns/:intern/absences/:absence](#put-internsinternabsencesabsence) | Edit an absence |
| [DELETE /interns/:intern/absences/:absence](#delete-internsinternabsencesabsence) | Delete an absence |

## `GET /interns/:intern/absences`

Returns a list of absences of an intern.

### Example Request
```bash
```

### Example Response
```json
```

## `POST /interns/:intern/absences`

Create a new absence for an intern

### Parameters

| Name | Type | Required? | Description |
| --- | :---: | :---: | --- |
| | | | |



### Json Elements

### Example Request
```bash
```

### Example Response
```json

```

## `PUT /interns/:intern/absences/:absence`

Edit an absence
*Be sure to return a full absence Object*

### Example Request
```bash
```

### Example Response
```json
```

## `DELETE /interns/:intern/absences/:absence`

Delete an absence

### Example Request
```bash
curl -X "DELETE" "http://localhost:9000/api/1/intern/14/absence/1"
```

### Example Response
```json
``
