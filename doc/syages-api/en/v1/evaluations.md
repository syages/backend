# Evaluations

***

| Endpoint | Description |
| --- | --- |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations](#get-diplomasdiplomalessonslessonevaluations) | Get a list of evaluation |
| [GET /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](#get-diplomasdiplomalessonslessonevaluationsevaluation) | Get an evaluation by its id |
| [POST /diplomas/:diploma/lessons/:lesson/evaluations](#post-diplomasdiplomalessonslessonevaluations) | Create a new evaluation |
| [PUT /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](#put-diplomasdiplomalessonslessonevaluationsevaluation) | Edit an evaluation |
| [DELETE /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation](#delete-diplomasdiplomalessonslessonevaluationsevaluation) | Delete an evaluation |

## `GET /diplomas/:diploma/lessons/:lesson/evaluations`

Get a list of evaluations for a specific Lesson given by its id `:lesson` and the diplom in which it is `:diploma`.

### Example Request

### Example Response

## `GET /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation`

### Example Request

### Example Response

## `POST /diplomas/:diploma/lessons/:lesson/evaluations`

`:diploma` is the id of the diploma, in which you have a Lesson of id `:lesson`.

### Parameters

| Name  | Type | Required? | Description |
| ----------- | :---: | :---:| --- |
| name | `String` | `Required` | Name of the evaluation |
| coefficient | `Float` | `Required` | Coefficient of the evaluation |
| type |"Final" or "Continue" | `Required` | Type of the evaluation |
| date | `Epoch millis`| `Required` | Epoch time for the date of the evaluation |

### Example Request
```bash
```

### Example Response

## `PUT /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation`

### Example Request

### Example Response

## `DELETE /diplomas/:diploma/lessons/:lesson/evaluations/:evaluation`

### Example Request

### Example Response