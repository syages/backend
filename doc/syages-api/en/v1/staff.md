# Staff

***

| Endpoint | Description |
| --- | ---:|
| [GET /staff](#get-staff) | Get a list of Staff users|
| [GET /staff/:staff](#get-staffstaff) | Get a Staff user by his id |
| [POST /staff](#post-staff) | Create a new Staff user |
| [PUT /staff/:staff](#put-staffstaff) | Edit a Staff user |
| [DELETE /staff/:staff](#delete-staffstaff) | Delete a Staff user |

## `GET /staff`

Returns a list of Staff users

### Parameters

| Name   | Required? | Type    | Description |
| --- |:---:|:---:| --- |
| limit | `optional` | `Integer` | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | `optional` | `Integer` | Object offset for pagination. Default is 0. |

### Example Request

```bash
```

### Example Response

```json
```

## `GET /staff/:staff`

Returns a Staff user by his id

### Example Request

```bash
```

### Example Response

```json
```

## `POST /staff`

Create a new Staff user

### Json Elements

| Name | Type | Required? | Description |
| --- | :---: | :---:| --- |
| firstName | String | Required | firstname of the Staff user |
| lastName | String | Required | lastname of the Staff user |
| email | String/Email | Required | email of the Staff user |
| office | String | Optional | office of the Staff user |
| number | String | Optional | number office of the Staff user |

### Example Request

```bash
```

### Example Response

```json
```

## `PUT /staff/:staff`

Edit a Staff user
*Be sure to return a full Staff User Object*

### Example Request

```bash
```

### Example Response

```json
```

## `DELETE /staff/:staff`

Delete a Staff user

### Example Request

```bash
```

### Example Response

```json
``
