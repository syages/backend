# DiplomaPeriods

***

| Endpoint 																			  | Description                                   |
| ----------------------------------------------------------------------------------- | ---------------------------------------------:|
| [GET /diploma/:diploma_id/periods](#get-diplomadiploma_idperiods)                   | Get a list of DiplomaPeriods inside a Diploma |
| [GET /diploma/:diploma_id/period/:period_id](#get-diplomadiploma_idperiodperiod_id) | Get a DiplomaPeriod inside a Diploma          |
| [PUT /diploma/:diploma_id/period/:period_id](#put-diplomadiploma_idperiodperiod_id) | Edit a DiplomaPeriod                          |

## `GET /diploma/:diploma_id/periods`

Returns a list of DiplomaPeriods inside a Diploma

### Parameters

| Name      | Required ? | Type | Description                     |
| --------- |:----------:|:----:| -------------------------------:|
| diplomaId | yes        | long | An id that refers to a diploma. |
| periodsId | yes        | long | An id that refers to a period.  |

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/diploma/12/periods"
```

### Example Response

## `GET /diploma/:diploma_id/period/:period_id`

Returns a DiplomaPeriod

### Example Request
```bash
curl -X "GET" "http://localhost:9000/api/1/diploma/12/period/44"
```

### Example Response

## `PUT /diploma/:diploma_id/period/:period_id`

Edit a DiplomaPeriod

### Json Elements

| Name    | Type        | Description                        |
| ------- |:-----------:| ----------------------------------:|
| period  | Period      | Period used for this DiplomaPeriod |
| teacher | TeacherUser | Teacher for the course             |

### Example Request
```bash
curl -X "PUT" "http://localhost:9000/api/1/diploma/12/period/44"
```

### Example Response

