# Diplomas

***

| Endpoint | Description |
| --- | --- |
| [GET /diplomas](#get-diplomas) | Get diplomas list |
| [GET /diplomas/:diploma](#get-diplomasdiploma) | Get a diploma by his id |
| [POST /diplomas](#post-diplomas) | Create a new diploma |
| [PUT /diplomas/:diploma](#put-diplomasdiploma) | Edit a diploma |
| [DELETE /diplomas/:diploma](#delete-diplomasdiploma) | Delete a diploma |


## `GET /diplomas`

Returns diplomas list

### Parameters

| Name | Type | Required? | Description |
| ---- | :---:|:---:| --- |
| limit | `Integer` | `optional` | Maximum number of objects in array. Default is 25. Maximum is 100. |
| offset | `Integer` | `optional` | Object offset for pagination. Default is 0.                        |

### Example Request
```bash
```

### Example Response
```json
```

## `GET /diplomas/:diploma`

Returns a diploma by its id

### Example Request
```bash
```

### Example Response
```json
```

## `POST /diplomas`

Create a new diploma

### Json Elements

| Name | Type | Description |
| --- |:---:|:---:|
| name | String | Name of the diploma |
| supervisor | TeacherUser | Supervisor of the diploma |
| lessons | List&lt;Lesson&gt; | A list of <a href="lessons.md">Lesson</a> Object |
| periods | List&lt;Period&gt; | A list of <a href="periods.md">Period</a> Object |

### Example Request
```bash

```

### Example Response
```json
```

## `PUT /diplomas/:diploma`

Edit a diploma
*Be sure to return a full Diploma Object*

### Example Request
```bash
```

### Example Response
```json
```

## `DELETE /diplomas/:diploma`

Delete a diploma

### Example Request
```bash
```

### Example Response
```json
``
