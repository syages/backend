name := """syages"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
	"org.apache.commons" % "commons-email" % "1.3.1",
	"org.postgresql" % "postgresql" % "9.3-1100-jdbc4",
  "commons-io" % "commons-io" % "2.4",
  "org.xhtmlrenderer" % "flying-saucer-pdf" % "9.0.7",
  "nu.validator.htmlparser" % "htmlparser" % "1.4",
	javaJdbc,
	javaJpa,
	javaEbean,
	cache,
	javaWs
)
