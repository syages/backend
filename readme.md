Syages - The web appplication for teachers and students
===

Syages is a web app, composed of an api (play framework) and a web interface (polymer). The two are communicating together thanks to a node.js proxy to secure request either way.
Its purpose is to allow teachers and students to share marks, informations, absences to falicitate their communication.

## Requirements

- [JAVA 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
- [Node.Js](http://nodejs.org/)
- [Postgresql](http://www.postgresql.org/)
- a terminal
- some brain to make it work ;)

We are running with Play Framework 2.3.7(https://www.playframework.com/documentation/2.3.x/Home), PostgreSQL 9.4, and Node.js v0.10.35

## Run it

At this point, I'm assuming you have installed all the requirements.  

### Application.conf

All important settings regarding the application are in the .conf file `conf/application.conf`.  
[Here is the Play documentation where you can find information about it](https://www.playframework.com/documentation/2.3.x/Home)

### Let's start with Postgresql.
- Create a new user  
`CREATE ROLE syages LOGIN ENCRYPTED PASSWORD 'md55bef9404664bdc8160e581fdbdc03eca' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;`

- Create a new database  
`CREATE DATABASE syages WITH OWNER = syages ENCODING = 'UTF8' TABLESPACE = pg_default CONNECTION LIMIT = -1;`

If you don't want to use Postgresql, or change its name, location, you can do so but you **have to** modify the `application.conf` file inside **conf/**

### Let's start the app, we're waiting already :)
- Open two terminals  
First one will be for the play framework, and the other one for node.js   
So `cd` to the app directory in both of them.

- First terminal  
Run:  
`./activator run`

- Second terminal  
Run:  
`node proxy/proxy`

You application should now be reachable at [https://localhost:8443](https://localhost:8433)

## Compatibility:

Syages is reported to work without problem with the following web browsers:

	* Google Chrome - iOS 8
	* Apple Safari 8.0.3 - OS X
	* Apple Safari Mobile 8.2 - iOS
