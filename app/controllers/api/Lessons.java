/**
 * Classe des DiplomaCourses
 * @author SYAGES
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.*;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;

public class Lessons extends Controller {

    public static final String defaultFields = "(id,version,course(id, name), diploma(id, name), passMark, coefficient, optional, teacher(user(id, firstName, lastName)))";
    private static final JsonContext json = Ebean.createJsonContext();

	/**
	 * Cette méthode retourne un message d'erreur si il n'y a pas
     * de diplôme/cours associé aux id, sinon, elle retourne
     * le cours associé à un diplôme.
	 *
	 * @param lessonId		un id qui donne l'emplacement d'un cours
	 * @return				le diplomaCourse à l'id spécifié / une requête HTTP 400
	 * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result byId(UUID lessonId){
        Lesson lesson = Lesson.find.byId(lessonId);

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", lessonId)));
        }

        return ok(Json.parse(json.toJsonString(lesson, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de créer une Leçon.
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        Diploma diploma = null;

        if(input.has("diploma")){
            diploma = Diploma.find.byId(UUID.fromString(input.get("diploma").asText()));
            if(diploma == null){
                return notFound(Json.parse(String.format(idNotFound, "diploma", input.get("diploma").asLong())));
            }
            input.replace("diploma", Json.toJson(diploma));
        }

        if(input.has("course")){
            Course course = Course.find.byId(UUID.fromString(input.get("course").asText()));
            if(course == null){
                return notFound(Json.parse(String.format(idNotFound, "course", input.get("course").asLong())));
            }
            input.replace("course", Json.toJson(course));
        }

        if(input.has("teacher")){
            TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(input.get("teacher").asText()));
            if(teacher== null){
                return notFound(Json.parse(String.format(idNotFound, "teacher", input.get("teacher").asLong())));
            }
            input.replace("teacher", Json.toJson(teacher));
        }

        Lesson lesson = Json.fromJson(input, Lesson.class);

        Map<String,String> err = lesson.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            lesson.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Lessons.byId(lesson.getId()).url());
        return ok(Json.parse(json.toJsonString(lesson, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID lessonId){
        Lesson lesson = Lesson.find.byId(lessonId);

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", lessonId)));
        }

        try{
            lesson.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }
	
	/**
	 * Cette méthode permet d'éditer un diplomaCourses existant.
     *
     * @param id		un id qui donne l'emplacement d'un cours
     * @return				la vue des diplomaCourses
     * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){
        Lesson lesson = Lesson.find.byId(id);
        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(input.has("diploma")){
            Diploma diploma = Diploma.find.byId(UUID.fromString(input.get("diploma").asText()));
            if(diploma == null){
                return notFound(Json.parse(String.format(idNotFound, "diploma", input.get("diploma").asLong())));
            }
            input.replace("diploma", Json.toJson(diploma));
        }

        if(input.has("course")){
            Course course = Course.find.byId(UUID.fromString(input.get("course").asText()));
            if(course == null){
                return notFound(Json.parse(String.format(idNotFound, "course", input.get("course").asLong())));
            }
            input.replace("course", Json.toJson(course));
        }

        if(input.has("teacher")){
            TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(input.get("teacher").asText()));
            if(teacher== null){
                return notFound(Json.parse(String.format(idNotFound, "teacher", input.get("teacher").asLong())));
            }
            input.replace("teacher", Json.toJson(teacher));
        }

        Lesson newLesson = Json.fromJson(input, Lesson.class);
        newLesson.setEvaluations(lesson.getEvaluations());
        newLesson.setLectures(lesson.getLectures());
        newLesson.setFollowedDiplomas(lesson.getFollowedDiplomas());

        Map<String,String> err = newLesson.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newLesson.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Lessons.byId(id).url());
        return ok(Json.parse(json.toJsonString(newLesson, true, fieldsProperties(defaultFields))));
    }

	/**
	 * Cette méthode retourne la liste de tous les cours
	 * associés à un diplôme.
     *
     * @return              la liste des diplomaCourses / une requête HTTP 200
     * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Lesson.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Lesson> lessons = Lesson.find.where()
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(lessons, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de retourner une liste de stagiaire suivant cette leçon
     *
     * @param lessonId id de la leçon
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result getInterns(UUID lessonId){
        Lesson lesson = Lesson.find.byId(lessonId);

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", lessonId)));
        }

        List<InternUser> interns = new ArrayList<>();
        List<FollowedDiploma> fd = lesson.getOptional() ? lesson.getFollowedDiplomas() : lesson.getDiploma().getFollowedDiplomas();

        for(FollowedDiploma f : fd){
            interns.add(f.getIntern());
        }

        return ok(Json.parse(json.toJsonString(interns, true, fieldsProperties(Interns.fieldsLessons))));
    }

    /**
     * Cette méthode permet de retourne une liste de lecture appartenant a cette leçon
     *
     * @param lessonId
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result getLectures(UUID lessonId){
        Lesson lesson = Lesson.find.byId(lessonId);

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", lessonId)));
        }

        List<Lecture> lectures = lesson.getLectures();

        return ok(Json.parse(json.toJsonString(lectures, true, fieldsProperties(Lectures.defaultFieldsList))));
    }

    /**
     * Cette méthode permet de retourner une liste d'évaluations appartenant a cette leçon
     *
     * @param lessonId
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result getEvaluations(UUID lessonId){
        Lesson lesson = Lesson.find.byId(lessonId);

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", lessonId)));
        }

        List<Evaluation> evaluations = lesson.getEvaluations();

        return ok(Json.parse(json.toJsonString(evaluations, true, fieldsProperties(Evaluations.defaultFields))));
    }
}
