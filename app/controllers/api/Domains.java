/**
 * Classe des domaines
 * @author SYAGES
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;

import helpers.util.CORS;
import helpers.util.Divers;
import models.Domain;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;


public class Domains extends Controller {

    /**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * de domaine associé à l'id, sinon, elle retourne le domaine.
     *
     * @param id	un id qui donne l'emplacement du domaine
     * @return		le domaine à l'id spécifié / une requête HTTP 400
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result byId(UUID id){
        Domain domain = Domain.find.byId(id);

        if(domain == null){
            return notFound(String.format(idNotFound, "domain", id));
        }

        return ok(Json.toJson(domain));
    }

    /**
	 * Cette méthode permet de créer un nouveau domaine.
	 *
	 * @return		la vue des domaines
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        Domain domain = Json.fromJson(request().body().asJson(), Domain.class);

        Map<String,String> err = domain.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            domain.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Domains.byId(domain.getId()).url());
        return created(Json.toJson(domain));
    }

	/**
	 * Cette méthode permet d'éditer un domaine existant,
	 * avec message d'erreur si il n'y a pas de domaine associé
	 * à l'id.
	 *
	 * @param id	un id qui donne l'emplacement d'un domaine
	 * @return		la vue des domaines
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){
        Domain domain = Domain.find.byId(id);
        if(domain == null){
            return notFound(Json.parse(String.format(Divers.idNotFound, "domain", id)));
        }

        Domain newDomain = Json.fromJson(request().body().asJson(), Domain.class);

        if(!newDomain.getId().equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        Map<String,String> err = newDomain.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newDomain.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Domains.byId(domain.getId()).url());
        return ok(Json.toJson(newDomain));
    }

	/**
	 * Cette méthode permet de supprimer un domaine existant,
	 * avec message d'erreur si il n'y a pas de domaine associé
	 * à l'id.
	 *
	 * @param id	un id qui donne l'emplacement d'un domaine
	 * @return		un message de réussite / une requête HTTP 200 ou 400
	 * @see			
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID id){
        Domain domain = Domain.find.byId(id);

        if (domain == null){
            return notFound(Json.parse(String.format(Divers.idNotFound, "domain", id)));
        }

        try {
            domain.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	/**
	 * Cette méthode retourne la liste de tous les domaines.
	 *
	 * @param limit     la limite d'affichage de domaines dans la liste
     * @param offset    la position à laquelle on commence à regarder dans la base de donnée pour l'affichage
	 * @return			la liste des domaines / une requête HTTP 200
	 * @see				Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result list(Integer limit, Integer offset, String sort){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Domain.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Domain> domains = Domain.find.where()
                .orderBy(sort != null ? sortProperties(sort) : "name")
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.toJson(domains));
    }

}
