/**
 * Classe des utilisateurs
 * @author SYAGES
 * @version 1.0
 * date 10/01/2015
 */

package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.google.common.io.Files;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import it.innove.play.pdf.PdfGenerator;
import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static helpers.util.Divers.exceptionCatch;
import static helpers.util.Divers.fieldsProperties;
import static helpers.util.Divers.idNotFound;

public class Users extends SecuredController {

    private static final JsonContext json = Ebean.createJsonContext();


	/**
	 * Cette méthode retourne un message d'erreur si l'id donné n'est
	 * associé à aucun utilisateur, sinon, elle retourne l'utilisateur.
	 *
	 * @param id	un id qui donne l'emplacement d'un utilisateur
	 * @return		l'utilisateur à l'id spécifié / une requête HTTP 400
	 * @see			Result
	 */
	@CORS
	@OAuthRequire(accessibleByValues = {"Administrator"})
	public static Result byId(UUID id)
	{
		User user = User.find.byId(id);
		if (user == null)
		{
			return notFound(Json.parse(String.format(idNotFound, "user", id)));
		}
		return ok(Json.toJson(user));
	}
	
	/**
	 * Cette méthode permet de supprimer un utilisateur existant.
	 *
	 * @param id	un id qui donne l'emplacement d'un utilisateur
	 * @return		un message de réussite / une requête HTTP 200 ou 400
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
	public static Result delete(UUID id) {
		User user = User.find.byId(id);
		if(user == null){
            return notFound(Json.parse(String.format(idNotFound, "user", id)));
        }else{
            switch(user.getFlagType()){
                case Administrator:
                    Administrator.findByUserId(id).delete();
                    break;
                case President:
                    President.findByUserId(id).delete();
                    break;
                case Teacher:
                    TeacherUser.findByUserId(id).delete();
                    break;
                case Staff:
                    StaffUser.findByUserId(id).delete();
                    break;
                case Intern:
                    InternUser.findByUserId(id).delete();
                    break;
            }
        }
		return noContent();
	}

	/**
	 * Cette méthode permet de récupérer l'image de profil d'un utilisateur.
	 *
	 * @param userId	un id qui donne l'emplacement d'un utilisateur
	 * @return			un message de réussite / une requête HTTP 200 ou 400
	 * @see				Result
	 */
	@CORS
	public static Result getPicture(UUID userId){
		User user = User.find.byId(userId);
		if(user == null){
			return badRequest(String.format(idNotFound, "user", userId));
		}

		if(user.getPicture() == null){
			return ok(new java.io.File("public/www/static/defaultAvatar.jpg"));
		}

		Attachment attachment = user.getPicture();
		return ok(attachment.getContent()).as(attachment.getContentType());
	}

	/**
	 * Cette méthode retourne la liste de tous les utilisateurs.
	 *
	 * @return			la liste des utilisateurs / une requête HTTP 200
	 * @see				Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
	public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = User.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<User> users = User.find.where()
                .findPagingList(limit).getPage(offset).getList();

		return ok(Json.toJson(users));
	}

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
	public static Result setPassword(UUID id, String password){
		User user = User.find.byId(id);
		if(user == null){
			return badRequest();
		}
		if (password != null && password.length() >= 8){
			user.setPassword(password);
			user.save();
			return noContent();
		}
		return badRequest();
	}

    @CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result teachersDirectory(Integer limit, Integer offset){
        List<TeacherUser> teachers = TeacherUser.find.where().eq("shareMail", true)
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(teachers, true, fieldsProperties(Teachers.listFields))));
    }

    @CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result internsDirectory(Integer limit, Integer offset){
        List<InternUser> interns = InternUser.find.where()
                .findPagingList(limit).getPage(offset).getList();;

        return ok(Json.parse(json.toJsonString(interns, true, fieldsProperties(Interns.fieldsLessons))));
    }

	/**
	 * Cette méthode permet de choisir l'image de profil d'un utilisateur.
	 *
	 * @param id	un id qui donne l'emplacement d'un utilisateur
	 * @return		un message de réussite / une requête HTTP 200 ou 400
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
	public static Result setPicture(UUID id){
		User user = User.find.byId(id);
		if(user == null){
			return notFound(String.format(idNotFound, "user", id));
		}

        Integer filesize = Integer.valueOf(request().getHeader("Content-Length"));

        if(filesize > Math.pow(10,6)){
            return badRequest(Json.parse("{\"error\":\"File is too large.\"}"));
        }

        MultipartFormData body = request().body().asMultipartFormData();
		MultipartFormData.FilePart picture = body.getFile("picture");

        Logger.info(picture.getContentType());

        if(!picture.getContentType().equals("image/png") && !picture.getContentType().equals("image/jpeg")){
            return badRequest(Json.parse("{\"error\":\"Picture must be JPG or PNG only.\"}"));
        }

        byte[] imgData = null;
        try{
            File file = picture.getFile();
            imgData = Files.toByteArray(file);
        }catch (IOException e){
            return badRequest("file empty");
        }


        if(user.getPicture() != null){
            Attachment attachment = user.getPicture();
            attachment.setContent(imgData);
            attachment.setContentType(picture.getContentType());
            try{
                attachment.update();
            }catch(PersistenceException e){
                return badRequest(Json.toJson(exceptionCatch(e)));
            }
        } else{
            Attachment attachment = new Attachment(imgData, picture.getContentType(), user);
            try{
                attachment.save();
            }catch(PersistenceException e){
                return badRequest(Json.toJson(exceptionCatch(e)));
            }
        }

        return noContent();
	}

	@CORS
    public static Result getPDF(UUID id){
        InternUser intern = InternUser.findByUserId(id);
        if(intern == null) return badRequest();
        return PdfGenerator.ok(views.html.report.trimestre.render(intern), "internNote");
    }
}
