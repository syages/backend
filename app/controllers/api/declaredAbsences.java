package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.DeclaredAbsence;
import models.InternUser;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;

public class declaredAbsences extends SecuredController {

    public static final String defaultFields = "(id,version,startDate,timeOfDayStart,endDate,timeOfDayEnd,status,reason,commentary,intern(user(id))";
    private static final JsonContext json =Ebean.createJsonContext();


    /**
     * Cette méthode permet de récupérer une absence déclarée par son id
     *
     * @param id    id de l'absence déclarée que l'on souhaite récupérer
     * @return Result   200 - Ok
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
    public static Result byId(UUID id){
        DeclaredAbsence da = DeclaredAbsence.find.byId(id);
        if (da == null){
            return notFound(Json.parse(String.format(idNotFound, "declaredAbsences", id)));
        }

        return ok(Json.parse(json.toJsonString(da, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de changer le statut d'une absence déclarée
     *
     * @param id    id de l'absence dont on souhaite modifier le statut
     * @return Result   200 - Ok
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator","StaffUser", "President"})
    public static Result changeStatus(UUID id){
        DeclaredAbsence da = DeclaredAbsence.find.byId(id);

        if(da == null){
            return notFound(Json.parse(String.format(idNotFound, "declaredAbsence", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!input.has("status")){
            return badRequest(Json.parse("{\"error\":\"Invalid Json format.\"}"));
        }

        da.setStatus(DeclaredAbsence.AbsenceStatus.valueOf(input.get("status").asText()));

        try{
            da.update();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return ok(Json.parse(json.toJsonString(da, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de créer une absence déclarée
     *
     * @return Result 201 - Created
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        InternUser intern = InternUser.findByUserId(oauthUser().getId());

        input.put("intern", Json.toJson(intern));

        if(input.has("status")){
            input.remove("status");
        }

        DeclaredAbsence da = Json.fromJson(input, DeclaredAbsence.class);

        Map<String, String> err = da.validate();
        if (!err.isEmpty()) {
            return badRequest(Json.toJson(err));
        }

        try{
            da.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.declaredAbsences.byId(da.getId()).url());
        return created(Json.parse(json.toJsonString(da, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de supprimer une absence déclarée
     *
     * @param id    id de l'absence que l'on souhaite supprimer
     * @return Result   204 - No Content
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator","StaffUser", "President"})
    public static Result delete(UUID id){
        DeclaredAbsence da = DeclaredAbsence.find.byId(id);
        if (da == null){
            return notFound(Json.parse(String.format(idNotFound, "declaredAbsences", id)));
        }

        try{
            da.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Cette méthode permet d'éditer une absence déclarée
     *
     * @param id    id de l'absence que l'on souhaite éditer
     * @Authorized  InternUser
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result edit(UUID id){
        DeclaredAbsence da = DeclaredAbsence.find.byId(id);

        if(da == null){
            return notFound(Json.parse(String.format(idNotFound, "declaredAbsence", id)));
        }

        InternUser intern = InternUser.findByUserId(oauthUser().getId());

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("intern").asText()).equals(intern.getUser().getId())){
            return unauthorized(Json.parse(unauthorized));
        }

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        String inputStatus = input.get("status").asText();
        DeclaredAbsence.AbsenceStatus sta = null;

        try{
            sta = DeclaredAbsence.AbsenceStatus.valueOf(inputStatus);
        }catch(IllegalArgumentException e){
            return badRequest(Json.parse(String.format(jsonInvalid, "status")));
        }

        if(input.has("status") && sta != da.getStatus()){
            return badRequest(Json.parse("{\"error\":\"You don't have the rights to modify this data.\"}"));
        }

        input.replace("intern", Json.toJson(intern));

        DeclaredAbsence newDa = Json.fromJson(input, DeclaredAbsence.class);

        Map<String, String> err = newDa.validate();
        if (!err.isEmpty()) {
            return badRequest(Json.toJson(err));
        }
        try {
            newDa.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        } finally {
            newDa.refresh();
        }


        response().setHeader(LOCATION, controllers.api.routes.declaredAbsences.byId(id).url());
        return ok(Json.parse(json.toJsonString(newDa, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de récupérer une liste contenant toutes les absences déclarées
     *
     * @Authorized      Administrator, StaffUser, President
     * @return  Result  Ok(Json)
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President", "TeacherUser"})
    public static Result list(Integer limit, Integer offset, String sort){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = DeclaredAbsence.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<DeclaredAbsence> das = DeclaredAbsence.find.where()
                .orderBy(sort != null ? sortProperties(sort): "startDate DESC")
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(das, true, fieldsProperties(defaultFields))));
    }
}
