package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.avaje.ebean.text.json.JsonWriteBeanVisitor;
import com.avaje.ebean.text.json.JsonWriteOptions;
import com.avaje.ebean.text.json.JsonWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.InternUser;
import models.NotedAbsence;
import models.User;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;


public class notedAbsences extends SecuredController{

    public static final String defaultFields = "(id,version,status,intern(user(id)),observerUser(id),date,timeOfDay)";
    public static final String defaultFieldsLecture = "(id,version,intern(user(id,firstName,lastName)))";
    private static final JsonContext json = Ebean.createJsonContext();

    /**
     * Cette méthode permet de récupérer une absence notée par son id
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "TeacherUser", "President"})
    public static Result byId(UUID id){
        NotedAbsence na = NotedAbsence.find.byId(id);

        if(na == null){
            return notFound(Json.parse(String.format(idNotFound, "notedAbsences", id)));
        }

        JsonWriteOptions opt = fieldsProperties(notedAbsences.defaultFields);
        opt.setRootPathVisitor(new JsonWriteBeanVisitor<NotedAbsence>() {
            @Override
            public void visit(NotedAbsence n, JsonWriter jsonWriter) {
                System.out.println(n.getDate().toString());
                jsonWriter.appendQuoteEscapeValue("date", n.getDate().toString());
                jsonWriter.appendQuoteEscapeValue("timeOfDay", n.getTimeOfDay().toString());
            }
        });

        return ok(Json.parse(json.toJsonString(na,true, opt)));
    }

    /**
     * Cette méthode permet de changer le statut d'une absence notée
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser", "President"})
    public static Result changeStatus(UUID id){
        NotedAbsence na = NotedAbsence.find.byId(id);

        if (na == null){
            return notFound(Json.parse(String.format(idNotFound, "notedAbsences", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!input.has("status")) return badRequest(Json.parse("{\"error\":\"Invalid JSON format.\"}"));

        if(!na.getStatus().toString().equals(input.get("status"))) {
            try {
                na.setStatus(NotedAbsence.AbsenceStatus.valueOf(input.get("status").asText()));
            } catch (IllegalArgumentException e) {
                return badRequest(Json.parse(String.format(jsonInvalid, "status")));
            }

            try {
                na.update();
            } catch (PersistenceException e) {
                return badRequest(Json.toJson(exceptionCatch(e)));
            }
        }

        return noContent();
    }

    /**
     * Cette méthode permet de créer une absence notée
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode)request().body().asJson();

        User observer = oauthUser();

        if(input.has("intern")){
            InternUser intern = InternUser.findByUserId(UUID.fromString(input.get("intern").asText()));
            if(intern == null){
                return notFound(Json.parse(String.format(idNotFound, "intern", input.get("intern").asText())));
            }

            input.replace("intern", Json.toJson(intern));
        }

        NotedAbsence na = Json.fromJson(input, NotedAbsence.class);
        na.setObserverUser(observer);

        Map<String,String> err = na.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            na.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.notedAbsences.byId(na.getId()).url());
        return created(Json.parse(json.toJsonString(na, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet d'éditer une absence notée
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser"})
    public static Result edit(UUID id){
        NotedAbsence na = NotedAbsence.find.byId(id);

        if (na == null) {
            return notFound(Json.parse(String.format(idNotFound, "notedAbsence", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(!input.has("intern")) return badRequest(Json.parse(String.format(jsonInvalid, "intern")));

        InternUser intern = InternUser.findByUserId(UUID.fromString(input.get("intern").asText()));
        if(intern == null){
            return badRequest(Json.parse(String.format(idNotFound, "intern", input.get("intern").asLong())));
        }

        if(oauthUser().getFlagType().toString().equals("TeacherUser") && !na.getObserverUser().equals(oauthUser())){
            return badRequest(Json.parse(unauthorized));
        }

        input.remove("intern");

        NotedAbsence newNa = Json.fromJson(input, NotedAbsence.class);
        newNa.setIntern(intern);
        newNa.setObserverUser(na.getObserverUser());

        Map<String,String> err = newNa.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newNa.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.notedAbsences.byId(id).url());
        return ok(Json.parse(json.toJsonString(newNa, true, fieldsProperties(defaultFields))));
    }


    /**
     * Cette méthode permet de supprimer une absences notées
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser"})
    public static Result delete(UUID id){
        NotedAbsence na = NotedAbsence.find.byId(id);
        if(na == null){
            return notFound(Json.parse(String.format(idNotFound, "notedAbsences", id)));
        }

        try{
            na.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Cette méthode permet de récupérer la liste des absences notées
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "TeacherUser", "President"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = NotedAbsence.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<NotedAbsence> nas = NotedAbsence.find.where()
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(nas, true, fieldsProperties(defaultFields))));
    }

}
