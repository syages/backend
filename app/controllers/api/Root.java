/**
 * Classe du super utilisateur
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.PasswordResetToken;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.exceptionCatch;
import static helpers.util.Divers.idNotFound;
import static helpers.util.Divers.jsonInvalid;

public class Root extends Controller {

	@CORS
    public static Result resetPassword(String email){
        User u = User.find.where().eq("email", email).findUnique();

        if(u == null){
            return badRequest();
        }
        // send email
        PasswordResetToken reset = new PasswordResetToken(u);

        try{
            reset.save();
            reset.sendMail();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	@CORS
    public static Result resetTokenValid(String token){
        PasswordResetToken reset = PasswordResetToken.getByKey(token);
        if (reset == null){
            return badRequest();
        }
        UUID id = reset.getUser().getId();

        Map<String, String> map = new HashMap<String, String>();
        map.put("valid", "true");
        map.put("user",id.toString());
        return ok(Json.toJson(map));
    }

    /**
     * Cette méthode permet de changer le mot de passe de l'utilisateur suite à un oubli.
     *
     * @return
     */
    @CORS
    public static Result setPassword(String token, String password){
        PasswordResetToken resetToken = PasswordResetToken.getByKey(token);
        if(resetToken == null || !resetToken.isValid()){
            return badRequest(Json.parse("{\"error\":\"Le token est invalide.\"}"));
        }
        User user = resetToken.getUser();


        user.setPassword(password);

        try{
            user.update();
            resetToken.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	@CORS
    public static Result teapot(){
        return status(418);
    }
}
