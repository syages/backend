/**
 * Classe des messages
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */

package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.News;
import models.User;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;

public class Informations extends SecuredController {

    private static final JsonContext json = Ebean.createJsonContext();
    public static final String defaultFields = "(id, version, author(id,firstName,lastName), post, posted, title)";
    public static final String defaultFieldsList = "(id,version,author(id,firstName,lastName),title,post,posted)";

    /**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * de messages associé à l'id, sinon, elle retourne
     * le message associé à l'id.
     *
     * @param id     un id qui donne l'emplacement d'un message
     * @return       le message à l'id spécifié / une requête HTTP 400
     * @see          Result
     */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result byId(UUID id){
        News news = News.find.byId(id);
        if(news == null){
            return notFound(Json.parse(String.format(idNotFound, "news", id)));
        }
        return ok(Json.parse(json.toJsonString(news, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de créer un nouveau message.
     *
     * @return      la vue des enseignants
     * @see         Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        User user = oauthUser();

        News news = Json.fromJson(input, News.class);
        news.setAuthor(user);

        Map<String,String> err = news.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            news.save();
        }catch( PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Informations.byId(news.getId()).url());
        return created(Json.parse(json.toJsonString(news, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de supprimer un message existant.
     *
     * @param id     un id qui donne l'emplacement d'un message
     * @return       un message de réussite / une requête HTTP 200 ou 400
     * @see          Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result delete(UUID id){
        News news = News.find.byId(id);

        if(news == null){
            return notFound(Json.parse(String.format(idNotFound, "news", id)));
        }

        User user = oauthUser();

        if(user.getFlagType().toString().equals("TeacherUser")){
            if(!news.getAuthor().equals(user)){
                return unauthorized(Json.parse(unauthorized));
            }
        }

        try{
            news.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Cette méthode permet d'éditer un message existant.
     *
     * @param id     un id qui donne l'emplacement d'un message
     * @return       la vue des messages
     * @see          Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result edit(UUID id){
        News news = News.find.byId(id);

        if(news == null){
            return notFound(Json.parse(String.format(idNotFound, "news", id)));
        }

        User editor = oauthUser();

        if(editor.getFlagType() == User.Usertype.Teacher && !news.getAuthor().equals(editor)){
            return unauthorized(Json.parse(unauthorized));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(input.has("author")){
            User user = User.find.byId(UUID.fromString(input.get("author").asText()));
            if(!user.equals(oauthUser()) && user.getFlagType().toString().equals("TeacherUser"))
                return unauthorized(Json.parse(unauthorized));

            input.replace("author", Json.toJson(user));
        }

        News newNews = Json.fromJson(input, News.class);

        Map<String,String> err = newNews.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newNews.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Informations.byId(id).url());
        return ok(Json.parse(json.toJsonString(newNews, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode retourne la liste de tous les messages.
     *
     * @return  la liste des évaluations / une requête HTTP 200
     * @see     Result
     */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result list(Integer limit, Integer offset, String sort){
        limit = formatLimit(limit);
        offset = formatOffset(offset);

        Integer total_count = News.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<News> newsList = News.find.where()
                .orderBy(sort != null ? sortProperties(sort): "posted DESC")
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(newsList, true, fieldsProperties(defaultFieldsList))));
    }
}
