/**
 * Classe des cours
 * @author SYAGES
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.util.CORS;
import helpers.util.Divers;
import models.Course;
import models.Domain;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;


public class Courses extends Controller {

    private static final JsonContext json = Ebean.createJsonContext();
    public static final String defaultFields = "(id,version,name,domain(id,name))";

	/**
	 * Cette méthode retourne un message d'erreur si il n'y a pas
     * de cours associé à l'id, sinon, elle retourne le cours.
     *
     * @param id	un id qui donne l'emplacement d'un cours
     * @return		le cours à l'id spécifié / une requête HTTP 400
     * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
    public static Result byId(UUID id, String fields){
        Course course = Course.find.byId(id);
        if(course == null){
            return notFound(Json.parse(String.format(idNotFound, "course", id)));
        }

        if(fields == null){
            return ok(Json.parse(json.toJsonString(course, true, fieldsProperties(defaultFields))));
        }


        return ok(Json.parse(json.toJsonString(course, true, fieldsProperties(fields))));
    }

	/**
	 * Cette méthode permet de créer un nouveau cours.
	 *
	 * @return		la vue des cours
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
    public static Result create(){
        // variables
        // take request body
        ObjectNode input = (ObjectNode) request().body().asJson();

        // check if body has "domain" field
        if(input.has("domain")){
            Domain domain = Domain.find.byId(UUID.fromString(input.get("domain").asText()));
            // check if "domain" field is valid Domain
            if(domain == null){
                return notFound(Json.parse(String.format(idNotFound, "domain", input.get("domain"))));
            }
            input.replace("domain", Json.toJson(domain));
        }

        Course course = Json.fromJson(input, Course.class);
        Map<String,String> err = course.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        // try to persist the POJO
        try{
            course.save();
        }catch (PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        // set Location header to access data
        response().setHeader(LOCATION, controllers.api.routes.Courses.byId(course.getId(), null).url());

        // return result as 201 status with data
        return created(Json.parse(json.toJsonString(course, true, fieldsProperties(defaultFields))));
    }

	/**
	 * Cette méthode permet de supprimer un cours existant.
     *
	 * @param   id  id de l'objet Course à supprimer
	 * @return      204 si réussite / 400 sinon
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
    public static Result delete(UUID id){
        Course course = Course.find.byId(id);

        if(course == null){
            return notFound(Json.parse(String.format(idNotFound, "course", id)));
        }

        try{
            course.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	/**
	 * Cette méthode permet d'éditer un cours existant.
	 *
	 * @param id	un id qui donne l'emplacement d'un cours
	 * @return		la vue des cours
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id) {
        Course course = Course.find.byId(id);
        if (course == null) {
            return notFound(Json.parse(String.format(idNotFound, "course", id)));
        }
        // take request body
        ObjectNode input = (ObjectNode) request().body().asJson();

        if (!UUID.fromString(input.get("id").asText()).equals(id)) {
            return badRequest(Json.parse(String.format(noMatch)));
        }

        // check if body has "domain" field
        if (input.has("domain")) {
            Domain domain = Domain.find.byId(UUID.fromString(input.get("domain").asText()));
            if (domain == null) {
                return notFound(Json.parse(String.format(idNotFound, "domain", input.get("domain"))));
            }
            input.replace("domain", Json.toJson(domain));
        }

        Course newCourse = Json.fromJson(input, Course.class);

        // validate the final POJO
        Map<String, String> err = newCourse.validate();
        if (!err.isEmpty()) {
            return badRequest(Json.toJson(err));
        }

        // try to persist the POJO
        try {
            newCourse.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }


        // set Location header to access data
        response().setHeader(LOCATION, controllers.api.routes.Courses.byId(id, null).url());
        return ok(Json.parse(json.toJsonString(newCourse, true, fieldsProperties(defaultFields))));
    }

	/**
	 * Cette méthode retourne la liste de tous les cours.
	 *
	 * @return	la liste des cours / une requête HTTP 200
	 * @see		Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "StaffUser", "President"})
    public static Result list(Integer limit, Integer offset, String sort, String fields){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Course.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Course> courses = Course.find.where().orderBy(sort != null ? Divers.sortProperties(sort): "name").findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(courses, true, fieldsProperties(fields == null ? defaultFields: fields))));
    }


}
