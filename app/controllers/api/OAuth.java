/**
 * Classe des OAuth
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;


import com.aureo.oauth.common.OAuthRequest;
import com.aureo.oauth.common.OAuthResponse;
import com.aureo.oauth.play.OAuthRequire;
import com.aureo.oauth.play.RequestWrapper;
import com.aureo.oauth.play.ResponseConverter;
import com.aureo.oauth.provider.OAuthProvider;

import helpers.oauth.OAuthProviderSingleton;
import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.AccessToken;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;

import java.sql.Timestamp;

public class OAuth extends SecuredController {

	/**
	 * Cette méthode permet de s'authentifier à l'API.
	 *
	 * @return	le jeton (si response_type = token), ou une page d'autorisation.
	 * @see		Result
	 */
	@CORS
	public static Result auth()
	{
		OAuthProvider provider = OAuthProviderSingleton.provider();
		OAuthResponse response = provider.authenticate(RequestWrapper.fromContext());
		return ResponseConverter.playResponse(response);
	}
	
	/**
	 * Cette méthode permet de s'authentifier à l'API.
	 *
	 * @return	un message de réussite
	 * @see		Result
	 */
	@CORS
	@BodyParser.Of(BodyParser.FormUrlEncoded.class)
	public static Result authCallback()
	{
		OAuthProvider provider = OAuthProviderSingleton.provider();
		OAuthResponse response = provider.authenticateCallback(RequestWrapper.fromContext());
		return ResponseConverter.playResponse(response);
	}
	
	/**
	 * Cette méthode permet de révoquer un jeton d'accès.
	 *
	 * @return	une requête HTTP 200 ou 400
	 * @see		Result
	 */
	@CORS
	public static Result revoke()
	{
		OAuthProvider provider = OAuthProviderSingleton.provider();
		OAuthRequest req = RequestWrapper.fromContext();
		
		String token = provider.validateRequestToken(req);
		if (token == null)
		{
			return notFound();
		}
		provider.getDAO().deleteToken(provider.getDAO().getToken(token));
		
        return ok();
	}
	
	/**
	 * Cette méthode va créer un token qui permettra d'éviter de
	 * retaper ses identifiants à chaque changement de vue.
	 *
	 * @return	un message de réponse
	 * @see		Result
	 */
	@CORS
	public static Result token()
	{
		OAuthProvider provider = OAuthProviderSingleton.provider();
		OAuthResponse response = provider.token(RequestWrapper.fromContext());
		return ResponseConverter.playResponse(response);
	}
	
	/**
	 * Cette méthode permet de vérifier que l'authentification
	 * s'est correctement déroulé.
	 *
	 * @return	un message de confirmation de connexion / une requête HTTP 200
	 * @see		Result
	 */
	@CORS
	@OAuthRequire(onFailure="{\"error\": \"invalid_token\"}", onFailureStatusCode=401, refreshToken=false, accessibleByMethod="")
	public static Result validateAuth()
	{
		ExpirationDate ed = new ExpirationDate((AccessToken)oauthToken());
		
		return ok(Json.toJson(ed));
	}
	
	static class ExpirationDate
	{
		public final Timestamp expiration_date;
		public ExpirationDate(AccessToken token)
		{
			expiration_date = token.getExpirationTimestamp();
		}
	}
}
