package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.Report;
import models.User;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;
import java.util.stream.Collectors;

import static helpers.util.Divers.*;

public class Reports extends SecuredController {

    public static final String defaultFieldsList = "(id,controller,type,status,createdAt,priority, userInfo, commentary)";

    private static final JsonContext json = Ebean.createJsonContext();

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result byId(UUID id) {
        Report report = Report.find.byId(id);
        if (report == null){
            return notFound(Json.parse(String.format(idNotFound, "report", id)));
        }

        return ok(Json.toJson(report));
    }


	@CORS
    @OAuthRequire(accessibleByMethod = "", bypass = true)
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();
        
        User user = oauthToken()!= null ? oauthUser() : null ;
        
        Report report = Json.fromJson(input, Report.class);

        if (user != null)
        	report.setUser(user);

        Map<String,String> err = report.validate();

        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            report.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }
        
        response().setHeader(LOCATION, controllers.api.routes.Reports.byId(report.getId()).url());
        return created(Json.toJson(report));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result delete(UUID id){
        Report report = Report.find.byId(id);

        if(report == null){
            return notFound(Json.parse(String.format(idNotFound, "report", id)));
        }

        try{
            report.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result changeStatus(UUID id){
        Report report = Report.find.byId(id);

        if(report == null){
            return notFound(Json.parse(String.format(idNotFound, "report", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!input.has("status")){
            return badRequest(Json.parse(String.format(jsonInvalid, "status")));
        }

        Report.ReportStatus sta = null;

        try {
            sta = Report.ReportStatus.valueOf(input.get("status").asText());
        }catch (IllegalArgumentException e){
            return badRequest(Json.parse(String.format(jsonInvalid, "status")));
        }

        report.setStatus(sta);

        try{
            report.update();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Reports.byId(id).url());
        return ok(Json.toJson(report));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Report.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Report> reports = Report.find.where()
                .orderBy("createdAt")
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(reports, true, fieldsProperties(defaultFieldsList))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result listOpened(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Report.find.where().eq("status", Report.ReportStatus.Opened).findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Report> reports = Report.find.where()
                .eq("status", Report.ReportStatus.Opened)
                .orderBy("createdAt")
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(reports, true, fieldsProperties(defaultFieldsList))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result listClosed(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Report.find.where().eq("status", Report.ReportStatus.Closed).findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Report> reports = Report.find.where()
                .eq("status", Report.ReportStatus.Closed)
                .orderBy("createdAt")
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(reports, true, fieldsProperties(defaultFieldsList))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result getStats(){
        List<Report> reports = Report.find.all();
        ObjectNode result = Json.newObject();
        Map<String,Long> byStatus = new HashMap<>();
        byStatus.put("closed", getClosedStats(reports));
        byStatus.put("opened", getOpenedStats(reports));
        JsonNode status = Json.toJson(byStatus);
        result.put("byStatus", status);

        List<String> con = new ArrayList<>();
        for(Report r: reports){
            if(!con.contains(r.getController())){
                con.add(r.getController());
            }
        }

        ObjectNode controllers = Json.newObject();
        for(String s : con){
            ObjectNode c = Json.newObject();
            List<Report> r = reports.stream().filter(l -> l.getController().equals(s)).collect(Collectors.toList());
            c.put("closed", getClosedStats(r));
            c.put("opened", getOpenedStats(r));
            controllers.put(s, c);
        }
        result.put("byController", controllers);

        ObjectNode users = Json.newObject();
        List<User.Usertype> usertypes = new ArrayList<>();
        for(Report r: reports){
            if(r.getUser()!= null && !usertypes.contains(r.getUser().getFlagType())){
                usertypes.add(r.getUser().getFlagType());
            }
        }

        
        for(User.Usertype t : usertypes){
            ObjectNode type = Json.newObject();
            List<Report> r = reports.stream().filter(l -> l.getUser().getFlagType().equals(t)).collect(Collectors.toList());
            type.put("closed", getClosedStats(r));
            type.put("opened", getOpenedStats(r));
            users.put(t.toString(), type);
        }

        result.put("byUser", users);

        ObjectNode priority = Json.newObject();
        List<Report.ReportBugPriority> priorities = new ArrayList<>();
        for(Report r: reports){
            if(r.getType().equals(Report.ReportType.Bug) && !priorities.contains(r.getPriority())){
                priorities.add(r.getPriority());
            }
        }

        for(Report.ReportBugPriority p : priorities){
            ObjectNode prio = Json.newObject();
            List<Report> r = reports.stream().filter(l -> l.getPriority().equals(p)).collect(Collectors.toList());
            prio.put("closed", getClosedStats(r));
            prio.put("opened", getOpenedStats(r));
            priority.put(p.toString(), prio);
        }

        ObjectNode m = Json.newObject();
        List<Report> r = reports.stream().filter(l -> l.getType().equals(Report.ReportType.Message)).collect(Collectors.toList());
        m.put("closed", getClosedStats(r));
        m.put("opened", getOpenedStats(r));
        priority.put("message", m);
        result.put("byPriority", priority);
        return ok(result);
    }

    public static Long getOpenedStats(List<Report> l){
        return l.stream().filter(r -> r.getStatus().equals(Report.ReportStatus.Opened)).count();
    }

    public static Long getClosedStats(List<Report> l){
        return l.stream().filter(r -> r.getStatus().equals(Report.ReportStatus.Closed)).count();
    }
}
