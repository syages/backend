package controllers.api;

import helpers.util.CORS;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Demo Controller for user creation
 */
public class Demo extends Controller {

    @CORS
    public static Result interns(){
        return Interns.create();
    }

    @CORS
    public static Result teachers(){
        return Teachers.create();
    }

    @CORS
    public static Result staff(){
        return Staff.create();
    }
}
