/**
 * Classe des diplômes
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.avaje.ebean.text.json.JsonWriteBeanVisitor;
import com.avaje.ebean.text.json.JsonWriteOptions;
import com.avaje.ebean.text.json.JsonWriter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;


public class Diplomas extends Controller {

    private static final JsonContext json = Ebean.createJsonContext();
    public static final String defaultFieldsList = "(id,name,supervisor(user(id,firstName,lastName)),status)";
    public static final String defaultFieldsUnique = "(id,version,name,passMark,status,supervisor(user(id)),lessons(id,version,course(id,name),teacher(user(id)),coefficient,optional,passMark),periods(id,version,startDate,endDate,ordre))";

	/**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * de diplôme associé à l'id, sinon, elle retourne le diplôme.
     *
     * @param id	un id qui donne l'emplacement du diplôme
     * @return		le diplôme à l'id spécifié / une requête HTTP 400
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result byId(UUID id){
        // id valid
        Diploma diploma = Diploma.find.byId(id);
        if(diploma == null){
            return notFound(String.format(idNotFound, "diploma", id));
        }
        return ok(Json.parse(json.toJsonString(diploma, true, fieldsProperties(defaultFieldsUnique))));
    }

	/**
	 * Cette méthode permet de créer un nouveau diplôme.
	 *
	 * @return		la vue des diplômes
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        Logger.debug("Create diploma ...");
        ObjectNode input = (ObjectNode) request().body().asJson();

        ArrayNode lessons = input.has("lessons") ? (ArrayNode)input.get("lessons") : null;
        for(JsonNode n: lessons){
            ObjectNode l = (ObjectNode) n;
            if(l.has("course")){
                Course course = Course.find.byId(UUID.fromString(l.get("course").asText()));
                if(course == null){
                    return notFound(Json.parse(String.format(idNotFound, "course", l.get("course").asLong())));
                }
                l.replace("course", Json.toJson(course));
            }
            if(l.has("teacher")){
                TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(l.get("teacher").asText()));
                if(teacher == null){
                    return notFound(Json.parse(String.format(idNotFound, "teacher", l.get("teacher").asLong())));
                }
                l.replace("teacher", Json.toJson(teacher));
            }
            n = l;
        }

        if(input.has("supervisor")){
            TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(input.get("supervisor").asText()));
            if(teacher == null){
                return notFound(Json.parse(String.format(idNotFound, "teacher", input.get("supervisor").asLong())));
            }
            input.replace("supervisor", Json.toJson(teacher));
        }


        Diploma diploma = Json.fromJson(input, Diploma.class);

        Map<String,String> err = diploma.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }


        try {
            diploma.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Diplomas.byId(diploma.getId()).url());
        return created(json.toJsonString(diploma, true, fieldsProperties(defaultFieldsUnique)));
    }

	/**
	 * Cette méthode permet de supprimer un diplôme existant.
     *
     * @param id	un id qui donne l'emplacement d'un diplôme
     * @return		un message de réussite / une requête HTTP 200 ou 400
     * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID id){
        Diploma diploma = Diploma.find.byId(id);

        if(diploma == null){
            return notFound(String.format(idNotFound, "diploma", "id"));
        }

        try {
            diploma.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	/**
	 * Cette méthode permet d'éditer un diplôme existant.
     *
     * @param id	un id qui donne l'emplacement d'un diplôme
     * @return		la vue des diplômes
     * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){
        Diploma diploma = Diploma.find.byId(id);
        if(diploma == null){
            return notFound(String.format(idNotFound, "diploma", id));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        // course id to Course
        ArrayNode lessons = input.has("lessons") ? (ArrayNode)input.get("lessons") : null;
        for(JsonNode n: lessons){
            ObjectNode l = (ObjectNode) n;
            if(l.has("course")){
                Course course = Course.find.byId(UUID.fromString(l.get("course").asText()));
                if(course == null){
                    return notFound(Json.parse(String.format(idNotFound, "course", l.get("course").asLong())));
                }
                l.replace("course", Json.toJson(course));
            }
            if(l.has("teacher")){
                TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(l.get("teacher").asText()));
                if(teacher == null){
                    return notFound(Json.parse(String.format(idNotFound, "teacher", l.get("teacher").asLong())));
                }
                l.replace("teacher", Json.toJson(teacher));
            }
            n = l;
        }

        if(input.has("supervisor")){
            TeacherUser teacher = TeacherUser.findByUserId(UUID.fromString(input.get("supervisor").asText()));
            if(teacher == null){
                return notFound(Json.parse(String.format(idNotFound, "teacher", input.get("supervisor").asLong())));
            }
            input.replace("supervisor", Json.toJson(teacher));
        }

        Diploma newDiploma = Json.fromJson(input, Diploma.class);

        Map<String,String> err = newDiploma.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newDiploma.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Diplomas.byId(id).url());
        return ok(Json.parse(json.toJsonString(newDiploma, true, fieldsProperties(defaultFieldsUnique))));
    }

	/**
	 * Cette méthode retourne la liste de tous les diplômes.
	 *
	 * @param limit     la limite d'affichage de diplômes dans la liste
     * @param offset    la position à laquelle on commence à regarder dans la base de donnée pour l'affichage
	 * @return			la liste des diplômes / une requête HTTP 200
	 * @see				Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Diploma.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Diploma> das = Diploma.find.where()
                .findPagingList(limit).getPage(offset).getList();

        JsonWriteOptions opt = fieldsProperties(defaultFieldsList);
        opt.setRootPathVisitor(new JsonWriteBeanVisitor<Diploma>() {
            @Override
            public void visit(Diploma diploma, JsonWriter jsonWriter) {
                jsonWriter.appendRawValue("endYear", diploma.getEndYear().toString());
                jsonWriter.appendRawValue("startYear", diploma.getStartYear().toString());
            }
        });

        return ok(Json.parse(json.toJsonString(das, true, opt)));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result setStatus(UUID id){
        Diploma diploma = Diploma.find.byId(id);

        if(diploma == null){
            return notFound(String.format(idNotFound, "diploma", id));
        }

        ObjectNode input = (ObjectNode)request().body().asJson();
        if(!input.has("status")){
            return badRequest();
        }

        Diploma.DiplomaStatus status = Diploma.DiplomaStatus.valueOf(input.get("status").asText());

        diploma.setStatus(status);
        try {
            diploma.update();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return ok(Json.parse(json.toJsonString(diploma, true, fieldsProperties(defaultFieldsUnique))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result getLessons(UUID id){
        Diploma diploma = Diploma.find.byId(id);

        if(diploma == null){
            return notFound(Json.parse(String.format(idNotFound, "diploma", id)));
        }

        List<Lesson> lessons = diploma.getLessons();

        return ok(Json.parse(json.toJsonString(lessons, true, fieldsProperties(Lessons.defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result getPeriods(UUID id){
        Diploma diploma = Diploma.find.byId(id);

        if(diploma == null){
            return notFound(Json.parse(String.format(idNotFound, "diploma", id)));
        }

        List<Period> periods = diploma.getPeriods();

        return ok(Json.parse(json.toJsonString(periods, true, fieldsProperties(Periods.defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result getInterns(UUID id){
        Diploma diploma = Diploma.find.byId(id);

        if(diploma == null){
            return notFound(Json.parse(String.format(idNotFound, "diploma", id)));
        }

        List<FollowedDiploma> fd = diploma.getFollowedDiplomas();

        List<InternUser> interns = new ArrayList<>();

        for(FollowedDiploma f : fd){
            interns.add(f.getIntern());
        }

        return ok(Json.parse(json.toJsonString(interns, true, fieldsProperties(Interns.fieldsLessons))));
    }
}
