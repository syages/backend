/**
 * Classe des enseignants
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */

package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Domain;
import models.PasswordResetToken;
import models.TeacherUser;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;


public class Teachers extends Controller {

    public static final String defaultFields = "(id,version,user(id,version,firstName,lastName,email),number,office,lessons(id,course(name)),domains(id,name),shareMail)";
    public static final String listFields = "(id,user(id,version,firstName,lastName,email),number,office)";

    private static final JsonContext json = Ebean.createJsonContext();

    /**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * d'enseignant associé à l'id, sinon, elle retourne l'enseignant.
     *
     * @param id	un id qui donne l'emplacement de l'enseignant
     * @return		l'enseignant à l'id spécifié / une requête HTTP 400
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result byId(UUID id){
        TeacherUser teacher = TeacherUser.findByUserId(id);
        if (teacher == null){
            return notFound(Json.parse(String.format(idNotFound, "teacher", id)));
        }
        return ok(Json.parse(json.toJsonString(teacher, true, fieldsProperties(defaultFields))));
    }

	/**
     * Cette méthode permet de créer un utilisateur enseignant.
     *
     * @return	la vue administrateur
     * @see		Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        Logger.debug("Create Teacher ...");

        ObjectNode input = (ObjectNode) request().body().asJson();

        List<Domain> domains = new ArrayList<>();
        if(input.has("domains")){
            if(input.get("domains").isArray()){
                ArrayNode doms = (ArrayNode) input.get("domains");
                for(JsonNode d: doms){
                    Domain domain = Domain.find.byId(UUID.fromString(d.asText()));
                    if(domain == null){
                        return notFound(Json.parse(String.format(idNotFound, "domain", d.asLong())));
                    }
                    domains.add(domain);
                }
                input.replace("domains", Json.toJson(domains));
            }
            else{
                return badRequest(Json.parse("{\"error\":\"domains need to be an Array.\"}"));
            }

        }


        TeacherUser teacher = Json.fromJson(input, TeacherUser.class);
        teacher.getUser().setFlagType(User.Usertype.Teacher);

        Map<String,String> err = teacher.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        PasswordResetToken token = new PasswordResetToken(teacher.getUser());

        try {
            teacher.save();
            token.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        confirmationEmail(teacher.getUser(), token);

        response().setHeader(LOCATION, controllers.api.routes.Teachers.byId(teacher.getId()).url());
        return created(Json.parse(json.toJsonString(teacher, true, fieldsProperties(defaultFields))));
    }

    /**
     * Scope: AdministrativeUser and upper
     * Cette méthode permet de supprimer un enseignant existant,
     * l'id de l'enseignant est nécessaire.
     *
     * @param id	un id qui donne l'emplacement d'un enseignant
     * @return		un message de réussite
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID id){
        TeacherUser teacher = TeacherUser.findByUserId(id);
        if(teacher == null){
            return notFound(Json.parse(String.format(idNotFound, "teacher", id)));
        }
        try {
            teacher.delete();
        } catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Scope: AdministrativeUser or TeacherUser
     * Cette méthode permet d'éditer un enseignant existant,
     * l'id de l'enseignant est nécessaire ainsi que de nouvelles
     * informations.
     *
     * @param id	un id qui donne l'emplacement d'un enseignant
     * @return		la vue utilisateur
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){
        TeacherUser teacher = TeacherUser.findByUserId(id);
        if(teacher == null){
            return notFound(Json.parse(String.format(idNotFound, "teacher", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }


        List<Domain> domains = new ArrayList<>();
        if(input.has("domains")){
            ArrayNode doms = (ArrayNode) input.get("domains");
            for(JsonNode d: doms){
                Domain domain = Domain.find.byId(UUID.fromString(d.asText()));
                if(domain == null){
                    return badRequest(Json.parse(String.format(idNotFound, "domain", d.asLong())));
                }
                domains.add(domain);
            }
            input.replace("domains", Json.toJson(domains));
        }

        TeacherUser newTeacher = Json.fromJson(input, TeacherUser.class);

        Map<String,String> err = newTeacher.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }
        try {
            newTeacher.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Teachers.byId(id).url());
        return ok(Json.parse(json.toJsonString(newTeacher, true, fieldsProperties(defaultFields))));
    }


    /**
	 * Cette méthode retourne la liste de tous les enseignants.
	 *
	 * @return	la liste des diplômes / une requête HTTP 200
	 * @see		Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result list(Integer limit, Integer offset){

        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = TeacherUser.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }


        List<TeacherUser> teachers = TeacherUser.find.where()
                .orderBy("user.lastName, user.firstName")
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(teachers, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result listByDomains(UUID domainId ){
        Domain domain = Domain.find.byId(domainId);
        if(domain == null){
            return notFound(Json.parse(String.format(idNotFound, "domain", domainId)));
        }
        List<TeacherUser> teachers = domain.getTeachers();
        return ok(Json.parse(json.toJsonString(teachers, true, fieldsProperties(defaultFields))));
    }
}
