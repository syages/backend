/**
 * Classe des evaluationsMarks
 * @author SYAGES
 * @version 1.0
 * date 12/01/2015
 */

package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Evaluation;
import models.InternUser;
import models.Mark;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;

public class Marks extends Controller {

    private static final JsonContext json = Ebean.createJsonContext();
    public static final String defaultFields = "(id, version, intern(id,user(firstName,lastName)),status,mark)";
    public static final String defaultFieldsList = "(id, version, intern(user(id,firstName,lastName)),status,mark,evaluation(id,name))";

    /**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * d'evaluationMarks associé aux id, sinon, elle retourne
     * l'evaluationMarks associé à un diplôme.
     *
     * @param id            un id qui donne l'emplacement d'une evaluationMark
     * @return              la note à l'id spécifié / une requête HTTP 400
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result byId(UUID id){
        Mark mark = Mark.find.byId(id);
        if(mark == null){
            return notFound(Json.parse(String.format(idNotFound, "mark", id)));
        }
        return ok(Json.parse(json.toJsonString(mark, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de créer une nouvelle Mark.
     *
     * @return              la vue des enseignants
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();
        if(input.has("evaluation")){
            Evaluation evaluation = Evaluation.find.byId(UUID.fromString(input.get("evaluation").asText()));
            if(evaluation == null){
                return notFound(Json.parse(String.format(idNotFound, "evaluation", input.get("evaluation").asLong())));
            }
            input.replace("evaluation", Json.toJson(evaluation));
        }

        if(input.has("intern")){
            InternUser intern = InternUser.findByUserId(UUID.fromString(input.get("intern").asText()));
            if(intern == null){
                return notFound(Json.parse(String.format(idNotFound, "intern", input.get("intern").asLong())));
            }
            input.replace("intern", Json.toJson(intern));
        }

        Mark mark = Json.fromJson(input, Mark.class);

        Map<String,String> err = mark.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            mark.save();
        }catch (PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Marks.byId(mark.getId()).url());
        return created(Json.parse(json.toJsonString(mark, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet d'éditer une Mark existante.
     *
     * @return              la vue des enseignants
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result edit(UUID id){
        Mark mark = Mark.find.byId(id);

        if(mark == null){
            return badRequest(Json.parse(String.format(idNotFound, "mark", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(input.has("evaluation")){
            Evaluation evaluation = Evaluation.find.byId(UUID.fromString(input.get("evaluation").asText()));
            if(evaluation == null){
                return notFound(Json.parse(String.format(idNotFound, "evaluation", input.get("evaluation").asLong())));
            }
            input.replace("evaluation", Json.toJson(evaluation));
        }

        if(input.has("intern")){
            InternUser intern = InternUser.findByUserId(UUID.fromString(input.get("intern").asText()));
            if(intern == null){
                return notFound(Json.parse(String.format(idNotFound, "intern", input.get("intern").asLong())));
            }
            input.replace("intern", Json.toJson(intern));
        }

        Mark newMark = Json.fromJson(input, Mark.class);

        Map<String,String> err = newMark.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newMark.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return ok(Json.parse(json.toJsonString(newMark, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode retourne la liste de toutes les Mark.
     *
     * @return              la liste des evaluationMark / une requête HTTP 200
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Mark.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }
        List<Mark> marks = Mark.find.where()
                .findPagingList(limit).getPage(offset).getList();

        return ok(json.toJsonString(marks, true, fieldsProperties(defaultFieldsList)));
    }

    /**
     * Cette méthode permet de supprimer une Mark
     * @param markId
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result delete(UUID markId){
        Mark mark = Mark.find.byId(markId);

        if(mark == null){
            return notFound(String.format(idNotFound, "mark", markId));
        }

        try{
            mark.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }
}
