/**
 * Classe des diplomaPeriods
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Diploma;
import models.Evaluation;
import models.Period;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;


public class Periods extends Controller {

    public static final String defaultFields = "(id,version,ordre, diploma(id, name), endDate, startDate)";
    private static final JsonContext json = Ebean.createJsonContext();

	/**
	 * Cette méthode retourne un message d'erreur si il n'y a pas
     * de diplôme/période associé aux id, sinon, elle retourne
     * la période associé à un diplôme.
	 *
	 * @param periodId		un id qui donne l'emplacement d'une période
	 * @return				le diplomaPeriods à l'id spécifié / une requête HTTP 400
	 * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result byId(UUID periodId){
        Period period = Period.find.byId(periodId);

        if(period == null){
            return notFound(Json.parse(String.format(idNotFound, "period", periodId)));
        }

        return ok(Json.parse(json.toJsonString(period, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        if(input.has("diploma")){
            Diploma diploma = Diploma.find.byId(UUID.fromString(input.get("diploma").asText()));
            if(diploma == null) return badRequest(Json.parse(String.format(idNotFound, "diploma", input.get("diploma").asLong())));
            input.replace("diploma", Json.toJson(diploma));
        }

        Period period = Json.fromJson(input, Period.class);

        Map<String,String> err = period.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            period.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return ok(Json.parse(json.toJsonString(period, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result delete(UUID periodId){
        Period period = Period.find.byId(periodId);

        if(period == null){
            return notFound(Json.parse(String.format(idNotFound, "period", periodId)));
        }

        try{
            period.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	/**
	 * Cette méthode permet d'éditer un diplomaPeriods existant.
	 *
	 * @param id		un id qui donne l'emplacement d'une période
	 * @return				la vue des diplomaPeriods
	 * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){
        Period period = Period.find.byId(id);

        if(period == null){
            return notFound(Json.parse(String.format(idNotFound, "period", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(input.has("diploma")){
            Diploma diploma = Diploma.find.byId(UUID.fromString(input.get("diploma").asText()));
            if(diploma == null){
                return notFound(Json.parse(String.format(idNotFound, "diploma", input.get("diploma").asLong())));
            }
            input.replace("diploma", Json.toJson(diploma));
        }

        Period newPeriod = Json.fromJson(input, Period.class);

        Map<String,String> err = newPeriod.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newPeriod.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Periods.byId(id).url());
        return ok(Json.parse(json.toJsonString(newPeriod, true, fieldsProperties(defaultFields))));
    }



	/**
	 * Cette méthode retourne la liste de toutes les périodes
	 * associés à un diplôme.
	 *
	 * @return				la liste de tout les diplomaPeriods
	 * @see					Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result list(Integer limit, Integer offset) {
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Period.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }
        List<Period> periods = Period.find.where()
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(periods, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result getEvaluations(UUID periodId){
        Period period = Period.find.byId(periodId);

        if(period == null){
            return notFound(Json.parse(String.format(idNotFound, "period", periodId)));
        }

        List<Evaluation> evaluations = period.getEvaluationList();

        return ok(Json.parse(json.toJsonString(evaluations, true, fieldsProperties(Evaluations.defaultFields))));
    }

}

