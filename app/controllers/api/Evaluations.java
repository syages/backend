/**
 * Classe des évaluations
 * @author SYAGES
 * @version 1.0
 * date 12/01/2015
 */

package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.*;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;

public class Evaluations extends SecuredController{

    public static final String defaultFields = "(id,version,name,date,coefficient,gradingScale,visible,type,lesson(id,course(id,name),teacher(user(id,firstName,lastName))),period(id,startDate,endDate,ordre))";
    private static final JsonContext json = Ebean.createJsonContext();

    /**
     * Cette méthode retourne la liste de toutes les évaluations.
     *
     * @return              la liste des évaluations / une requête HTTP 200
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President"})
    public static Result list(Integer limit, Integer offset, String sort){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Evaluation.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }
        List<Evaluation> evaluations = Evaluation.find.where()
                .orderBy(sort != null ? sortProperties(sort): "date, lesson.course.name")
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(evaluations, true,fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode retourne un message d'erreur si il n'y a pas
     * d'évaluation associé aux id, sinon, elle retourne
     * l'évaluation associé à un diplôme.
     *
     * @param id  un id qui donne l'emplacement d'une évaluation
     * @return              l'évaluation à l'id spécifié / une requête HTTP 400
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result byId(UUID id){

        Evaluation evaluation = Evaluation.find.byId(id);

        if(evaluation == null){
            return notFound(Json.parse(String.format(idNotFound, "evaluation", id)));
        }

        return ok(Json.parse(json.toJsonString(evaluation, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de créer une nouvelle évaluation.
     *
     * @return              la vue des enseignants
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result create(){
        ObjectNode evaluationNode = (ObjectNode) request().body().asJson();

        if(!evaluationNode.has("lesson")) return badRequest();

        Lesson lesson = Lesson.find.byId(UUID.fromString(evaluationNode.get("lesson").asText()));

        if(lesson == null){
            return notFound(Json.parse(String.format(idNotFound, "lesson", evaluationNode.get("lesson").asText())));
        }

        evaluationNode.put("lesson", Json.toJson(lesson));

        Evaluation evaluation = Json.fromJson(evaluationNode, Evaluation.class);

        Long evalDate = evaluation.getDate();
        Period evalPeriod = new Period();

        for(Period period : lesson.getDiploma().getPeriods()){
            if(period.getStartDate().getTime() <= evalDate && period.getEndDate().getTime() >= evalDate){
                evalPeriod = period;
            }
        }

        evaluation.setPeriod(evalPeriod);

        Map<String,String> err = evaluation.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            evaluation.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        evaluation.refresh();

        response().setHeader(LOCATION, controllers.api.routes.Evaluations.byId(evaluation.getId()).url());
        return created(Json.parse(json.toJsonString(evaluation, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet d'éditer une évaluation existante.
     *
     * @param id  id de l'évaluation à modifier
     * @return              la vue des enseignants
     * @see                 Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result edit(UUID id){
        Evaluation evaluation = Evaluation.find.byId(id);
        if(evaluation == null){
            return notFound(Json.parse(String.format(idNotFound, "evaluation", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        Lesson l = null;
        if(input.has("lesson")){
            l = Lesson.find.byId(UUID.fromString(input.get("lesson").asText()));
            if (l == null) {
                return notFound(Json.parse(String.format(idNotFound, "lesson", input.get("lesson").asText())));
            }
            input.replace("lesson", Json.toJson(l));
        }

        Evaluation newEvaluation = Json.fromJson(input, Evaluation.class);

        Long evalDate = newEvaluation.getDate();
        Period evalPeriod = new Period();

        for(Period period : l.getDiploma().getPeriods()){
            if(period.getStartDate().getTime() <= evalDate && period.getEndDate().getTime() >= evalDate){
                evalPeriod = period;
            }
        }

        newEvaluation.setPeriod(evalPeriod);

        newEvaluation.setMarks(evaluation.getMarks());

        Map<String,String> err = newEvaluation.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newEvaluation.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        newEvaluation.refresh();

        response().setHeader(LOCATION, controllers.api.routes.Evaluations.byId(id).url());
        return ok(Json.parse(json.toJsonString(newEvaluation, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de supprimer une évaluation
     *
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result delete(UUID id){
        Evaluation evaluation = Evaluation.find.byId(id);
        if(evaluation == null){
            return notFound(String.format(idNotFound, "evaluation", id));
        }

        try{
            evaluation.delete();
        }catch (PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Cette méthode permet de récupérer une liste de Mark pour une évaluation
     *
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result getMarks(UUID id){
        Evaluation evaluation = Evaluation.find.byId(id);

        if(oauthUser().getFlagType() == User.Usertype.Teacher && !evaluation.getLesson().getTeacher().getUser().equals(oauthUser()))
            return unauthorized(Json.parse(unauthorized));

        if(evaluation == null){
            return notFound(Json.parse(String.format(idNotFound, "evaluation", id)));
        }

        List<Mark> marks = evaluation.getMarks();

        return ok(Json.parse(json.toJsonString(marks, true, fieldsProperties(Marks.defaultFields))));
    }

    /**
     * Cette méthode permet de créer plusieurs Mark pour une évaluation
     *
     * @param id id de l'évaluation dans laquelle on saisie des notes
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result createMarks(UUID id){
        Evaluation evaluation = Evaluation.find.byId(id);

        if(oauthUser().getFlagType() == User.Usertype.Teacher && !evaluation.getLesson().getTeacher().getUser().equals(oauthUser())){
            return unauthorized(Json.parse(unauthorized));
        }


        if(evaluation == null){
            return notFound(Json.parse(String.format(idNotFound, "evaluation", id)));
        }

        ArrayNode input = (ArrayNode)request().body().asJson();

        List<Mark> marks = new ArrayList<>();

        for(JsonNode markNode: input){
            if(markNode.has("intern")){
                InternUser intern = InternUser.findByUserId(UUID.fromString(markNode.get("intern").asText()));
                if(intern == null){
                    return notFound(Json.parse(String.format(idNotFound, "intern", markNode.get("intern").asText())));
                }
                ObjectNode mNode =(ObjectNode) markNode;
                mNode.replace("intern", Json.toJson(intern));
                mNode.put("evaluation", Json.toJson(evaluation));
            }

            Mark mark = Json.fromJson(markNode, Mark.class);

            Map<String,String> err = mark.validate();
            if(!err.isEmpty()){
                return badRequest(Json.toJson(err));
            }

            marks.add(mark);
        }

        Ebean.beginTransaction();
        try {
            for (Mark mark : marks) {
                try {
                    mark.save();
                } catch (PersistenceException e) {
                    return badRequest(Json.toJson(exceptionCatch(e)));
                }
            }
            Ebean.commitTransaction();
        }
        finally{
            Ebean.endTransaction();
        }


        response().setHeader(LOCATION, controllers.api.routes.Evaluations.getMarks(id).url());
        return created(Json.parse(json.toJsonString(marks, true, fieldsProperties(Marks.defaultFields))));
    }


    /**
     * Cette méthode permet d'éditer plusieurs Mark pour une évaluation
     *
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result editMarks(UUID id){
        Evaluation evaluation = Evaluation.find.byId(id);

        if(oauthUser().getFlagType().toString().equals("TeacherUser") && !evaluation.getLesson().getTeacher().getUser().equals(oauthUser())){
            return unauthorized(Json.parse(unauthorized));
        }

        if(evaluation == null){
            return notFound(Json.parse(String.format(idNotFound, "evaluation", id)));
        }

        ArrayNode input = (ArrayNode) request().body().asJson();

        List<Mark> marks = new ArrayList<>();

        for(JsonNode markNode: input){
            if(markNode.has("intern")){
                InternUser intern = InternUser.findByUserId(UUID.fromString(markNode.get("intern").asText()));
                if(intern == null){
                    return notFound(Json.parse(String.format(idNotFound, "intern", markNode.get("intern").asText())));
                }
                ObjectNode mNode =(ObjectNode) markNode;
                mNode.replace("intern", Json.toJson(intern));
                mNode.put("evaluation", Json.toJson(evaluation));
                markNode = mNode;
            }

            Mark mark = Json.fromJson(markNode, Mark.class);

            Map<String,String> err = mark.validate();
            if(!err.isEmpty()){
                return badRequest(Json.toJson(err));
            }

            marks.add(mark);
        }

        Ebean.beginTransaction();
        try {
            for (Mark mark : marks) {
                try {
                    mark.update();
                } catch (PersistenceException e) {
                    return badRequest(Json.toJson(exceptionCatch(e)));
                }
            }
            Ebean.commitTransaction();
        }
        finally{
            Ebean.endTransaction();
        }


        response().setHeader(LOCATION, controllers.api.routes.Evaluations.getMarks(id).url());
        return ok(Json.parse(json.toJsonString(marks, true, fieldsProperties(Marks.defaultFields))));
    }
}
