/**
 * Classe des administrateurs
 * @author SYAGES
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.util.CORS;
import helpers.util.Divers;
import models.PasswordResetToken;
import models.StaffUser;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;


public class Staff extends Controller {

    public static final String defaultFields = "(id,version,user(id,version,firstName,lastName,email),number,office)";
    private static final JsonContext json = Ebean.createJsonContext();

	/**
	 * Cette méthode retourne un message d'erreur si il n'y a pas
     * d'administrateur associé à l'id, sinon, elle retourne l'administrateur.
     *
     * @param id	un id qui donne l'emplacement d'un administrateur
     * @return		l'administrateur à l'id spécifié / une requête HTTP 400
     * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser","TeacherUser"})
    public static Result byId(UUID id){
        StaffUser staff = StaffUser.findByUserId(id);
        if(staff == null){
            return notFound(Json.parse(String.format(idNotFound, "staff user", id)));
        }

        return ok(Json.parse(json.toJsonString(staff, true, fieldsProperties(defaultFields))));
    }

	/**
     * Cette méthode permet de créer un nouvel administrateur.
     *
     * @return		la vue administrateur
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        Logger.debug("Creating StaffUser ...");
        // on passe par un formulaire qui appelera le validator factory et le validate() automatiquement.
        ObjectNode input =(ObjectNode) request().body().asJson();

        StaffUser staff = Json.fromJson(input, StaffUser.class);
        staff.getUser().setFlagType(User.Usertype.Staff);

        Map<String,String> err = staff.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        PasswordResetToken token = new PasswordResetToken(staff.getUser());
        //save
        try {
            staff.save();
            token.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        confirmationEmail(staff.getUser(), token);

        response().setHeader(LOCATION, controllers.api.routes.Staff.byId(staff.getId()).url());
        return created(Json.parse(json.toJsonString(staff, true, fieldsProperties(defaultFields))));
    }


    /**
     * Cette méthode permet d'éditer un administrateur existant.
     *
     * @param id	un id qui donne l'emplacement d'un administrateur
     * @return		la vue administrateur
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id) {
        StaffUser staff = StaffUser.findByUserId(id);
        if (staff == null) {
            return notFound(String.format(idNotFound, "staff", id));
        }

        StaffUser newStaff = Json.fromJson(request().body().asJson(), StaffUser.class);

        if(!newStaff.getId().equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        Map<String, String> err = newStaff.validate();
        if (!err.isEmpty()) {
            return badRequest(Json.toJson(err));
        }
        try {
            newStaff.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Staff.byId(id).url());
        return ok(Json.parse(json.toJsonString(newStaff, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de supprimer un administrateur existant.
     *
     * @param id	un id qui donne l'emplacement d'un utilisateur
     * @return		un message de réussite / une requête HTTP 200 ou 400
     * @see			Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID id) {
        StaffUser staff = StaffUser.findByUserId(id);

        if (staff == null) {
            return notFound(String.format(Divers.idNotFound, "administrative user", id));
        }
        try {
            staff.delete();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }
    /**
	 * Cette méthode retourne la liste de tous les administrateurs.
	 *
	 * @param limit		la limite d'affichage d'administrateurs dans la liste
	 * @param offset	la position à laquelle on commence à regarder dans la base de donnée pour l'affichage
	 * @return			la liste des administrateurs / une requête HTTP 200
	 * @see				Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = StaffUser.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<StaffUser> staff = StaffUser.find.where()
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(staff, true, fieldsProperties(defaultFields))));
    }


}
