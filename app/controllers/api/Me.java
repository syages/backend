package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;

import com.fasterxml.jackson.databind.node.ObjectNode;
import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.*;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.UUID;

import static helpers.util.Divers.*;

public class Me extends SecuredController {

    private static final JsonContext json = Ebean.createJsonContext();

	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result logged() {
        User user = ((AccessToken) Http.Context.current().args.get("oauth_token")).getOwner();
        user.refresh();
        return ok(Json.toJson(user));
    }

	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result loggedTokens() {
        User user = oauthUser();
        List<AccessToken> tokens = user.associatedTokens();

        return ok(Json.parse(json.toJsonString(tokens, true, fieldsProperties("(id,version,consumer(author,name),expiration)"))));
    }

    /**
     * Cette méthode permet de retourner la liste des absences déclarées du stagiaire connecté
     *
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result getDeclaredAbsences(){
        return Interns.getDeclaredAbsences(((AccessToken) Http.Context.current().args.get("oauth_token")).getOwner().getId());
    }

    /**
     * Cette méthode permet de retourner la liste des absences notées du stagiaire connecté
     *
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result getNotedAbsences(){
        return Interns.getNotedAbsences(((AccessToken) Http.Context.current().args.get("oauth_token")).getOwner().getId());
    }

    /**
     * Cette méthode permet de retourner la liste des leçons d'un enseignant connecté
     *
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"TeacherUser"})
    public static Result getLessons(){
        TeacherUser teacher = TeacherUser.findByUserId(oauthUser().getId());

        if(teacher == null){
            return notFound(Json.parse(String.format(idNotFound, "teacher", oauthUser().getId())));
        }

        List<Lesson> lessons = teacher.getLessons();

        return ok(Json.parse(json.toJsonString(lessons, true, fieldsProperties(Lessons.defaultFields))));
    }

    /**
     * Cette méthode permet de changer le mot de passe de l'utilisateur connecté
     *
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result setPassword(String password){
        return Users.setPassword(oauthUser().getId(), password);
    }

    /**
     * Cette méthode permet de changer la photo de l'utilisateur connecté
     *
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result setPicture(){
        return Users.setPicture(oauthUser().getId());
    }

}
