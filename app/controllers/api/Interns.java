/**
 * Classe des stagiaires
 * @author SYAGE
 * @version 1.0
 * date 10/01/2015
 */
 
package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.avaje.ebean.text.json.JsonWriteBeanVisitor;
import com.avaje.ebean.text.json.JsonWriteOptions;
import com.avaje.ebean.text.json.JsonWriter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import models.*;
import play.Logger;
import play.libs.Json;
import play.mvc.Result;
import helpers.util.CORS;

import javax.persistence.PersistenceException;

import java.util.*;

import static helpers.util.Divers.*;


public class Interns extends SecuredController {

    public static final String defaultFields = "(id,version,infos(id,job,lastSchool,lastSchoolYear,professionalProject,worked,version),user(id,version,firstName,lastName,email),dateOfBirth,followedDiplomas(id,version,paid,status,diploma(id,name),optionalLessons(id,course(name)))";

    public static final String fieldsLessons = "(id,version,user(id,firstName,lastName,email))";

    private static final JsonContext json = Ebean.createJsonContext();
	/**
	 * Cette méthode retourne un message d'erreur si il n'y a pas
     * de stagiaires associé à l'id, sinon, elle retourne
     * le stagiaire.
	 *
	 * @param id	un id qui donne l'emplacement d'un diplôme
	 * @return		le stagiaire à l'id spécifié / une requête HTTP 400
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result byId(UUID id){
        InternUser intern = InternUser.findByUserId(id);
        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        if(oauthUser().getFlagType()==User.Usertype.Intern && !intern.getId().equals(oauthUser().getId())){
            return unauthorized(Json.parse(unauthorized));
        }

        return ok(Json.parse(json.toJsonString(intern, true, fieldsProperties(defaultFields))));
    }

	/**
	 * Cette méthode permet de créer un nouveau stagiaire.
	 *
	 * @return		la vue administrateur / enseignant
	 * @see			Result
	 */
	@CORS
    //@OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result create(){
        Logger.debug("Create Intern ...");
        ObjectNode input = (ObjectNode) request().body().asJson();

        ObjectNode fDiplomaNode = null;
        FollowedDiploma fd = new FollowedDiploma();
        if(input.has("followedDiplomas")){
            fDiplomaNode = (ObjectNode) input.get("followedDiplomas").get(0);
            input.remove("followedDiplomas");

            if(!(fDiplomaNode.has("optionalLessons") && fDiplomaNode.has("diploma") && fDiplomaNode.has("paid"))){
                return badRequest(Json.parse("{\"error\":\"You have to provide a complete FollowedDiploma object\"}"));
            }

            List<Lesson> lessons = new ArrayList<>();
            for(JsonNode lNode: fDiplomaNode.get("optionalLessons") ){
                Lesson l = Lesson.find.byId(UUID.fromString(lNode.asText()));
                if(l == null){
                    return notFound(Json.parse(String.format(idNotFound, "lesson", lNode.asLong())));
                }
                lessons.add(l);
            }


            Diploma diploma = Diploma.find.byId(UUID.fromString(fDiplomaNode.get("diploma").asText()));
            if(diploma == null){
                return notFound(Json.parse(String.format(idNotFound, "diploma", fDiplomaNode.get("diploma").asLong())));
            }

            fd.setDiploma(diploma);
            fd.setOptionalLessons(lessons);
            fd.setPaid(fDiplomaNode.get("paid").asBoolean());
        }

        InternUser intern = Json.fromJson(input, InternUser.class);
        intern.getUser().setFlagType(User.Usertype.Intern);
        if(fDiplomaNode != null){
            intern.addFollowedDiploma(fd);
        }

        Map<String,String> err = intern.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        PasswordResetToken token = new PasswordResetToken(intern.getUser());

        try {
            intern.save();
            token.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        confirmationEmail(intern.getUser(), token);

        response().setHeader(LOCATION, controllers.api.routes.Interns.byId(intern.getId()).url());
        return created(Json.parse(json.toJsonString(intern, true, fieldsProperties(defaultFields))));
    }

	/**
	 * Cette méthode permet de supprimer un stagiaire existant.
	 *
	 * @param id	un id qui donne l'emplacement d'un stagiaire
	 * @return		un message de réussite / une requête HTTP 200 ou 400
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result delete(UUID id){
        InternUser intern = InternUser.findByUserId(id);
        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        try {
            intern.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	/**
	 * Cette méthode permet d'éditer un stagiaire existant.
	 *
	 * @param id	un id qui donne l'emplacement d'un stagiaire
	 * @return		la vue administrateur / enseignant / une requête HTTP 400
	 * @see			Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser"})
    public static Result edit(UUID id){

        InternUser intern = InternUser.findByUserId(id);
        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        List<FollowedDiploma> internFds = intern.getFollowedDiplomas();

        ObjectNode input = (ObjectNode) request().body().asJson();
        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        if(input.has("followedDiplomas")){
            ArrayNode fdNode = (ArrayNode)input.get("followedDiplomas");
            for(JsonNode fd: fdNode){
                ObjectNode fdObject = (ObjectNode) fd;

                Boolean in = false;
                for(FollowedDiploma internFd: internFds){
                    if(UUID.fromString(fdObject.get("id").asText()) ==internFd.getId()){
                        in = true;
                        break;
                    }
                }
                if(!in){
                    return badRequest(Json.parse(noMatch));
                }

                if(fdObject.has("diploma")){
                    Diploma diploma = Diploma.find.byId(UUID.fromString(fdObject.get("diploma").asText()));
                    if(diploma != null){
                        fdObject.replace("diploma", Json.toJson(diploma));
                    }
                }
                if(fdObject.has("optionalLessons")){
                    List<Lesson> lessons = new ArrayList<>();
                    for(JsonNode lNode: fdObject.get("optionalLessons")){
                        Lesson l = Lesson.find.byId(UUID.fromString(lNode.get("id").asText()));
                        lessons.add(l);
                    }
                    fdObject.replace("optionalLessons", Json.toJson(lessons));
                }
            }
        }else{
            return badRequest(Json.parse("{\"error\":\"Invalid Json - missing followedDiplomas array.\"}"));
        }


        InternUser newIntern = Json.fromJson(input, InternUser.class);

        Map<String,String> err = intern.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newIntern.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Interns.byId(id).url());
        return ok(Json.parse(json.toJsonString(newIntern, true, fieldsProperties(defaultFields))));
    }

	/**
     * Cette méthode permet d'éditer des informations de profil
     * déjà existantes.
     *
     * @param id    un id qui donne l'emplacement d'un utilisateur
     * @return          la vue stagiaire / une requête HTTP 400
     * @see             Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "InternUser"})
    public static Result editInfos(UUID id){
        InternInfo infos = Json.fromJson(request().body().asJson(), InternInfo.class);
        InternUser intern = InternUser.findByUserId(id);


        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        if(!infos.getId().equals(intern.getInfos().getId())){
            return badRequest(Json.parse(noMatch));
        }

        Map<String,String> err = infos.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            infos.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return ok(Json.toJson(infos));
    }

    /**
     * Cette méthode permet d'ajouter des informations de profil.
     *
     * @param id    un id qui donne l'emplacement d'un utilisateur
     * @return          la vue stagiaire / une requête HTTP 400
     * @see             Result
     */
	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result infos(UUID id){
        InternUser intern = InternUser.findByUserId(id);

        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        InternInfo infos = intern.getInfos();

        if (infos != null){
            return ok(Json.toJson(infos));
        }
        return noContent();
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"InternUser"})
    public static Result isFirst(UUID id){
        InternUser intern = InternUser.findByUserId(id);

        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        return ok(Json.parse("{\"first\":"+ (intern.getInfos() == null ? "true": "false") +"}"));
    }

	@CORS
    @OAuthRequire(accessibleByMethod = "")
    public static Result getMarks(UUID userId, UUID followedDiplomaId){
        InternUser intern = InternUser.findByUserId(userId);

        if(intern == null){
            return notFound(String.format(idNotFound, "intern", userId));
        }

        if(oauthUser().getFlagType()==User.Usertype.Intern && !intern.getId().equals(oauthUser().getId())){
            return unauthorized(Json.parse(unauthorized));
        }

        JsonNodeFactory factory = JsonNodeFactory.instance;

        List<Mark> markList = Mark.find.where().eq("intern", intern).findList();

        // On cherche récupère la formation donné dans l'URL, ou on cherche la formation la plus récente
        FollowedDiploma fd = followedDiplomaId == null ? null: FollowedDiploma.find.byId(followedDiplomaId);
        if(followedDiplomaId == null){
            List<FollowedDiploma> fds = FollowedDiploma.find.where().eq("intern", intern).findList();
            for(FollowedDiploma f: fds){
                if(fd == null || fd.getDiploma().getEndYear().isBefore(f.getDiploma().getEndYear())){
                    fd = f;
                }
            }
        }

        ObjectNode node = new ObjectNode(factory);
        node.put("diploma", Json.parse(json.toJsonString(fd.getDiploma(), true, fieldsProperties("(id, name, status, passMark)"))));
        node.put("status", Json.toJson(fd.getStatus()));
        ArrayNode periods = new ArrayNode(factory);
        Float global_average = 0F;
        for(Period p: fd.getDiploma().getPeriods()){
            Float period_average = 0F;
            Float period_lessons_sum_coeff = 0F;
            ObjectNode period = new ObjectNode(factory);
            period.put("period", Json.parse(json.toJsonString(p, true, fieldsProperties("(id, ordre, startDate, endDate)"))));
            ArrayNode lessonsNode = new ArrayNode(factory);
            for(Lesson l: fd.getAllLessons()){
                ObjectNode lessonNode = new ObjectNode(factory);
                lessonNode.put("lesson",Json.parse(json.toJsonString(l, true, fieldsProperties("(id,course(id,name),passMark, coefficient)"))));
                ArrayNode evalArray = new ArrayNode(factory);
                Float lesson_average = 0F;
                Float lesson_evals_sum_coeff = 0F;
                for(Mark m: markList) {
                    if (m.getEvaluation().getPeriod().equals(p) && m.getEvaluation().getLesson().equals(l)) {
                        Long eval_gradingScale = m.getEvaluation().getGradingScale();
                        Float eval_coeff = m.getEvaluation().getCoefficient();
                        Float m_mark = m.getMark();
                        Mark.EvalInternStatus m_status = m.getStatus();
                        // Absent -> 0
                        switch(m_status){
                            case Excuse:
                                break;
                            case Absent:
                                m_mark = 0F;
                            case Present:
                                if (m.getEvaluation().getVisible() || oauthUser().getFlagType() != User.Usertype.Intern){
                                    evalArray.add(Json.parse(json.toJsonString(m, true, fieldsProperties("(id,mark,status,evaluation(name,date,coefficient,type,gradingScale))"))));
                                }
                                lesson_average += 20 / (eval_gradingScale == null ? 0 : eval_gradingScale) * m_mark * (eval_coeff == null ? 0: eval_coeff);
                                lesson_evals_sum_coeff += eval_coeff == null ? 0: eval_coeff;
                                break;
                        }
                    }
                }
                lessonNode.put("evaluations", evalArray);
                lessonNode.put("average", lesson_evals_sum_coeff != 0 ? lesson_average / lesson_evals_sum_coeff: 0);
                period_average += lesson_evals_sum_coeff != 0 ? (lesson_average / lesson_evals_sum_coeff) * l.getCoefficient() : 0;
                period_lessons_sum_coeff += lesson_evals_sum_coeff == 0 ? 0: l.getCoefficient();
                lessonsNode.add(lessonNode);
            }
            period.put("marks", lessonsNode);
            period.put("average", period_lessons_sum_coeff != 0 ? (period_average / period_lessons_sum_coeff) : 0);
            global_average += period_lessons_sum_coeff != 0 ? period_average / period_lessons_sum_coeff : 0;
            periods.add(period);
        }
        node.put("periods", periods);
        int period_passed = fd.getDiploma().getPeriodByDate(new Date());
        node.put("average", global_average / period_passed);


        return ok(node);

    }

    /**
	 * Cette méthode retourne la liste de tous les stagiaires.
	 *
	 * @param limit		la limite d'affichage de stagiaires dans la liste
     * @param offset    la position à laquelle on commence à regarder dans la base de donnée pour l'affichage
	 * @return			la liste des stagiaires / une requête HTTP 200
	 * @see				Result
	 */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = InternUser.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<InternUser> interns = InternUser.find.where().findPagingList(limit).getPage(offset).getList();

        return ok(Json.parse(json.toJsonString(interns, true, fieldsProperties(defaultFields))));
    }

    /**
     * Cette méthode permet de sauvegarder les informations de profil d'un utilisateur.
     *
     * @param userId    un id qui donne l'emplacement d'un utilisateur
     * @return          la vue stagiaires / une requête HTTP 400
     * @see             Result
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "InternUser"})
    public static Result saveInfos(UUID userId){
        InternInfo infos = Json.fromJson(request().body().asJson(), InternInfo.class);
        InternUser intern = InternUser.findByUserId(userId);

        if(intern == null){
            return notFound(String.format(idNotFound, "intern", userId));
        }

        intern.setInfos(infos);
        try{
            intern.update();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "InternUser"})
    public static Result getLessons(UUID id){
        InternUser intern = InternUser.findByUserId(id);

        if(intern == null){
            return notFound(String.format(idNotFound, "intern", id));
        }

        if(oauthUser().getFlagType()==User.Usertype.Intern && !intern.getId().equals(oauthUser().getId())){
            return unauthorized(Json.parse(unauthorized));
        }

        JsonNodeFactory factory = JsonNodeFactory.instance;

        ArrayNode lessonsNode = new ArrayNode(factory);

        for(FollowedDiploma fd: intern.getFollowedDiplomas()){
            if(fd.getStatus() == FollowedDiploma.FollowedDiplomaStatus.Undergoing){
                for(Lesson l: fd.getAllLessons()){
                    lessonsNode.add(Json.toJson(l));
                }
            }
        }
        return ok(lessonsNode);

    }

	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser", "President", "Administrator"})
    public static Result getNotedAbsences(UUID id){
        InternUser intern = InternUser.findByUserId(id);

        if(intern == null){
            return notFound(Json.parse(String.format(idNotFound, "intern", id)));
        }

        List<NotedAbsence> nas = intern.getNotedAbsences();

        JsonWriteOptions opt = fieldsProperties(notedAbsences.defaultFields);
        opt.setRootPathVisitor(new JsonWriteBeanVisitor<NotedAbsence>() {
            @Override
            public void visit(NotedAbsence n, JsonWriter jsonWriter) {
                jsonWriter.appendQuoteEscapeValue("date", String.valueOf(n.getDate().getTime()));
                jsonWriter.appendQuoteEscapeValue("timeOfDay", n.getTimeOfDay().toString());
            }
        });

        return ok(Json.parse(json.toJsonString(nas, true, opt)));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"StaffUser", "TeacherUser", "President", "Administrator"} )
    public static Result getDeclaredAbsences(UUID id){
        InternUser intern = InternUser.findByUserId(id);

        if(intern == null){
            return notFound(Json.parse(String.format(idNotFound, "intern", id)));
        }

        List<DeclaredAbsence> das = intern.getDeclaredAbsences();

        return ok(Json.parse(json.toJsonString(das, true, fieldsProperties(declaredAbsences.defaultFields))));
    }
}
