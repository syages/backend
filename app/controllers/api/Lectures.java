package controllers.api;

import com.aureo.oauth.play.OAuthRequire;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.avaje.ebean.text.json.JsonWriteBeanVisitor;
import com.avaje.ebean.text.json.JsonWriteOptions;
import com.avaje.ebean.text.json.JsonWriter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.*;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;


public class Lectures extends SecuredController {

    public static final String defaultFields = "(id,version,lesson(id,course(name)),date,classroom,notedAbsences(id,version,intern(user(id,firstName,lastName))), duration)";
    public static final String defaultFieldsList = "(id,version,lesson(id,course(name)),date,classroom)";
    private static final JsonContext json = Ebean.createJsonContext();

    @OAuthRequire(accessibleByValues = {"Administrator", "President", "StaffUser", "TeacherUser"})
    public static Result byId(UUID id){
        Lecture lecture = Lecture.find.byId(id);
        if(lecture == null){
            return notFound(Json.parse(String.format(idNotFound, "lecture", id)));
        }
        return ok(Json.parse(json.toJsonString(lecture, true, fieldsProperties(defaultFields))));
    }

    /**
     * Création d'une Lecture.
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator","President","TeacherUser"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        if(input.has("lesson")){
            Lesson lesson = Lesson.find.byId(UUID.fromString(input.get("lesson").asText()));
            if(lesson == null){
                return notFound(Json.parse(String.format(idNotFound, "lesson", input.get("lesson").asText())));
            }
            input.replace("lesson", Json.toJson(lesson));
        }

        Lecture lecture = Json.fromJson(input, Lecture.class);

        Map<String,String> err = lecture.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try{
            lecture.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        response().setHeader(LOCATION, controllers.api.routes.Lectures.byId(lecture.getId()).url());
        return created(Json.parse(json.toJsonString(lecture, true, fieldsProperties(defaultFields))));
    }

    /**
     * Edition d'une Lecture.
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result delete(UUID id){
        Lecture lecture = Lecture.find.byId(id);

        if(lecture == null){
            return notFound(Json.parse(String.format(idNotFound, "lecture", id)));
        }

        try{
            lecture.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }
        return noContent();
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result edit(UUID id){
        Lecture lecture = Lecture.find.byId(id);

        if(lecture == null){
            return notFound(Json.parse(String.format(idNotFound, "lecture", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }


        if(input.has("lesson")){
            Lesson lesson = Lesson.find.byId(UUID.fromString(input.get("lesson").asText()));
            if(lesson == null){
                return notFound(Json.parse(String.format(idNotFound, "lesson", input.get("lesson").asText())));
            }
            input.replace("lesson", Json.toJson(lesson));
        }

        Lecture newLecture = Json.fromJson(input, Lecture.class);

        newLecture.setNotedAbsences(lecture.getNotedAbsences());


        Map<String,String> err = newLecture.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newLecture.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }


        response().setHeader(LOCATION, controllers.api.routes.Lectures.byId(id).url());
        return ok(Json.parse(json.toJsonString(newLecture, true, fieldsProperties(defaultFields))));
    }

	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result list(Integer limit, Integer offset, String sort){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = Lecture.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Lecture> lectures = Lecture.find.where()
                .orderBy(sort != null ? sortProperties(sort) : "lesson.course.name")
                .findPagingList(limit).getPage(offset).getList();
        return ok(Json.parse(json.toJsonString(lectures, true, fieldsProperties(defaultFieldsList))));
    }


    /**
     * Cette méthode permet de récupérer les absents pour une Lecture
     *
     * @param lectureId
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result getAbsences(UUID lectureId){
        Lecture lecture = Lecture.find.byId(lectureId);

        if(lecture == null){
            return notFound(Json.parse(String.format(idNotFound, "lecture", lectureId)));
        }

        List<NotedAbsence> nas = lecture.getNotedAbsences();

        JsonWriteOptions opt = fieldsProperties(notedAbsences.defaultFields);
        opt.setRootPathVisitor(new JsonWriteBeanVisitor<NotedAbsence>() {
            @Override
            public void visit(NotedAbsence n, JsonWriter jsonWriter) {
                jsonWriter.appendQuoteEscapeValue("date", String.valueOf(n.getDate().getTime()));
                jsonWriter.appendQuoteEscapeValue("timeOfDay", n.getTimeOfDay().toString());
            }
        });


        return ok(Json.parse(json.toJsonString(nas, true, opt)));
    }


    /**
     * Cette méthode permet de créer des absences (1 ou plusieurs) pour une Lecture
     *
     * @param id
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator", "President", "TeacherUser"})
    public static Result createAbsences(UUID id){
        Lecture lecture = Lecture.find.byId(id);
        User observer = oauthUser();
        if(lecture == null){
            return notFound(Json.parse(String.format(idNotFound, "lecture", id)));
        }
        if(observer.getFlagType().equals(User.Usertype.Teacher) && !lecture.getLesson().getTeacher().getUser().equals(observer)){
            return unauthorized(Json.parse(unauthorized));
        }


        List<NotedAbsence> old = NotedAbsence.find.where().eq("lecture", lecture).findList();

        List<NotedAbsence> fromInput = new ArrayList<>();

        JsonNode input = request().body().asJson();

        if(input.isArray()){
            for(JsonNode nANode: input){
                if (nANode.isContainerNode()){
                    for(NotedAbsence n : old){
                        if(n.getId() == UUID.fromString(nANode.get("id").asText())){
                            fromInput.add(n);
                            break;
                        }
                    }
                }
                else if(nANode.isValueNode()){

                    InternUser intern = InternUser.findByUserId(UUID.fromString(nANode.textValue()));
                    if(intern == null){
                        return notFound(Json.parse(String.format(idNotFound, "intern", nANode.longValue())));
                    }
                    NotedAbsence newNoted = new NotedAbsence();
                    newNoted.setIntern(intern);
                    newNoted.setLecture(lecture);
                    newNoted.setObserverUser(observer);
                    fromInput.add(newNoted);
                }
            }
        }
        else {
            InternUser intern = InternUser.findByUserId(UUID.fromString(input.textValue().substring(2, input.textValue().length() - 2)));
            if(intern == null){
                return notFound(Json.parse(String.format(idNotFound, "intern", input.asText())));
            }
            NotedAbsence newNoted = new NotedAbsence(intern, lecture, observer);

            fromInput.add(newNoted);
        }


        if(input.isArray()){
            lecture.setNotedAbsences(fromInput);
            for(NotedAbsence n: old){
                Boolean in = false;
                for(NotedAbsence nNEw: fromInput){
                    if(nNEw.getId() != null && nNEw.getId() == n.getId()){
                        in = true;
                        break;
                    }
                }
                if(!in){
                    try{
                        n.delete();
                    }catch(PersistenceException e){
                        return badRequest(Json.toJson(exceptionCatch(e)));
                    }
                }
            }
        }
        else {
            lecture.getNotedAbsences().add(fromInput.get(0));
        }

        Map<String,String> err = lecture.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            lecture.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }



        response().setHeader(LOCATION, controllers.api.routes.Lectures.getAbsences(id).url());
        return created(Json.parse(json.toJsonString(fromInput, true, fieldsProperties(notedAbsences.defaultFieldsLecture))));
    }
}
