package controllers.api;

import helpers.oauth.SecuredController;
import helpers.syages.SyagesConfig;
import helpers.util.CORS;
import helpers.util.Divers;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.PersistenceException;

import models.Administrator;
import models.PasswordResetToken;
import models.User;
import play.libs.Json;
import play.mvc.Result;

import com.aureo.oauth.play.OAuthRequire;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static helpers.util.Divers.*;

public class Administrators extends SecuredController {

    /**
     * Cette méthode permet de récupérer un administrateur
     *
     * @param id id de l'administrateur a récupérer
     * @return 200 - Ok
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result byId(UUID id){
        Administrator admin = Administrator.findByUserId(id);

        if(admin == null){
            return notFound(Json.parse(String.format(idNotFound, "administrator", id)));
        }

        return ok(Json.toJson(admin));
    }

    /**
     * Cette méthode permet de créer un administrateur`
     *
     * @return 200 - Ok
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        Administrator admin = Json.fromJson(input, Administrator.class);
        admin.getUser().setFlagType(User.Usertype.Administrator);


        Map<String,String> err = admin.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        PasswordResetToken token = new PasswordResetToken(admin.getUser());
        try{
            admin.save();
            token.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        confirmationEmail(admin.getUser(), token);

        response().setHeader(LOCATION, controllers.api.routes.Administrators.byId(admin.getId()).url());
        return created(Json.toJson(admin));
    }

    /**
     * Cette méthode permet de supprimer un administrateur
     *
     * @param id id de l'admin à supprimer
     * @return 204 - No Content
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result delete(UUID id){
        Administrator admin = Administrator.findByUserId(id);

        if(admin == null){
            return notFound(Json.parse(String.format(idNotFound, "administrator", id)));
        }

        try{
            admin.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

    /**
     * Cette méthode permet de modifier un administrateur
     * @param id id de l'admin à modifier
     * @return
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result edit(UUID id){
        Administrator admin = Administrator.findByUserId(id);

        if(admin == null){
            return notFound(Json.parse(String.format(idNotFound, "administrator", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        Administrator newAdmin = Json.fromJson(input, Administrator.class);

        Map<String,String> err = newAdmin.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }


        try {
            newAdmin.update();
        } catch (PersistenceException e) {
            return badRequest(Json.toJson(exceptionCatch(e)));
        }


        response().setHeader(LOCATION, controllers.api.routes.Administrators.byId(id).url());
        return ok(Json.toJson(newAdmin));
    }

    /**
     * Cette méthode permet de lister tous les administrateurs
     *
     * @return 200 - Ok
     */
	@CORS
    @OAuthRequire(accessibleByValues = {"Administrator"})
    public static Result list(Integer limit, Integer offset, String sort){
        limit = formatLimit(limit);
        offset = formatOffset(offset);

        Integer total_count = Administrator.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<Administrator> admins = Administrator.find.where().order(sort != null ?Divers.sortProperties(sort) : "user.lastName, user.firstName").findPagingList(limit).getPage(offset).getList();

        response().setHeader("Link", routeHeader(limit, offset, sort, total_count, request().path()));

        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        return ok(Json.toJson(admins));
    }

}
