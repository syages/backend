package controllers.api;

import com.fasterxml.jackson.databind.node.ObjectNode;

import helpers.oauth.SecuredController;
import helpers.util.CORS;
import models.PasswordResetToken;
import models.President;
import models.User;
import play.libs.Json;
import play.mvc.Result;

import javax.persistence.PersistenceException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static helpers.util.Divers.*;
import static helpers.syages.SyagesDatabase.*;


public class Presidents extends SecuredController {
    /**
     * Cette méthode permet de récupérer le Président
     * @param id id du Président
     * @return
     */
	@CORS
    public static Result byId(UUID id){
        President president = President.findByUserId(id);
        if(president == null){
            return notFound(Json.parse(String.format(idNotFound, "president", id)));
        }

        return ok(Json.toJson(president));

    }

    /**
     * Cette méthode permet de créer un nouveau Président
     * @return
     */
	@CORS
    public static Result create(){
        ObjectNode input = (ObjectNode) request().body().asJson();

        President president = Json.fromJson(input, President.class);
        president.getUser().setFlagType(User.Usertype.President);

        Map<String, String> err = president.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        PasswordResetToken token = new PasswordResetToken(president.getUser());
        try{
            president.save();
            token.save();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        //loadSchedule(president);
        confirmationEmail(president.getUser(), token);

        response().setHeader(LOCATION, controllers.api.routes.Presidents.byId(president.getId()).url());
        return created(Json.toJson(president));
    }

	@CORS
    public static Result delete(UUID id){
        President president = President.findByUserId(id);
        if(president == null){
            return notFound(Json.parse(String.format(idNotFound, "president", id)));
        }

        try{
            president.delete();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        return noContent();
    }

	@CORS
    public static Result edit(UUID id){
        President president = President.findByUserId(id);
        if(president == null){
            return notFound(Json.parse(String.format(idNotFound, "president", id)));
        }

        ObjectNode input = (ObjectNode) request().body().asJson();

        if(!UUID.fromString(input.get("id").asText()).equals(id)){
            return badRequest(Json.parse(noMatch));
        }

        President newPresident = Json.fromJson(input, President.class);

        Map<String,String> err = newPresident.validate();
        if(!err.isEmpty()){
            return badRequest(Json.toJson(err));
        }

        try {
            newPresident.update();
        }catch(PersistenceException e){
            return badRequest(Json.toJson(exceptionCatch(e)));
        }

        loadSchedule(newPresident);

        response().setHeader(LOCATION, controllers.api.routes.Presidents.byId(id).url());
        return ok(Json.toJson(newPresident));
    }

	@CORS
    public static Result list(Integer limit, Integer offset){
        limit = limit <= 0 ? 25 : limit > 100 ? 100 : limit;
        offset = offset < 0 ? 0 : offset;

        Integer total_count = President.find.findRowCount();
        response().setHeader("X-Total-Count", total_count.toString());
        response().setHeader(CONTENT_RANGE, (offset * limit) + "-" + (((offset+1) * limit) - 1) + "/" + total_count );

        Integer last_offset = total_count / limit;

        if( offset > last_offset ){
            return badRequest(Json.parse("{\"error\":\"Offset too high.\"}"));
        }

        List<President> presidents = President.find.where()
                .findPagingList(limit).getPage(offset).getList();

        return ok(Json.toJson(presidents));
    }

	@CORS
    public static Result dumpDatabase(){
        Boolean status = dbDumpEmail();
        return ok(Json.parse("{\"status\":\"" + (status ? "envoi": "echec") + "\"}"));
    }
}
