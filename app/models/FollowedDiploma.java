package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class FollowedDiploma extends BaseModel {

    public enum FollowedDiplomaStatus{
        Failed("Ajourne"),
        Undergoing("En cours"),
        Validated("Admis");

        private String name="";

        FollowedDiplomaStatus(String name){
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }

    public static Finder<UUID,FollowedDiploma> find = new Finder<UUID,FollowedDiploma>(UUID.class, FollowedDiploma.class);

    private String appreciation;

    @ManyToOne
    @Constraints.Required
    private Diploma diploma;

    @ManyToMany(mappedBy = "followedDiplomas")
    private List<Lesson> optionalLessons = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private FollowedDiplomaStatus status = FollowedDiplomaStatus.Undergoing;

    @ManyToOne
    private InternUser intern;

    @Constraints.Required
    private Boolean paid;


    public FollowedDiploma(){
        // keep empty
    }


    public String getAppreciation() {
        return appreciation;
    }

    public void setAppreciation(String appreciation) {
        this.appreciation = appreciation;
    }

    public Diploma getDiploma() {
        return diploma;
    }

    public void setDiploma(Diploma diploma) {
        this.diploma = diploma;
    }

    public List<Lesson> getOptionalLessons() {
        return optionalLessons;
    }

    public void setOptionalLessons(List<Lesson> optionalLessons) {
        this.optionalLessons = optionalLessons;
    }

    public FollowedDiplomaStatus getStatus() {
        return status;
    }

    public void setStatus(FollowedDiplomaStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public InternUser getIntern() {
        return intern;
    }

    public void setIntern(InternUser intern) {
        this.intern = intern;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    @JsonIgnore
    public List<Lesson> getAllLessons() {
        List<Lesson> inDiploma = this.getDiploma().getLessons();
        List<Lesson> followedLessons = new ArrayList<>();
        for(Lesson l: inDiploma){
            if(!l.getOptional()){
                followedLessons.add(l);
            }
        }
        followedLessons.addAll(this.optionalLessons);

        return followedLessons;
    }

    public Map<String,String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        return errors;
    }

    /*@Override
    public boolean equals(Object other){
        return this.getPaid() == other.getPaid() && this.getAppreciation() == other.getAppreciation()
                && this.getIntern().getId() == other.getIntern().getId() && this.getStatus() == other.getStatus()
                && this.getDiploma().getId() == other.getDiploma().getId()
                && super.equals(other);
    }

    public boolean equalsAllLesson(FollowedDiploma other){
        List<UUID> id_this = new ArrayList<>();
        this.getAllLessons().forEach((l) -> id_this.add(l.getId()));
        List<UUID> id_other = new ArrayList<>();
        other.getAllLessons().forEach((l) -> id_other.add(l.getId()));
        return id_this.containsAll(id_other);
    }*/
}
