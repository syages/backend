package models;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@SuppressWarnings("serial")
public class PeriodAppreciation extends BaseModel {
	public static final Finder<UUID,PeriodAppreciation> find = new Finder<UUID,PeriodAppreciation>(UUID.class, PeriodAppreciation.class);

	public String appreciation;

    public InternUser intern;

    public Lesson lesson;

    public Period period;
}
