package models;

import com.aureo.oauth.common.OAuthResourceOwner;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import helpers.syages.SyagesConfig;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.NotNull;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@Table(name = "Users")
@SuppressWarnings("serial")
public class User extends BaseModel implements OAuthResourceOwner {

	public enum Usertype{
        Administrator("Administrator"),
        Intern("InternUser"),
        President("President"),
        Staff("StaffUser"),
        Teacher("TeacherUser");

        private String name = "";

        Usertype(String name){
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }

    public static Finder<UUID, User> find = new Finder<UUID, User>(UUID.class, User.class);

    @Constraints.Required
    @Constraints.Pattern(value=".+@.+")
    @NotNull
    @Column(unique = true)
    protected String email;

    @Constraints.Required
    @Constraints.MinLength(2)
    @NotNull
    protected String firstName;

    @Enumerated(EnumType.STRING)
    @Constraints.Required

    private Usertype flagType;

    @Constraints.Required
    @Constraints.MinLength(2)
    @NotNull
    protected String lastName;

    @OneToMany(mappedBy = "author", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<News> newsPosted = new ArrayList<News>();

    @OneToMany(mappedBy = "observerUser", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<NotedAbsence> observedAbsences = new ArrayList<NotedAbsence>();

    protected String password;
    @JsonIgnore
    private String passwordSalt;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "user")
    @JsonIgnore
    private Attachment picture;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    @JsonIgnore
    private List<Report> reports;

    public User(String email, String firstName, String lastName, String password, Usertype type){
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.setPassword(password);
        this.flagType = type;
    }

    protected User(){
        // keep empty
    }

    public String getEmail() {
        return this.email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Usertype getFlagType() {
        return this.flagType;
    }

    public String getLastName() {
        return this.lastName;
    }

    public List<News> getNewsPosted() {
        return this.newsPosted;
    }

    public List<NotedAbsence> getObservedAbsences() {
        return this.observedAbsences;
    }

    @JsonIgnore
    public String getPassword() {
        return this.password;
    }

    @JsonIgnore
    public String getPasswordSalt() {
        return this.passwordSalt;
    }

    public Attachment getPicture() {
        return this.picture;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFlagType(Usertype flagType) {
        this.flagType = flagType;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNewsPosted(List<News> newsPosted) {
        this.newsPosted = newsPosted;
    }

    public void setObservedAbsences(List<NotedAbsence> observedAbsences) {
        this.observedAbsences = observedAbsences;
    }

    @JsonProperty
    public void setPassword(String password) {
    	this.passwordSalt = SyagesConfig.passwordHashingMethod.gensalt();
    	this.password = SyagesConfig.passwordHashingMethod.hash(password, this.passwordSalt);
    }

    public void setPicture(Attachment picture) {
        this.picture = picture;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    public List<AccessToken> associatedTokens() {
        List<AccessToken> tokens = AccessToken.find.where().eq("owner", this).findList();
        for (AccessToken token : tokens)
        {
            if (token.isValid() == false) {
                tokens.remove(token);
                /* token.delete(); Remplacer par un acteur Akka.*/
            }
        }
        return tokens;
    }

    public static User create(String email, String firstName, String lastName, String password, Usertype type){
        User u = new User(email, firstName, lastName, password, type);
        u.save();
        return u;
    }

    public static Boolean isEmailUnique(String email){
        return find.where().eq("email", email).findList().isEmpty();
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if (this.getId() == null && !isEmailUnique(email)){
            errors.put("email", "L'email saisi est déjà lié à un compte.");
        }
        return errors;
    }

    /*public boolean equals(User other){
        return this.getEmail() == other.getEmail() && this.getFirstName() == other.getFirstName()
                && this.getFlagType() == other.getFlagType() && this.getLastName() == other.getLastName()
                && super.equals(other);
    }*/
}