package models;

import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthWorkflow;
import com.google.common.base.Joiner;
import play.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@SuppressWarnings("serial")
public class Consumer extends BaseModel implements com.aureo.oauth.common.OAuthConsumer {

	public static final Finder<UUID, Consumer> find = new Finder<UUID, Consumer>(UUID.class, Consumer.class);

	@NotNull
	private String author;
	
	@NotNull
	private String customSchemesList;
	
	private boolean grantTypeAuthCode, grantTypeClientCredentials, grantTypeImplicit, grantTypePassword;

	@NotNull
	private String host;
	
	@Column(unique = true)
	@NotNull
	private String key;

	@NotNull
	private String name;
	
	@Column(unique = true)
	@NotNull
	private String secret;
	
	public Consumer(String name, String author, String host, String key, String secret) {
		this.author = author;
		this.customSchemesList = "";
		this.host = host;
		this.key = key;
		this.name = name;
		this.secret = secret;
	}
	
	public Consumer(String name, String author, String host, String key, String secret, Set<String> urlSchemes) {
		this.author = author;
		this.setCustomSchemesList(urlSchemes);
		this.host = host;
		this.key = key;
		this.name = name;
		this.secret = secret;
	}

	public void addWorkflow(OAuthWorkflow workflow)
	{
		switch(workflow)
		{
		case CLIENT_CREDENTIALS:
			this.grantTypeClientCredentials = true;
			break;
		case IMPLICIT:
		case NATIVE:
			this.grantTypeImplicit = true;
			break;
		case PASSWORD:
			this.grantTypePassword = true;
			break;
		case SERVER_SIDE:
			this.grantTypeAuthCode = true;
			break;
		default:
			Logger.warn(this.getClass().getCanonicalName() + ".addGrantType() does not handle all grant types");
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof Consumer) == false) return false;
		Consumer c = (Consumer)obj;
		return this.getId() != null && this.getId().equals(c.getId());
	}

	@Override
	public boolean equals(OAuthConsumer consumer) {
		if ((consumer instanceof Consumer) == false) return false;
		Consumer c = (Consumer)consumer;
		return this.getId() != null && this.getId().equals(c.getId());
	}

	@Override
	public String getAuthor() {
		return this.author;
	}
	
	@Override
	public Set<String> getCustomSchemesList() {
		return new HashSet<String>(Arrays.asList(this.customSchemesList.split(";")));
	};
	
	@Override
	public EnumSet<OAuthWorkflow> getWorkflows() {
		EnumSet<OAuthWorkflow> set = EnumSet.noneOf(OAuthWorkflow.class);
		if (this.grantTypeAuthCode) set.add(OAuthWorkflow.SERVER_SIDE);
		if (this.grantTypeClientCredentials) set.add(OAuthWorkflow.CLIENT_CREDENTIALS);
		if (this.grantTypeImplicit) set.add(OAuthWorkflow.IMPLICIT);
		if (this.grantTypeImplicit) set.add(OAuthWorkflow.NATIVE);
		if (this.grantTypePassword) set.add(OAuthWorkflow.PASSWORD);
		return set;
	}

	@Override
	public String getHost() {
		return this.host;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getSecret() {
		return this.secret;
	}

	public void removeWorkflow(OAuthWorkflow workflow)
	{
		switch(workflow)
		{
		case CLIENT_CREDENTIALS:
			this.grantTypeClientCredentials = false;
			break;
		case IMPLICIT:
		case NATIVE:
			this.grantTypeImplicit = false;
			break;
		case PASSWORD:
			this.grantTypePassword = false;
			break;
		case SERVER_SIDE:
			this.grantTypeAuthCode = false;
			break;
		default:
			Logger.warn(this.getClass().getCanonicalName() + ".addGrantType() does not handle all grant types");
		}
	}
	
	public void setCustomSchemesList(Set<String> extra) {
	    this.customSchemesList = Joiner.on(";").join(extra);
	}
	
}
