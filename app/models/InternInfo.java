package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.ConstraintViolation;
import java.sql.Date;
import java.util.Map;
import java.util.Set;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class InternInfo extends BaseModel {


    private String job;

    private String lastSchool;

    @Temporal(TemporalType.DATE)
    private Date lastSchoolYear;

    private String professionalProject;
    
    private Boolean worked;

    public InternInfo(){
        // keep empty
    }

    public Map<String,String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(!this.job.isEmpty() && this.worked){
            errors.put("error", "Si un emploi est saisi, worked doit être à Vrai.");
        }

        return errors;
    }


    public String getJob() {
        return job;
    }

    public String getLastSchool() {
        return lastSchool;
    }

    public Long getLastSchoolYear() {
        return lastSchoolYear.getTime();
    }

    public String getProfessionalProject() {
        return professionalProject;
    }

    public Boolean getWorked() {
        return worked;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setLastSchool(String lastSchool) {
        this.lastSchool = lastSchool;
    }

    public void setLastSchoolYear(Date lastSchoolYear) {
        this.lastSchoolYear = lastSchoolYear;
    }

    public void setProfessionalProject(String professionalProject) {
        this.professionalProject = professionalProject;
    }

    public void setWorked(Boolean worked) {
        this.worked = worked;
    }

    /*public boolean equals(InternInfo other){
        return this.getWorked() == other.getWorked() && this.getJob() == other.getJob()
                && this.getLastSchool() == other.getLastSchool() && this.getProfessionalProject() == other.getProfessionalProject()
                && this.getLastSchoolYear() == other.getLastSchoolYear() && super.equals(other);
    }*/
}
