package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.Logger;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.util.*;
import java.sql.Date;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Period extends BaseModel {

    /**
	 * A DiplomaPeriod is a set of day during which there are evaluations.
     * Each Period has one/one final evaluation/DiplomaCourse
     * its parameters are:
     * - a Diploma
     * - a startDate
     * - an endDate
	 */
    public static final Finder<UUID, Period> find = new Finder<UUID, Period>(UUID.class, Period.class);


    @ManyToOne
	private Diploma diploma;

    @Temporal(TemporalType.DATE)
    @Constraints.Required
    private Date endDate;

    @OneToMany(mappedBy = "period", cascade = CascadeType.ALL)
    private List<Evaluation> evaluationList;


    @Basic
    @Constraints.Min(value=1)
    @Constraints.Max(value=3)
    private Integer ordre;


    @Temporal(TemporalType.DATE)
    @Constraints.Required
    private Date startDate;

    public Period(){
        // keep empty
    }

    @JsonIgnore
    public Diploma getDiploma() {
        return diploma;
    }

    public Date getEndDate() {
        return endDate;
    }

    @JsonIgnore
    public List<Evaluation> getEvaluationList() {
        return evaluationList;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public Date getStartDate() {
        return startDate;
    }

    @JsonProperty
    public void setDiploma(Diploma diploma) {
        this.diploma = diploma;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setEvaluationList(List<Evaluation> evaluationList) {
        this.evaluationList = evaluationList;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Map<String,String> validate(){
        Logger.info("Period validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(this.getEndDate().getTime() < this.getStartDate().getTime()){
            errors.put("start_end", "endDate can't be before startDate.");
        }

        return errors;
    }

    /*public boolean equals(Period other){
        return this.getDiploma().getId() == other.getDiploma().getId() && this.getStartDate() == other.getStartDate()
                && this.getEndDate() == other.getEndDate() && this.getOrdre() == other.getOrdre()
                && super.equals(other);
    }*/

}
