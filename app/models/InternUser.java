package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.constraints.Past;
import java.sql.Date;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "user_id"))
@SuppressWarnings("serial")
public final class InternUser extends BaseModel{

	public static Finder<UUID, InternUser> find = new Finder<UUID, InternUser>(UUID.class, InternUser.class);


    public static InternUser create(String email, String firstName, String lastName, Date birth)
    {
        InternUser user = new InternUser(email, firstName, lastName, "", User.Usertype.Intern, birth);
        user.save();
        return user;
    }

    public static InternUser findByUserId(UUID id){
        return find.where().eq("user_id", id).findUnique();
    }

    // Date of birth
    @Past
    @Constraints.Required
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @OneToMany(mappedBy = "intern", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<DeclaredAbsence> declaredAbsences;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "intern")
    private List<FollowedDiploma> followedDiplomas = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="infos_id", unique= true, nullable = true)
    private InternInfo infos;

    @OneToMany(mappedBy = "intern", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Mark> marks;

    @OneToMany(mappedBy = "intern", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<NotedAbsence> notedAbsences;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", unique = true)
    @Constraints.Required
    private User user;

    public InternUser(){
        // keep empty
    }

    public InternUser(String email, String firstName, String lastName, String password,User.Usertype type, Date birth){
        this.user = User.create(email, firstName, lastName, password, type);
        this.dateOfBirth = birth;
        this.declaredAbsences = new ArrayList<DeclaredAbsence>();
        this.notedAbsences = new ArrayList<NotedAbsence>();
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        errors.putAll(this.user.validate());
        for(FollowedDiploma fd: this.followedDiplomas){
            errors.putAll(fd.validate());
        }

        if(this.infos != null){
            errors.putAll(this.infos.validate());
        }

        return errors;
    }


    public void addFollowedDiploma(FollowedDiploma fd){
        this.followedDiplomas.add(fd);
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonIgnore
    public List<DeclaredAbsence> getDeclaredAbsences() {
        return declaredAbsences;
    }

    public List<FollowedDiploma> getFollowedDiplomas() {
        return followedDiplomas;
    }

    public InternInfo getInfos() {
        return infos;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    @JsonIgnore
    public List<NotedAbsence> getNotedAbsences() {
        return notedAbsences;
    }

    public User getUser(){
        return this.user;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDeclaredAbsences(List<DeclaredAbsence> declaredAbsences) {
        this.declaredAbsences = declaredAbsences;
    }

    public void setFollowedDiplomas(List<FollowedDiploma> followedDiplomas) {
        this.followedDiplomas = followedDiplomas;
    }

    public void setInfos(InternInfo infos) {
        this.infos = infos;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public void setNotedAbsences(List<NotedAbsence> notedAbsences) {
        this.notedAbsences = notedAbsences;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addMark(Mark mark){
        this.marks.add(mark);
    }

    // queries

    @Override
    public void update(){
        this.getUser().update();
        super.update();
    }

    @PrePersist
    @Override
    public void setIDonPersist(){
        this.setId(this.getUser().getId());
    }

    /*public boolean equals(InternUser other){
        return this.getUser().equals(other.getUser()) && this.getInfos() == other.getInfos()
                && this.getDateOfBirth() == other.getDateOfBirth() && super.equals(other);
    }*/

}
