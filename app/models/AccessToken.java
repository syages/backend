package models;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthWorkflow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import helpers.syages.SyagesConfig;
import play.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Entity
@SuppressWarnings("serial")
public class AccessToken extends BaseModel implements OAuthAccessToken {
	
	public static final Finder<UUID, AccessToken> find = new Finder<UUID, AccessToken>(UUID.class, AccessToken.class);
	
	@NotNull
	@Column(unique = true)
	private String code;
	
	@NotNull
	@ManyToOne
	private Consumer consumer;

	private Timestamp expiration;
	
	private int expirationRenew;
	
	@NotNull
	@ManyToOne
	private User owner;
	
	public AccessToken(String code, Consumer consumer, User owner, OAuthWorkflow type)
	{
		this.code = code;
		this.consumer = consumer;
		this.owner = owner;
		
		switch (type)
		{
		case SERVER_SIDE:
			setExpiration(SyagesConfig.accessTokenExpirationFromServerSide);
			break;
		case CLIENT_CREDENTIALS:
			setExpiration(SyagesConfig.accessTokenExpirationFromClientCred);
			break;
		case PASSWORD:
			setExpiration(SyagesConfig.accessTokenExpirationFromPassword);
			break;
		case IMPLICIT:
			setExpiration(SyagesConfig.accessTokenExpirationFromImplicit);
			break;
		case NATIVE:
			setExpiration(SyagesConfig.accessTokenExpirationFromImplicit);
			break;
		default:
			Logger.warn(this.getClass().getCanonicalName() + ".ctor(code, consumer, owner, type) does not handle all OAuth grant types");
		}
	}
	
	public String getCode()
	{
		return this.stringRepresentation();
	}

	public Consumer getConsumer()
	{
		return this.consumer;
	}
	
	@JsonIgnore
	public Timestamp getExpirationTimestamp()
	{
		return this.expiration;
	}
	
	@Override
	public Date getExpirationDate()
	{
		if (this.expiration == null)
			return null;
		return new Date(this.expiration.toInstant().getNano()/1000);
	}
	
	public User getOwner()
	{
		return this.owner;
	}
	
	@Override
	public boolean isValid()
	{
		if (this.expiration == null) return true;
		
		boolean valid = this.expiration.after(Timestamp.from(Instant.now()));
		if (valid && this.expirationRenew > 0)
		{
			this.expiration = Timestamp.from(Instant.now().plus(this.expirationRenew, ChronoUnit.MINUTES));
			this.save();
		}
		return valid;
	}
	
	@Override
	public void refresh() {
		this.setExpiration(this.expirationRenew);
	}
	
	@Override
	public String stringRepresentation() {
		return code;
	}
	
	private void setExpiration(int renew)
	{
		if (renew == 0)
		{
			this.expiration = null;
			this.expirationRenew = 0;
		} else {
			this.expiration = Timestamp.from(Instant.now().plus(renew, ChronoUnit.MINUTES));
			this.expirationRenew = renew;
		}
	}
	
}
