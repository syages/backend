package models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Course extends BaseModel {
    /**
	 * A Course is set by an AdministrativeUser
     * its parameters are:
     * - a teacher (models.TeacherUser)
     * - a domain (models.Domain)
     * - a name
	 */
	public static Model.Finder<UUID, Course> find = new Finder<UUID, Course>(UUID.class, Course.class);

    @ManyToOne
    private Domain domain;

    @OneToMany (mappedBy = "course", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Lesson> lessons;

    @Constraints.Required
    private String name;

    public Course(){
        // keep empty
    }

    public Course(Domain domain, String name) {
        this.domain = domain;
        this.name = name;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        return errors;
    }

    public Domain getDomain() {
        return domain;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public String getName() {
        return name;
    }

    public void setDomain(Domain domain){
        this.domain = domain;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Course findByName(String name){
        return find.where().eq("name", name).findUnique();
    }

    /*public boolean equals(Course other){
        return this.getDomain().getId() == other.getDomain().getId() && this.getName() == other.getName()
                && super.equals(other);
    }*/

}
