package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;


@Entity
@SuppressWarnings("serial")
public class Mark extends BaseModel {
    public enum EvalInternStatus{
        Absent ("Abs"),
        Excuse ("Exc"),
        Present ("Présent");

        private String name;

        EvalInternStatus(String s){
            this.name = s;
        }

        public String getName(){
            return this.name;
        }
    }

    /**
     * An EvaluationMark is set by a teacher for a couple Evaluation,InternUser.
     * its parameters are:
     * - an InternUser
     * - an Evaluation
     * - a mark
     * - an enum Type
     */

    public static final Finder<UUID, Mark> find = new Finder<UUID, Mark>(UUID.class, Mark.class);

    @ManyToOne
    private Evaluation evaluation;

    @ManyToOne
    @Constraints.Required
    private InternUser intern;

    private Float mark;

    @Enumerated(EnumType.STRING)
    @Constraints.Required
    private EvalInternStatus status;

    public Mark(){
        // keep empty
    }

    @JsonIgnore
    public Evaluation getEvaluation() {
        return evaluation;
    }
    @JsonProperty
    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }


    @JsonIgnore
    public InternUser getIntern() {
        return intern;
    }

    public Float getMark() {
        return mark;
    }

    public EvalInternStatus getStatus() {
        return status;
    }

    @JsonProperty
    public void setIntern(InternUser intern) {
        this.intern = intern;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    public void setStatus(EvalInternStatus status) {
        this.status = status;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(status == EvalInternStatus.Present){
            if(mark < 0 || mark > evaluation.getGradingScale()){
                errors.put("note", "la note doit etre comprise dans l'échelle de notation de l'évaluation.");
            }
        }

        return errors;
    }

    /*public boolean equals(Mark other){
        return this.getEvaluation().getId() == other.getEvaluation().getId() && this.getIntern().getId() == other.getIntern().getId()
                && this.getMark() == other.getMark() && this.getStatus() == other.getStatus() && super.equals(other);
    }*/
}
