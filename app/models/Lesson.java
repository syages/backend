package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Lesson extends BaseModel {

	public static final Finder<UUID,Lesson> find = new Finder<UUID,Lesson>(UUID.class, Lesson.class);

	@Constraints.Required
    private Float coefficient;

    @ManyToOne
    private Course course;

    @ManyToOne
    private Diploma diploma;

    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    private List<Evaluation> evaluations;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<FollowedDiploma> followedDiplomas = new ArrayList<>();

    @Constraints.Required
    private Boolean optional;

    @Constraints.Required
    private Float passMark;

    @ManyToOne
    private TeacherUser teacher;

    @OneToMany(mappedBy = "lesson",cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Lecture> lectures;


    public Lesson(){
        // keep empty
    }

    public Lesson(TeacherUser teacher, Course course, Float coefficient, Boolean optional, Float validationMoy){
        this.teacher = teacher;
        this.course = course;
        this.coefficient = coefficient;
        this.optional = optional;
        this.passMark = validationMoy;
    }

    public Float getCoefficient() {
        return coefficient;
    }

    public Course getCourse() {
        return course;
    }

    @JsonIgnore
    public Diploma getDiploma() {
        return diploma;
    }

    @JsonIgnore
    public List<Evaluation> getEvaluations() {
        return evaluations;
    }


    public Boolean getOptional() {
        return optional;
    }

    public Float getPassMark() {
        return passMark;
    }

    @JsonIgnore
    public TeacherUser getTeacher() {
        return teacher;
    }

    public void setCoefficient(Float coefficient) {
        this.coefficient = coefficient;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @JsonProperty
    public void setDiploma(Diploma diploma) {
        this.diploma = diploma;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }


    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

    public void setPassMark(Float passMark) {
        this.passMark = passMark;
    }

    @JsonProperty
    public void setTeacher(TeacherUser teacher) {
        this.teacher = teacher;
    }

    @JsonIgnore
    public List<FollowedDiploma> getFollowedDiplomas() {
        return followedDiplomas;
    }

    public void setFollowedDiplomas(List<FollowedDiploma> followedDiplomas) {
        this.followedDiplomas = followedDiplomas;
    }

    public List<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(List<Lecture> lectures) {
        this.lectures = lectures;
    }

    public Map<String,String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);


        return errors;
    }

    /*public boolean equals(Lesson other){
        return this.getOptional() == other.getOptional() && this.getCoefficient() == other.getCoefficient()
                && this.getCourse().getId() == other.getCourse().getId() && this.getDiploma().getId() == other.getDiploma().getId()
                && this.getTeacher().getId() == other.getTeacher().getId() && super.equals(other);
    }*/
}
