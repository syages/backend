package models;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Attachments")
@SuppressWarnings("serial")
public class Attachment extends BaseModel {

    public static final Finder<UUID, Attachment> find = new Finder<UUID, Attachment>(UUID.class, Attachment.class);


    @Constraints.Required
    @Lob
    private byte[] content;

    @Constraints.Required
    private String contentType;

    @OneToOne
    @JoinColumn(name="user_id", unique=true)
    private User user;

    public Attachment(byte[] attachmentItem, String type, User user) {
        this.content = attachmentItem;
        this.contentType = type;
        this.user = user;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
