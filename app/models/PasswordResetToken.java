package models;

import helpers.crypto.GetRandom;
import helpers.mail.MailFactory;
import helpers.syages.SyagesConfig;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.UUID;

@Entity
@SuppressWarnings("serial")
public class PasswordResetToken extends BaseModel {

	public static Finder<UUID, PasswordResetToken> find = new Finder<UUID, PasswordResetToken>(UUID.class, PasswordResetToken.class);

    @Column(unique = true)
    private String key;

    private Long timestamp;

    @ManyToOne
    public User user;

    public PasswordResetToken(User user) {
        this.key = GetRandom.hexString(16);
        this.timestamp = new Date().getTime();
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isValid()
    {
        Date now = new Date();
        long delay = 12 * 60 * 6000; // 12hours
        if (now.getTime() - (this.timestamp + delay) > 0) {
            delete();
            return false;
        }
        return true;
    }

    public void sendMail()
    {
        try {
            HtmlEmail email = MailFactory.htmlEmail();
            String name = this.user.getFirstName() + " " + this.user.getLastName();
            String url = "https://" + SyagesConfig.domain + "#iforgot/" + this.key;

            email.setFrom(SyagesConfig.smtpFrom);
            email.setSubject("Syages - Réinitialisation du mot de passe");
            email.setHtmlMsg(views.html.mail.passwordreset.render(name, url).body());
            email.addTo(this.user.getEmail());
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

    public static PasswordResetToken getByKey(String key)
    {
        return find.where().eq("key", key).findUnique();
    }
}