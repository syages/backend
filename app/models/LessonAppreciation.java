package models;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
@SuppressWarnings("serial")
public class LessonAppreciation extends BaseModel {

    /**
	 * A DiplomaCourseAppreciation is an appreciation set to a specific course for an Intern.
     * It's set by an TeacherUser.
     * its parameters are:
     * - an InternUser
     * - a DiplomaCourse
     * - an appreciation (String)
	 */
	public static final Finder<UUID,LessonAppreciation> find = new Finder<UUID,LessonAppreciation>(UUID.class, LessonAppreciation.class);

	public String appreciation;

    public InternUser intern;

    public Lesson lesson;
}