package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public final class StaffUser extends Agent {

    public static Finder<UUID, StaffUser> find = new Finder<UUID, StaffUser>(UUID.class, StaffUser.class);

    public StaffUser(){
        // keep empty
    }

    public StaffUser(User user, String number, String office){
        super(user, number, office);
    }

    public static StaffUser create(String email, String firstName,
                                   String lastName, String password,
                                   String number, String office){
        User user = User.create(email, firstName, lastName, password, User.Usertype.Staff);
        StaffUser admin = new StaffUser(user, number, office);
        admin.save();
        return admin;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        errors.putAll(super.validate());


        return errors;
    }

    public static StaffUser findByUserId(UUID id){
        return find.where().eq("user_id", id).findUnique();
    }

}
