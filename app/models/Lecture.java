package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.ConstraintViolation;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
public class Lecture extends BaseModel{

    public static final Finder<UUID, Lecture> find = new Finder<UUID, Lecture>(UUID.class, Lecture.class);

    @Constraints.Required
    private Timestamp date;

    @ManyToOne
    private Lesson lesson;

    @Constraints.Required
    /*
    duration of a lecture in minutes
     */
    private Long duration;

    @OneToMany(mappedBy = "lecture",cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NotedAbsence> notedAbsences;


    private String classroom;

    public Lecture() {
    }


    public Map<String,String> validate() {
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        for(NotedAbsence na : this.notedAbsences){
            errors.putAll(na.validate());
        }

        Lesson l = Lesson.find.byId(this.lesson.getId());
        Period first = l.getDiploma().getPeriodNth(1);
        Period last = l.getDiploma().getPeriodNth(3);

        if(!(first.getStartDate().getTime() <= this.date.getTime() && last.getEndDate().getTime() >= this.date.getTime())){
            errors.put("error", "la date de la Lecture ne se situe pas entre les date de la formation.");
        }

        List<InternUser> interns = new ArrayList<>();
        for(NotedAbsence na: this.notedAbsences){
            boolean in = false;
            for(InternUser intern: interns){
                if(intern.getId() == na.getIntern().getId()){
                    in = true;
                    break;
                }
            }
            if(in){
                errors.put("error", "Il y a déjà une absence pour l'intern" + na.getIntern().getUser().getId() + "dans cette Lecture.");
            }
        }

        return errors;
    }

    public List<NotedAbsence> getNotedAbsences() {
        return notedAbsences;
    }

    public void setNotedAbsences(List<NotedAbsence> notedAbsences) {
        this.notedAbsences = notedAbsences;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /*public boolean equals(Lecture other){
        return this.getClassroom() == other.getClassroom() && this.getDate() == other.getDate()
                && this.getLesson().getId() == other.getLesson().getId() && this.getNotedAbsences() == other.getNotedAbsences()
                && super.equals(other);
    }*/
}
