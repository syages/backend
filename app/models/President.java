package models;


import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class President extends Agent {

    public static final Finder<UUID, President> find = new Finder<UUID, President>(UUID.class, President.class);

    // jour de la semaine (1 = Lundi, 2 = Mardi, ...)
    private Integer day;
    // heure du jour
    private String hour;

    // récurrence en jours
    private Integer recurrence;

    public President(){
        // keep empty
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Integer recurrence) {
        this.recurrence = recurrence;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public static President findByUserId(UUID id){
        return find.where().eq("user_id", id).findUnique();
    }

    public Map<String,String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        errors.putAll(super.validate());

        return errors;
    }

    /*public boolean equals(President other){
        return this.getDay() == other.getDay() && this.getHour() == other.getHour()
                && this.getRecurrence() == other.getRecurrence() && super.equals(other);
    }*/

}
