package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.sql.Date;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;


@Entity
@SuppressWarnings("serial")
public class Evaluation extends BaseModel {
    /**
     * An Evaluation is of three type, is set by a teacher and is linked to a DiplomaCourse and a Period
     * its parameters are:
     * - a name
     * - a type
     * - a coefficient
     * - a date (timestamp)
     * - a DiplomaCourse
     * - a Period
     * - a list of all the marks
     */
    public enum EvaluationType{
        Continue ("Contrôle continu"),
        Final ("Partiel");

        private String name;

        EvaluationType (String s){
            this.name = s;
        }

        public String getName(){
            return this.name;
        }
    }

    public static final Finder<UUID, Evaluation> find = new Finder<UUID, Evaluation>(UUID.class, Evaluation.class);


    @Constraints.Required
    private Float coefficient;

    @Temporal(TemporalType.DATE)
    @Constraints.Required
    private Date date;

    @Constraints.Required
    private Long gradingScale;

    @ManyToOne
    @Constraints.Required
    private Lesson lesson;

    @OneToMany(mappedBy = "evaluation", cascade = CascadeType.ALL)
    private List<Mark> marks = new ArrayList<>();

    @Constraints.Required
    private String name;

    @ManyToOne
    private Period period;

    @Enumerated(EnumType.STRING)
    @Constraints.Required
    private EvaluationType type;

    @Constraints.Required
    private Boolean visible;


    public Evaluation(){
        // keep empty
    }


    public Float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Float coefficient) {
        this.coefficient = coefficient;
    }

    public Long getDate() {
        return date.getTime();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getGradingScale() {
        return gradingScale;
    }

    public void setGradingScale(Long gradingScale) {
        this.gradingScale = gradingScale;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public EvaluationType getType() {
        return type;
    }

    public void setType(EvaluationType type) {
        this.type = type;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(this.gradingScale < 0){
            errors.put("echelle de notation", "l'échelle de notation ne peut pas être négative.");
        }

        if(this.gradingScale > 100){
            errors.put("echelle de notation", "l'échelle de notation est limité à 100.");
        }
        if(this.type.equals(EvaluationType.Final)) {
            for (Evaluation e : this.period.getEvaluationList()) {
                if (!e.equals(this) && e.getLesson().equals(this.lesson) && e.getType().equals(EvaluationType.Final)){
                    errors.put("error", "Il y a déja un partiel pour cette matière dans ce trimestre.");
                }
            }
        }
        return errors;
    }

    /*public boolean equals(Evaluation other){
        return this.getVisible() == other.getVisible() && this.getCoefficient() == other.getCoefficient()
                && this.getDate() == other.getDate() && this.getGradingScale() == other.getGradingScale()
                && this.getLesson().getId() == other.getLesson().getId() && this.getName() == other.getName()
                && this.getType() == other.getType() && super.equals(other);
    }*/
}
