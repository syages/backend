package models;

import play.Logger;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;


@MappedSuperclass
@SuppressWarnings("serial")
public abstract class Agent extends BaseModel {

    public static Finder<UUID, Agent> find = new Finder<UUID, Agent>(UUID.class, Agent.class);

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id", unique = true)
    private User user;

	protected String number;

    protected String office;

    public Agent(){
        // keep empty
    }

    public Agent(User user, String number, String office){
        this.user = user;
        this.office = office;
        this.number = number;
    }

    public Map<String,String> validate(){
        Logger.info("Agent validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String,String> errors = constraintsViolationToMap(constraintViolations);

        if(this.number != null && this.number.length() != 10){
            errors.put("number", "le numéro doit être composé de 10 chiffres.");
        }

        errors.putAll(this.user.validate());

        return errors;
    }

    public String getNumber(){
        return number;
    }

    public String getOffice(){
        return office;
    }

    public User getUser(){
        return this.user;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void update(){
        this.getUser().update();
        super.update();
    }

    @PrePersist
    @Override
    public void setIDonPersist(){
        this.setId(this.getUser().getId());
    }

    /*public boolean equals(Agent other){
        return this.getNumber() == other.getNumber() && this.getOffice() == other.getOffice()
                && super.equals(other);
    }*/
}
