package models;

import play.db.ebean.Model;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Version;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.UUID;

@MappedSuperclass
@SuppressWarnings("serial")
public abstract class BaseModel extends Model {

    public static final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    public static final Validator validator = factory.getValidator();

    @Id
    private UUID id = null;

    @Version
    private Long version;

    public BaseModel(){

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @PrePersist
    public void setIDonPersist(){
        this.id = UUID.randomUUID();
    }

    /*@Override
    public boolean equals(Object other){
        return this.id == other.getId() && this.version == other.getVersion();
    }
*/
}
