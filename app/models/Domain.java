package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Domain extends BaseModel{

	public static Finder<UUID, Domain> find = new Finder<UUID, Domain>(UUID.class, Domain.class);

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)
    private List<Course> courses;

    @Constraints.Required
    private String name;

    @ManyToMany(mappedBy = "domains", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<TeacherUser> teachers;

    public Domain(){
        // keep empty
    }

    private Domain(String name){
        this.name = name;
    }

    @JsonIgnore
    public List<Course> getCourses() {
        return courses;
    }

    // GET/SET
    public String getName(){
        return this.name;
    }

    @JsonIgnore
    public List<TeacherUser> getTeachers() {
        return teachers;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTeachers(List<TeacherUser> teachers) {
        this.teachers = teachers;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        return errors;
    }

    /*public boolean equals(Domain other){
        return this.getName() == other.getName() && super.equals(other);
    }*/
}
