package models;

import play.Logger;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;

import static helpers.util.Divers.constraintsViolationToMap;

@MappedSuperclass
@SuppressWarnings("serial")
public abstract class Absence extends BaseModel {

    /**
	 * An absence is a record of absence for a student.
     * models.Absence is a abstract class and is extended by DeclaredAbsence and NotedAbsence
     * it's a MappedSuperclass and isn't represented in the database.
     * its parameters are:
     * - an InternUser
	 */

    public enum TimeOfDay{
        Afternoon("Après-midi"),
        Morning("Matin");

        private String name="";

        TimeOfDay(String name){
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }
    
    @ManyToOne
    @Constraints.Required
    private InternUser intern;

    public Absence(){
        // keep empty
    }

    protected Absence(InternUser intern){
        this.intern = intern;
    }

    public InternUser getIntern(){
        return intern;
    }

    public void setIntern(InternUser intern) {
        this.intern = intern;
    }

    public Map<String,String> validate(){
        Logger.info("Absence validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        return constraintsViolationToMap(constraintViolations);
    }

    /*public boolean equals(Absence other){
        return this.intern.getId() == other.intern.getId() && super.equals(other);
    }*/
}
