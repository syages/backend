package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.Logger;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.time.Year;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Diploma extends BaseModel {
    public enum DiplomaStatus{
        Active ("En cours"),
        Inactive ("Finis");

        private String name="";

        DiplomaStatus(String s){
            this.name = s;
        }

        public String getName(){
            return this.name;
        }
    }

    /**
	 * A Diploma is for example DAEU-A.
     * It's followed by intern, and has an in-charge teacher.
     * A Diploma is composed of 3 period. This period are describe in models.DiplomaPeriod
     * Its parameters are:
     * - a name
     * - a supervisor (TeacherUser)
     * - a list of DiplomaCourses
     * - a list of InternUser
     * - a set of period
     * - a minimum mark to obtain it
     * - a final Appreciation set by the director
     * - an enum of diplomaStatus
     * - validated/not validated
	 */
	public static final Finder<UUID,Diploma> find = new Finder<UUID,Diploma>(UUID.class, Diploma.class);


    @OneToMany(mappedBy = "diploma", cascade = CascadeType.ALL)
    @Constraints.Required
    private List<Lesson> lessons;

    @Constraints.Required(message="Il est nécessaire de saisir un nom pour la formation.")
	private String name;

    @Constraints.Required
    private Long passMark;

    @OneToMany(mappedBy = "diploma", cascade = CascadeType.ALL)
    private List<FollowedDiploma> followedDiplomas;

    @OneToMany(mappedBy = "diploma", cascade = CascadeType.ALL)
    @Constraints.Required
    private List<Period> periods = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private DiplomaStatus status = DiplomaStatus.Active;

    @ManyToOne
    @Constraints.Required(message ="Il est nécessaire de choisir un enseignant responsable.")
    private TeacherUser supervisor;

    @Transient
    @JsonIgnore
    private Year startYear;

    @Transient
    @JsonIgnore
    private Year endYear;

    public Diploma(){
        // keep empty
    }

    // GETTER / SETTER

    public List<Lesson> getLessons() {
        return lessons;
    }

    public String getName() {
        return name;
    }

    public Long getPassMark() {
        return passMark;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public DiplomaStatus getStatus() {
        return status;
    }

    public TeacherUser getSupervisor() {
        return supervisor;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassMark(Long passMark) {
        this.passMark = passMark;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }

    public void setStatus(DiplomaStatus status) {
        this.status = status;
    }

    public void setSupervisor(TeacherUser supervisor) {
        this.supervisor = supervisor;
    }

    @JsonIgnore
    public List<FollowedDiploma> getFollowedDiplomas() {
        return followedDiplomas;
    }

    public void setFollowedDiplomas(List<FollowedDiploma> followedDiplomas) {
        this.followedDiplomas = followedDiplomas;
    }


    public Period getPeriodNth(Integer n){
        for(Period p: this.periods){
            if(p.getOrdre() == n){
                return p;
                /* doc */
            }
        }
        return null;
    }

    public Integer getPeriodByDate(Date date){
        for(Period p: this.periods){
            if(p.getStartDate().before(date) && p.getEndDate().after(date)){
                return p.getOrdre();
            }
        }
        return -1;
    }

    public Year getStartYear() {
        Period first = null;
        for(Period p: this.periods){
            if(first == null || p.getOrdre()< first.getOrdre()){
                first = p;
            }
        }
        this.endYear = Year.of(first.getStartDate().toLocalDate().getYear());
        return this.endYear;
    }

    public Year getEndYear() {
        Period last = null;
        for(Period p: this.periods){
            if(last == null || p.getOrdre() > last.getOrdre()){
                last = p;
            }
        }
        this.endYear = Year.of(last.getEndDate().toLocalDate().getYear());
        return this.endYear;
    }

    public Map<String, String> validate(){
        Logger.info("User validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        // validate 3 periods
        if(periods.size()>3 || periods.size()<3){
            errors.put("period","min = 3 and max = 3.");
        }

        // period.validate()
        for(Period p: periods){
            errors.putAll(p.validate());
        }

        // ordre period coherent
        // end period 1 before start period 2
        Period p1 = new Period();
        Period p2 = new Period();
        Period p3 = new Period();

        for(Period p: periods){
            if(p.getOrdre() == 1){
                p1 = p;
            }
            else if(p.getOrdre() == 2){
                p2 = p;
            }
            else{
                p3 = p;
            }
        }

        // p1 est bien avant p2
        if(p1.getEndDate().getTime() > p2.getStartDate().getTime() || p2.getEndDate().getTime() >  p3.getStartDate().getTime()){
            errors.put("periods order", "periods must be in chronological order.");
        }

        for(Lesson l: this.lessons){
            errors.putAll(l.validate());
        }

        return errors;
    }

    /*public boolean equals(Diploma other){
        return this.getEndYear() == other.getEndYear() && this.getStartYear() == other.getStartYear()
                && this.getLessons() == other.getLessons() && this.getName() == other.getName()
                && this.getPassMark() == other.getPassMark() && this.getStatus() == other.getStatus()
                && this.getPeriods() == other.getPeriods() && super.equals(other);
    }*/
}
