package models;

import play.Logger;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.sql.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import play.db.ebean.Model;

import static helpers.util.Divers.constraintsViolationToMap;
import static models.Absence.TimeOfDay;


@Entity
@SuppressWarnings({ "serial", "unused" })
public class DeclaredAbsence extends Absence {
	
    public enum Reason {
        Death ("Décès"),
        DrivingTestNotification ("Convocation permis de conduire"),
        MedicalCertificate ("Certificat médical"),
        Other ("Autre"),
        PublicTransportProblem ("Problème de transport en commun"),
        ReligiousHoliday ("Fête religieuse"),
        Summons ("Convocation administrative"),
        TheoryDrivingTestNotification ("Convocation code de la route");

        private String name="";

        Reason(String name){
            this.name = name;
        }

        public String toString(){
            return name;
        }

    }

	public enum AbsenceStatus{
        Accepted("Ok"),
        Refused ("Refus"),
        ToProcess ("A traiter");

        private String name="";

        AbsenceStatus(String name){
            this.name = name;
        }

        public String toString(){
            return name;
        }
    }

    /**
	 * A DeclaredAbsence is an Absence declared by an Intern
     * it's parameters are:
     * - a startDate
     * - a timeOfDayStart
     * - an endDate
     * - a timeOfDayEnd
     * - a reason
     * - a commentary (for the Reason.Other)
     * - a status
	 */
	public static final Finder<UUID, DeclaredAbsence> find = new Finder<UUID, DeclaredAbsence>(UUID.class, DeclaredAbsence.class);
    private String commentary;

    @Constraints.Required
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Constraints.Required
    @Enumerated(EnumType.STRING)
    private Reason reason;

    @Constraints.Required
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Enumerated(EnumType.STRING)
    private AbsenceStatus status = AbsenceStatus.ToProcess;

    @Constraints.Required
    @Enumerated(EnumType.STRING)
    private TimeOfDay timeOfDayEnd;

    @Constraints.Required
    @Enumerated(EnumType.STRING)
    private TimeOfDay timeOfDayStart;


    public DeclaredAbsence(){
        // keep empty
    }

    private DeclaredAbsence(InternUser intern, Date startDate, Date endDate, TimeOfDay start,
                            TimeOfDay end, Reason reason, String commentary){
        super(intern);
        this.startDate = startDate;
        this.endDate = endDate;
        this.timeOfDayStart = start;
        this.timeOfDayEnd = end;
        this.reason = reason;
        this.commentary = commentary;
        this.status = AbsenceStatus.ToProcess;
    }

    public String getCommentary() {
        return commentary;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Reason getReason() {
        return reason;
    }

    public Date getStartDate() {
        return startDate;
    }

    public AbsenceStatus getStatus() {
        return status;
    }

    public TimeOfDay getTimeOfDayEnd() {
        return timeOfDayEnd;
    }

    public TimeOfDay getTimeOfDayStart() {
        return timeOfDayStart;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStatus(AbsenceStatus status) {
        this.status = status;
    }

    public void setTimeOfDayEnd(TimeOfDay timeOfDayEnd) {
        this.timeOfDayEnd = timeOfDayEnd;
    }

    public void setTimeOfDayStart(TimeOfDay timeOfDayStart) {
        this.timeOfDayStart = timeOfDayStart;
    }

    public Map<String,String> validate(){
        Logger.info("DeclaredAbsence validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);
        errors.putAll(super.validate());
        return errors;
    }

    /*public boolean equals(DeclaredAbsence other){
        return this.getCommentary() == other.getCommentary() && this.getEndDate() == other.getEndDate()
                && this.getReason() == other.getReason() && this.getStartDate() == other.getStartDate()
                && this.getTimeOfDayEnd() == other.getTimeOfDayEnd() && this.getTimeOfDayStart() == other.getTimeOfDayStart()
                && this.getStatus() == other.getStatus() && super.equals(other);
    }*/
}
