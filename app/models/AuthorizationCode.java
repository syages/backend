package models;

import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthResourceOwner;
import helpers.syages.SyagesConfig;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Entity
@SuppressWarnings("serial")
public class AuthorizationCode extends BaseModel implements OAuthAuthorizationCode {
	
	public static final Finder<UUID, AuthorizationCode> find = new Finder<UUID, AuthorizationCode>(UUID.class, AuthorizationCode.class);
	
	@NotNull
	@Column(unique = true)
	private String code;
	
	@NotNull
	@ManyToOne
	private Consumer consumer;
	
	@NotNull
	private Timestamp expiration;
	
	@NotNull
	@OneToOne
	private User loggedUser;
	
	private String state;
	
	public AuthorizationCode(String code, Consumer consumer, User loggedUser, String state)
	{
		this.code = code;
		this.consumer = consumer;
		this.expiration = Timestamp.from(Instant.now().plus(SyagesConfig.authorizationCodeExpiration, ChronoUnit.MINUTES));
		this.loggedUser = loggedUser;
		this.state = state;
	}

	public String getCode() {
		return this.code;
	}

	public Consumer getConsumer() {
		return this.consumer;
	}

	public Timestamp getExpiration() {
		return this.expiration;
	}

	public OAuthResourceOwner getLoggedOwner() {
		return this.loggedUser;
	}

	public String getState() {
		return this.state;
	}

	@Override
	public boolean isValid()
	{
		return Timestamp.from(Instant.now()).before(this.expiration);
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public void setExpiration(Timestamp expiration) {
		this.expiration = expiration;
	}

	public void setLoggedUser(User logged) {
		this.loggedUser = logged;
	}
	
	@Override
	public String stringRepresentation() {
		return this.code;
	}
	
}
