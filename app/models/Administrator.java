package models;

import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
@SuppressWarnings("serial")
public class Administrator extends Agent {

    public static Finder<UUID, Administrator> find = new Finder<UUID, Administrator>(UUID.class, Administrator.class);

    public Administrator(){
        //
    }

    public static Administrator create(String email, String firstName, String lastName, String password){
        User user = User.create(email, firstName, lastName, password, User.Usertype.Administrator);
        Administrator admin = new Administrator();
        admin.setUser(user);
        admin.save();

        return admin;
    }


    /**
     * Cette méthode permet de valider les annotations de contraintes.
     *
     * @return String : Json stringifié représentant les constraints non respectées
     */
    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        errors.putAll(super.validate());

        return errors;
    }

    public static Administrator findByUserId(UUID id){
        return find.where().eq("user_id", id).findUnique();
    }

}
