package models;


import com.avaje.ebean.annotation.CreatedTimestamp;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static helpers.util.Divers.constraintsViolationToMap;

@Entity
public class Report extends BaseModel {

    public enum ReportType{
        Bug,
        Message;
    }

    public enum ReportBugPriority{
        Critical,
        Normal,
        Minor;
    }

    public enum ReportStatus{
        Opened,
        Closed;
    }

    public static Finder<UUID,Report> find = new Finder<UUID, Report>(UUID.class, Report.class);

    @Enumerated(EnumType.STRING)
    private ReportBugPriority priority;

    @Lob
    private String commentary;

    @Constraints.Required
    private String context;

    @Constraints.Required
    private String controller;

    @CreatedTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp createdAt;

    private String route;

    @Enumerated(EnumType.STRING)
    private ReportStatus status = ReportStatus.Opened;

    @Enumerated(EnumType.STRING)
    @Constraints.Required
    private ReportType type;

    @ManyToOne
    private User user;

    private String userInfo;

    public Report() {
        //keep empty
    }

    public ReportBugPriority getPriority() {
        return priority;
    }

    public void setPriority(ReportBugPriority priority) {
        this.priority = priority;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public String getUserInfo() {
    	return this.userInfo;
    }
    
    public void setUserInfo(String userInfo) {
    	this.userInfo = userInfo;
    }

    public Map<String,String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(this.getType().equals(ReportType.Message)){
            this.setPriority(null);
        }
        return errors;
    }

    /*public boolean equals(Report other){
        return this.getCommentary() == other.getCommentary() && this.getContext() == other.getContext()
                && this.getController() == other.getController() && this.getCreatedAt() == other.getCreatedAt()
                && this.getPriority() == other.getPriority() && this.getRoute() == other.getRoute()
                && this.getStatus() == other.getStatus() && this.getType() == other.getType()
                && this.getUserInfo() == other.getUserInfo() && super.equals(other);
    }*/
}
