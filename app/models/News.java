package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import play.Logger;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.sql.Timestamp;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;


@Entity
@SuppressWarnings("serial")
public class News extends BaseModel {

    public static final Finder<UUID, News> find = new Finder<UUID, News>(UUID.class, News.class);


    @ManyToOne
    private User author;

    @Lob
    @Constraints.Required
    private String post;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedTimestamp
    private Timestamp posted;

    @Constraints.Required
    private String title;

    public News(){
        // keep empty
    }

    public User getAuthor() {
        return author;
    }


    public String getPost() {
        return post;
    }

    public Timestamp getPosted() {
        return posted;
    }

    public void setPosted(Timestamp posted) {
        this.posted = posted;
    }

    public String getTitle() {
        return title;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, String> validate(){
        Logger.info("User validate()");
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        if(author.getFlagType() == User.Usertype.Intern){
            errors.put("user", "intern don't have right to post a news.");
        }

        return errors;
    }

    /*public boolean equals(News other){
        return this.getAuthor().getId() == other.getAuthor().getId() && this.getPost() == other.getPost()
                && this.getPosted() == other.getPosted() && this.getTitle() == other.getTitle()
                && super.equals(other);
    }*/
}
