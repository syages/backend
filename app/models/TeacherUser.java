package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.util.*;

import static helpers.util.Divers.constraintsViolationToMap;


@Entity
@SuppressWarnings("serial")
public final class TeacherUser extends Agent {
    /*
    Teacher are defined by:
    - domain(s) of practice
    - course(s) they teach
    - diploma(s) they supervise
    - a phone number
    - an office adress
     */

	public static Finder<Long, TeacherUser> find = new Finder<Long, TeacherUser>(Long.class, TeacherUser.class);

    @ManyToMany
    @JoinTable(name = "Teacher_Domain")
    private List<Domain> domains = new ArrayList<>();

    private Boolean shareMail = true;

    @OneToMany(mappedBy = "teacher")
    private List<Lesson> lessons= new ArrayList<>();

    @OneToMany(mappedBy = "supervisor")
    private List<Diploma> supervisedDiploma = new ArrayList<>();

    public TeacherUser(){
        // keep empty
    }

    public TeacherUser(User user, String number, String office, List<Domain> domains){
        super(user, number, office);
        this.domains = domains;
    }

    public static TeacherUser create(String email, String firstName, String lastName,
                                     List<Domain> domains, String number, String office){
        return TeacherUser.create(email, firstName, lastName, "", number, office, domains);

    }

    public static TeacherUser create(String email, String firstName, String lastName, String password, String number, String office, List<Domain> domains){
        User user = User.create(email, firstName, lastName, password, User.Usertype.Teacher);
        TeacherUser teacher = new TeacherUser(user, number, office, domains);
        teacher.save();
        return teacher;
    }

    public Map<String, String> validate(){
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);

        errors.putAll(super.validate());


        return errors;
    }

    // Getter / Setter
    public List<Domain> getDomains() {
        return domains;
    }

    @JsonIgnore
    public List<Lesson> getLessons() {
        return lessons;
    }

    @JsonIgnore
    public List<Diploma> getSupervisedDiploma() {
        return supervisedDiploma;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public void setSupervisedDiploma(List<Diploma> supervisedDiploma) {
        this.supervisedDiploma = supervisedDiploma;
    }

    public Boolean isShareMail() {
        return shareMail;
    }

    public void setShareMail(Boolean shareMail) {
        this.shareMail = shareMail;
    }

    public static TeacherUser findByUserId(UUID id){
        return find.where().eq("user_id", id).findUnique();
    }

    @Override
    public void delete(){
        for (Domain d: domains){
            d.getTeachers().remove(this);
            d.update();
        }
        for(Diploma d: supervisedDiploma){
            d.setSupervisor(null);
            d.update();
        }
        for(Lesson l: lessons){
            l.setTeacher(null);
            l.update();
        }
        super.delete();
    }

    /*public boolean equals(TeacherUser other){
        return this.equalsDomains(other) && super.equals(other);
    }

    public boolean equalsDomains(TeacherUser other){
        List<UUID> id_this = new ArrayList<>();
        this.getDomains().forEach((d) -> id_this.add(d.getId()));
        List<UUID> id_other = new ArrayList<>();
        other.getDomains().forEach((d) -> id_other.add(d.getId()));
        return id_this.containsAll(id_other);
    }*/
}
