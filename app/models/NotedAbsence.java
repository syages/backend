package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import play.data.validation.Constraints;
import javax.persistence.*;
import javax.validation.ConstraintViolation;
import java.time.LocalDate;
import java.util.*;

import play.db.ebean.Model;

import static helpers.util.Divers.constraintsViolationToMap;
import static models.Absence.TimeOfDay;

@Entity
@SuppressWarnings({ "serial", "unused" })
public class NotedAbsence extends Absence {
    public enum AbsenceStatus {
        Justified("Justifiée"),
        Unjustified("Non justifiée");

        private String name = "";

        AbsenceStatus(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }
    }

    public static final Finder<UUID, NotedAbsence> find = new Finder<UUID, NotedAbsence>(UUID.class, NotedAbsence.class);

    @Transient
    private Date date;

    @ManyToOne
    @Constraints.Required
    private User observerUser;

    @Enumerated(EnumType.STRING)
    private AbsenceStatus status = AbsenceStatus.Unjustified;

    @Transient
    private TimeOfDay timeOfDay;

    @ManyToOne
    @Constraints.Required
    private Lecture lecture;

    public NotedAbsence() {
        // keep empty
    }

    public NotedAbsence(InternUser intern, Lecture lecture, User observer) {
        super(intern);
        this.lecture = lecture;
        this.observerUser = observer;

    }

    public Date getDate() {
        this.date = this.lecture.getDate();
        return date;
    }

    public User getObserverUser() {
        return observerUser;
    }

    public AbsenceStatus getStatus() {
        return status;
    }

    public TimeOfDay getTimeOfDay() {
        this.timeOfDay = this.lecture.getDate().toLocalDateTime().getHour() > 12 ? TimeOfDay.Afternoon : TimeOfDay.Morning;
        return timeOfDay;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setObserverUser(User observerUser) {
        this.observerUser = observerUser;
    }

    public void setStatus(AbsenceStatus status) {
        this.status = status;
    }

    public void setTimeOfDay(TimeOfDay timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    @JsonIgnore
    public Lecture getLecture() {
        return lecture;
    }

    @JsonProperty
    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }

    public Map<String,String> validate() {
        Set<ConstraintViolation<Model>> constraintViolations = validator.validate(this);
        Map<String, String> errors = constraintsViolationToMap(constraintViolations);
        errors.putAll(super.validate());

        return errors;
    }

    /*public boolean equals(NotedAbsence other){
        return this.getDate() == other.getDate() && this.getLecture().getId() == other.getLecture().getId()
                && this.getObserverUser().getId() == other.getObserverUser().getId() && this.getStatus() == other.getStatus()
                && this.getTimeOfDay() == other.getTimeOfDay() && super.equals(other);
    }*/
}
