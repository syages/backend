package helpers.oauth.play;

import com.aureo.oauth.provider.OAuthProvider;
import com.aureo.oauth.provider.OAuthScopeValidator;

public abstract class PlayProviderSingletonFactory {
	
	private static final OAuthProvider provider = new OAuthProvider(new PlayDAO(), new PlayGenerator(), new OAuthScopeValidator() {});
	
	public static OAuthProvider provider() {
		return provider;
	}

}
