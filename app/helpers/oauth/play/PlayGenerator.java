package helpers.oauth.play;

import helpers.crypto.GetRandom;

import java.util.ArrayList;
import java.util.Arrays;

import play.Logger;
import models.AccessToken;
import models.AuthorizationCode;
import models.Consumer;
import models.User;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthResourceOwner;
import com.aureo.oauth.common.OAuthResponse;
import com.aureo.oauth.common.OAuthWorkflow;
import com.aureo.oauth.provider.OAuthGenerator;

public class PlayGenerator implements OAuthGenerator {

	@Override
	public OAuthAuthorizationCode generateCode(OAuthConsumer consumer, OAuthResourceOwner owner, String state) {
		Logger.info("Generating code with consumer: " + consumer + "; owner: " + owner + "; and state: " + state);
		if (consumer instanceof Consumer && owner instanceof User)
			return new AuthorizationCode(GetRandom.hexString(24), (Consumer)consumer, (User)owner, state);
		else return null;
	}

	@Override
	public OAuthResponse generateOAuthLoginPage(OAuthConsumer consumer, String redirectUrl, boolean usesFragment, String state, String[] errors) {
		Logger.info("Generating login page with consumer: " + consumer + "; redirectUrl: "+ redirectUrl
				+ "; using fragment: " + usesFragment + "; with state: " + state + "; and " + (errors == null ? 0 : errors.length) + " error(s)");
		String body = views.html.authorization.render(consumer, redirectUrl, state,
				errors == null ? new ArrayList<String>() : Arrays.asList(errors), usesFragment).body();
		OAuthResponse response = new OAuthResponse(200, body, "text/html");
		return response;
	}

	@Override
	public OAuthAccessToken generateToken(OAuthConsumer consumer, OAuthResourceOwner owner, OAuthWorkflow workflow) {
		Logger.info("Generating token with consumer: " + consumer + "; owner: " + owner + "; and workflow: " + workflow);
		if (consumer instanceof Consumer && owner instanceof User)
			return new AccessToken(GetRandom.hexString(32), (Consumer)consumer, (User)owner, workflow);
		return null;
	}

}
