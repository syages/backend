package helpers.oauth.play;

import helpers.syages.SyagesConfig;

import javax.persistence.PersistenceException;

import models.AccessToken;
import models.AuthorizationCode;
import models.Consumer;
import models.User;
import play.Logger;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthResourceOwner;
import com.aureo.oauth.provider.OAuthDAO;
import com.avaje.ebean.ExpressionList;

public class PlayDAO implements OAuthDAO {
	
	@Override
	public void deleteCode(OAuthAuthorizationCode code)
	{
		if (code != null && code instanceof AuthorizationCode)
			((AuthorizationCode)code).delete();
	}
	
	@Override
	public void deleteToken(OAuthAccessToken token) {
		if (token != null && token instanceof AccessToken)
			((AccessToken)token).delete();
	}
	
	@Override
	public OAuthAuthorizationCode getCode(String code) {
		return AuthorizationCode.find.where().eq("code", code).findUnique();
	}
	
	@Override
	public OAuthConsumer getConsumer(OAuthAccessToken token) {
		return getToken(token.stringRepresentation()).getConsumer();
	}

	@Override
	public OAuthConsumer getConsumer(String key, String secret) {
		ExpressionList<Consumer> dbRequest = Consumer.find.where().eq("key", key);
		if (secret != null)
			dbRequest.eq("secret", secret);
		return dbRequest.findUnique();
	}

	@Override
	public OAuthResourceOwner getResourceOwner(String username, String password) {
		User u = User.find.where().eq("email", username).findUnique();
		if (u == null) return null;
		return User.find.where().eq("email", username).eq("password", SyagesConfig.passwordHashingMethod.hash(password, u.getPasswordSalt())).findUnique();
	}

	@Override
	public OAuthAccessToken getToken(String code) {
		return AccessToken.find.where().eq("code", code).findUnique();
	}

	@Override
	public boolean saveCode(OAuthAuthorizationCode code) {
		if (code instanceof AuthorizationCode)
		{
			try {
				((AuthorizationCode) code).save();
			} catch(PersistenceException exc) {
				Logger.error("PlayDAO] Unable to save authorization code: " + code);
				exc.printStackTrace();
				return false;
			}
			return true;
		}
		Logger.error("PlayDAO] Trying to save an authorization code which is not an instance of models.AuthorizationCode: " + code);
		return false;
	}

	@Override
	public boolean saveToken(OAuthAccessToken token) {
		if (token instanceof AccessToken)
		{
			try {
				((AccessToken) token).save();
			} catch(PersistenceException exc) {
				Logger.error("PlayDAO] Unable to save access token: "+ token);
				exc.printStackTrace();
				return false;
			}
			return true;
		}
		Logger.error("PlayDAO] Trying to save an access token which is not an instance of models.AccessToken: " + token);
		return false;
	}

}
