package helpers.oauth;

import models.User;
import play.mvc.Controller;
import play.mvc.Http;

import com.aureo.oauth.common.OAuthAccessToken;

public class SecuredController extends Controller {
	public static OAuthAccessToken oauthToken()
	{
		return (OAuthAccessToken)Http.Context.current().args.get("oauth_token");
	}
	
	public static User oauthUser()
	{
		return (User)oauthToken().getOwner();
	}
}
