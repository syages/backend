package helpers.crypto;

import java.util.Random;

public final class GetRandom {

    public static String hexString(int numchars) {
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, numchars);
    }

    private GetRandom() {
    }

}
