package helpers.syages;

@SuppressWarnings("serial")
public class SyagesException extends Exception {

	public SyagesException(String s) {
        super(s);
    }
}
