package helpers.syages;


import com.aureo.oauth.common.OAuthWorkflow;
import com.avaje.ebean.Ebean;
import models.*;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.Yaml;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class Global extends GlobalSettings {

    public void onStart(Application app){
        if(play.Play.application().isDev()){
            try {
                    cleanDatabase(app.getWrappedApplication());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            loadInitialData();
        }
		SyagesDatabase.init();
	}

    @Override
    public Promise<Result> onBadRequest(Http.RequestHeader header, String error){
        return Promise.<Result>pure(Results.badRequest(Json.parse("{\"error\":\"" + error + "\"}")));
    }

    public void onStop(Application app){
		SyagesDatabase.dbDumpScheduler.cancel();
	}
    
    private void cleanDatabase(play.api.Application app) throws IOException
    {
    	for (AccessToken m : AccessToken.find.all()) { m.delete(); }
    	for (Attachment m : Attachment.find.all()) { m.delete(); }
    	for (AuthorizationCode m : AuthorizationCode.find.all()) { m.delete(); }
    	for (Consumer m : Consumer.find.all()) { m.delete(); }
    	for (Course m : Course.find.all()) { m.delete(); }
    	for (DeclaredAbsence m : DeclaredAbsence.find.all()) { m.delete(); }
        for (Diploma m : Diploma.find.all()) { m.delete(); }
        for (FollowedDiploma m : FollowedDiploma.find.all()) { m.delete(); }
    	for (Lesson m : Lesson.find.all()) { m.delete(); }
    	for (LessonAppreciation m : LessonAppreciation.find.all()) { m.delete(); }
    	for (PeriodAppreciation m : PeriodAppreciation.find.all()) { m.delete(); }
    	for (NotedAbsence m : NotedAbsence.find.all()) { m.delete(); }
    	for (PasswordResetToken m : PasswordResetToken.find.all()) { m.delete(); }
        for(Administrator m: Administrator.find.all()){
            Logger.info("Deleting Administrator ...");
            m.delete();
        }
		for (StaffUser m : StaffUser.find.all()) {
			Logger.info("Deleting staff ...");
			m.delete();
		}
		for (TeacherUser m : TeacherUser.find.all()) {
			Logger.info("Deleting teacher ...");
			m.delete();
		}
		for (InternUser m : InternUser.find.all()) {
			Logger.info("Deleting intern ...");
			m.delete();
		}
    	for (Domain m : Domain.find.all()) {
			Logger.info("Deleting domain ...");
			m.delete();
		}
        for(President m: President.find.all()){
            m.delete();
        }
    }

	private void loadInitialData()
    {
		Logger.info("Loading initial data into the database");
		Ebean.save((Collection<?>) Yaml.load("domains_and_course.yml"));

        Logger.info("Creating Administrator ...");
        Administrator.create("administrateur@syages.ovh", "Administrateur", "test", "syages");
    	Administrator.create("bidon.aurelien@gmail.com", "Aurelien", "Bidon", "root");
		Administrator.create("timo.b35@gmail.com", "Timothée", "Barbot", "root");
		Administrator.create("romain.talleu@gmail.com", "Romain", "Talleu", "root");
        Logger.info("Creating Staff ...");
		StaffUser.create("staff@syages.fr", "Demo", "Staff", "root", "0102030405", "A101");
        Logger.info("Creating Intern ...");
		InternUser intern1 = InternUser.create("stagiaire1@syages.ovh", "Demo1", "Stagiaire", Date.valueOf(LocalDate.of(1993, 7, 22)));
		intern1.getUser().setPassword("root");
		intern1.save();
        InternUser intern2 = InternUser.create("stagiaire2@syages.ovh", "Demo2", "Stagiaire", Date.valueOf(LocalDate.of(1993, 6, 21)));
        intern2.getUser().setPassword("root");
        intern2.save();
        InternUser intern3 = InternUser.create("stagiare3@syages.ovh", "Demo3", "Stagiaire", Date.valueOf(LocalDate.of(1992, 6, 21)));
        intern3.getUser().setPassword("root");
        intern3.save();
        Logger.info("Creating Teacher ...");
        Domain d = Domain.find.all().get(0);
        List<Domain> dS = new ArrayList<>();
        dS.add(d);
        TeacherUser u3 = TeacherUser.create("enseignant@syages.ovh", "Enseignant", "test", "root", "0123456789", "T007", dS);

        Logger.info("Creating Diploma ...");
        Diploma diploma = new Diploma();
        diploma.setName("DAEU-A 2015");
        diploma.setPassMark(10L);
        diploma.setSupervisor(u3);

        Period p1 = new Period();
        p1.setOrdre(1);
        p1.setStartDate(Date.valueOf("2014-09-02"));
        p1.setEndDate(Date.valueOf("2014-12-05"));

        Period p2 = new Period();
        p2.setOrdre(2);
        p2.setStartDate(Date.valueOf("2014-12-08"));
        p2.setEndDate(Date.valueOf("2015-03-06"));

        Period p3 = new Period();
        p3.setOrdre(3);
        p3.setStartDate(Date.valueOf("2015-03-09"));
        p3.setEndDate(Date.valueOf("2015-06-12"));

        List<Period> periods = new ArrayList<>();
        periods.add(p1);
        periods.add(p2);
        periods.add(p3);

        diploma.setPeriods(periods);

        List<Lesson> lessons = new ArrayList<>();

        Lesson lesson1 = new Lesson();
        lesson1.setCourse(Course.findByName("Mathématiques"));
        lesson1.setCoefficient(10F);
        lesson1.setOptional(true);
        lesson1.setTeacher(u3);
        lessons.add(lesson1);

        Lesson lesson2 = new Lesson();
        lesson2.setCourse(Course.findByName("Français"));
        lesson2.setCoefficient(10F);
        lesson2.setOptional(false);
        lesson2.setTeacher(u3);
        lessons.add(lesson2);

        Lesson lesson3 = new Lesson();
        lesson3.setCourse(Course.findByName("Anglais"));
        lesson3.setCoefficient(10F);
        lesson3.setOptional(false);
        lesson3.setTeacher(u3);
        lessons.add(lesson3);

        Lesson lesson4 = new Lesson();
        lesson4.setCourse(Course.findByName("Histoire"));
        lesson4.setCoefficient(10F);
        lesson4.setOptional(true);
        lesson4.setTeacher(u3);
        lessons.add(lesson4);

        Lesson lesson5 = new Lesson();
        lesson5.setCourse(Course.findByName("Géographie"));
        lesson5.setCoefficient(10F);
        lesson5.setOptional(true);
        lesson5.setTeacher(u3);
        lessons.add(lesson5);

        Lesson lesson6 = new Lesson();
        lesson6.setCourse(Course.findByName("Philosophie"));
        lesson6.setCoefficient(10F);
        lesson6.setOptional(true);
        lesson6.setTeacher(u3);
        lessons.add(lesson6);

        Lesson lesson7 = new Lesson();
        lesson7.setCourse(Course.findByName("Sciences Sociales"));
        lesson7.setCoefficient(10F);
        lesson7.setOptional(true);
        lesson7.setTeacher(u3);
        lessons.add(lesson7);

        Lesson lesson8 = new Lesson();
        lesson8.setCourse(Course.findByName("Droit"));
        lesson8.setCoefficient(10F);
        lesson8.setOptional(true);
        lesson8.setTeacher(u3);
        lessons.add(lesson8);

        diploma.setLessons(lessons);

        diploma.save();

        FollowedDiploma fd1 = new FollowedDiploma();
        fd1.setDiploma(diploma);
        fd1.setIntern(intern1);
        fd1.setPaid(true);
        List<Lesson> opt1 = new ArrayList<>();
        opt1.add(lesson1);
        opt1.add(lesson7);
        fd1.setOptionalLessons(opt1);
        fd1.save();

        FollowedDiploma fd2 = new FollowedDiploma();
        fd2.setDiploma(diploma);
        fd2.setIntern(intern2);
        fd2.setPaid(true);
        List<Lesson> opt2 = new ArrayList<>();
        opt2.add(lesson4);
        opt2.add(lesson5);
        fd2.setOptionalLessons(opt2);
        fd2.save();

        FollowedDiploma fd3 = new FollowedDiploma();
        fd3.setDiploma(diploma);
        fd3.setIntern(intern3);
        fd3.setPaid(true);
        List<Lesson> opt3 = new ArrayList<>();
        opt3.add(lesson6);
        opt3.add(lesson8);
        fd3.setOptionalLessons(opt3);
        fd3.save();

        HashSet<String> urlSchemes = new HashSet<String>();
		Consumer c = new Consumer("Syages Website", "Syages", "localhost", "myClientKey", "myClientSecret", urlSchemes);
		c.addWorkflow(OAuthWorkflow.PASSWORD);
		c.addWorkflow(OAuthWorkflow.IMPLICIT);
		c.addWorkflow(OAuthWorkflow.SERVER_SIDE);
		c.save();
    }
    
}
