package helpers.syages;

import helpers.crypto.GetRandom;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import play.Logger;

public abstract class SyagesConfig {

    public static final int accessTokenExpirationFromServerSide = 0;
    public static final int accessTokenExpirationFromClientCred = 2;
    public static final int accessTokenExpirationFromPassword = 15;
    public static final int accessTokenExpirationFromImplicit = 30;
	public static final int authorizationCodeExpiration = 5;
    
    public static final String domain = play.Play.isProd() ? "syages.ovh" : "localhost:9000";
    public static final String route = play.Play.isProd() ? "https://syages.ovh" : "http://localhost:9000";
    public static final String smtpDomain = "smtp.gmail.com";
    public static final String smtpFrom = "Syages <projetsyages@gmail.com>";

    public static final String smtpPassword = "lemotdepasse";
    public static final int smtpPort = 465;
    public static final String smtpUsername = "projetsyages";
    
    public static final PasswordHashingMethod passwordHashingMethod = PasswordHashingMethod.SHA512;
    
    
    public enum PasswordHashingMethod {
    	/**
    	 * Hash passwords using the SHA-512 algorithm.<br/>
    	 * This algorithm offer sufficient protection with a relatively low CPU usage.<br/>
    	 * With an Intel i7 @ 4,1 GHz from 2013 this algorithm needs less than a second to hash.
    	 */
    	SHA512(new IHash() {
			@Override
			public String hash(String in, String salt) {
				final String finalInput = salt + in;
	            MessageDigest md = null;
	            try {
	                md = MessageDigest.getInstance("SHA-512");
	            } catch (NoSuchAlgorithmException e) {
	                e.printStackTrace();
	                Logger.error("The SHA-512 hashing algorithm implementation is unavailable on your machine. Please choose another hashing method in helpers.syages.SyagesConfig");
	                System.exit(-1);
	            }
	            md.update(finalInput.getBytes());
	            byte[] digest = md.digest();
	            
	            StringBuffer sb = new StringBuffer();
	            for (byte b : digest)
	                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
	            return sb.toString();
			}

			@Override
			public String gensalt() {
				return GetRandom.hexString(96);
			}
    	}),
    	/**
    	 * Hash passwords using the BCrypt algorithm.<br/>
    	 * This algorithm offer very high protection but is CPU intensive.<br/>
    	 * With an Intel i7 @ 4.1GHz from 2013 this algorithm needs an average of 5,71s to hash.
    	 */
    	BCrypt(new IHash() {
			@Override
			public String hash(String in, String salt) {
	    		return org.mindrot.jbcrypt.BCrypt.hashpw(in, salt);
			}

			@Override
			public String gensalt() {
	    		return org.mindrot.jbcrypt.BCrypt.gensalt(16); // max: 30, increase 
			}
    		
    	});
    	
    	private IHash hash;
    	
    	private PasswordHashingMethod(IHash hash) {
			this.hash = hash;
		}
    	
    	public String gensalt()
    	{
    		return this.hash.gensalt();
    	}
    	
    	public String hash(String in, String salt)
    	{
    		return this.hash.hash(in, salt);
    	}
    	
    	private interface IHash {
    		String hash(String in, String salt);
    		String gensalt();
    	}
    }

}
