package helpers.syages;


import akka.actor.Cancellable;
import helpers.mail.MailFactory;
import models.President;
import org.apache.commons.mail.*;
import play.Logger;
import play.Play;
import play.libs.Akka;

import java.io.IOException;
import java.time.*;
import java.time.chrono.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class SyagesDatabase {

    public static Cancellable dbDumpScheduler;

    private static String dumpEmail;
    private static LocalTime dumpTime;
    private static DayOfWeek dumpDay;
    private static Duration dumpRecurrence;
    private static String dumpPath;

    /**
     * Cette méthode met en place le Scheduler si les infos necessaires ont été transmises dans application.conf
     */
    public static void init(){
        // init values from application.conf
        Logger.info("Loading settings for database dump...");
        dumpEmail = Play.application().configuration().getString("dbdump.email");
        dumpTime = LocalTime.of(Play.application().configuration().getInt("dbdump.hour"), Play.application().configuration().getInt("dbdump.minutes"));
        dumpDay = DayOfWeek.of(Play.application().configuration().getInt("dbdump.day"));
        dumpRecurrence = Duration.ofDays(Play.application().configuration().getInt("dbdump.recurrence"));
        dumpPath = Play.application().configuration().getString("dbdump.path");

        if(dumpEmail != null && dumpTime != null && dumpDay != null && dumpRecurrence != null){
            startScheduler();
        }
    }

    /**
     * Cette méthode démarre le Scheduler une fois les informations nécessaires obtenues
     */
    public static void startScheduler(){
        dbDumpScheduler = Akka.system().scheduler().schedule(
                scala.concurrent.duration.Duration.create(nextExecutionInSeconds(), TimeUnit.SECONDS),
                scala.concurrent.duration.Duration.create(dumpRecurrence.toDays(), TimeUnit.DAYS),
                new Runnable() {
                    @Override
                    public void run() {
                        Logger.info("Envoi de la sauvegarde du " + LocalDate.now().toString() + " à l'email "+ dumpEmail);
                        dbDumpEmail();
                    }
                },
                Akka.system().dispatcher()
        );
    }

    /**
     * Cette méthode est appelé pour changer les informations du Scheduler et le redemarrer.
     */
    public static void loadSchedule(President president){
        // update each variables with input
        dumpEmail = president.getUser().getEmail();
        dumpTime = president.getHour() != null ? LocalTime.parse(president.getHour()): dumpTime;
        dumpDay = president.getDay()!= null ? DayOfWeek.of(president.getDay()) : dumpDay;
        dumpRecurrence = president.getRecurrence()!= null ? Duration.ofDays(president.getRecurrence()) : dumpRecurrence;
        // restart scheduler.
        dbDumpScheduler.cancel();
        startScheduler();
    }


    public static boolean dbDumpEmail(){
        // now date
        String date = LocalDate.now().toString();
        // path to save db dump to
        String path = dumpPath + date + ".sql";
        // receiver (appplication.conf)
        String emailTo = dumpEmail;
        // command to make the dump
        List<String> cmds = new ArrayList<String>();
        cmds.add("pg_dump");
        cmds.add("syages");
        cmds.add("-a");
        cmds.add("-f");
        // take path from application.conf
        cmds.add(path);



        ProcessBuilder process = new ProcessBuilder();
        try{
            process.command(cmds).start();
            process.redirectErrorStream(true);
            Logger.info("Dump effectué");
        }catch(IOException e){
            System.out.println(e.getMessage());
        }

        // send email with db as attachment
        HtmlEmail email = MailFactory.htmlEmail();
        EmailAttachment attachment = new EmailAttachment();
        attachment.setPath(path);
        System.out.println("path: "+ attachment.getPath());
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("Sauvegarde database " + date);
        attachment.setName(date+".sql");

        try{
            email.attach(attachment);
            email.addTo(emailTo);
            email.setSubject("Sauvegarde Syages");
            email.setHtmlMsg(views.html.mail.dbdump.render(date, dumpRecurrence.toDays()).body());
            email.setFrom("projetsyages@gmail.com", "Projet Syages");
            email.send();
            Logger.info("Email envoyé !");
        }catch (EmailException e){
            return false;
        }
        finally {
            return true;
        }
    }



    /**
     * Cette méthode trouve la date du prochain jour auquel on souhaite
     * effectue le dump de la base de données.
     *
     * @return LocalDate date et heure du
     */
    public static LocalDateTime nextExecution(){

        // on chercher la date du prochain dumpDay
        LocalDateTime date = LocalDateTime.now();
        date = date.with(dumpTime);
        date = date.with(dumpDay);
        // so if next is already past, we go to the next week
        LocalDateTime next = date.isBefore(ChronoLocalDateTime.from(LocalDateTime.now())) ? date.plusDays(7) : date;
        Logger.info(next.toString());
        return next;
    }

    public static long nextExecutionInSeconds(){
        // calcul du nombre de secondes entre maintenant et la date (jour,heure) selectionnée
        // la date provient de la fonction nextExecution.
        Long d = Duration.between(LocalDateTime.now(), nextExecution()).getSeconds();
        Logger.info("Secondes avant la prochaine sauvegarde: " + d.toString() + " seconds");
        return d;
    }
}
