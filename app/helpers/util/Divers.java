package helpers.util;

import com.avaje.ebean.text.PathProperties;
import com.avaje.ebean.text.json.JsonWriteOptions;
import controllers.api.routes;
import helpers.mail.MailFactory;
import helpers.syages.SyagesConfig;
import models.PasswordResetToken;
import models.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import play.db.ebean.Model;
import play.mvc.*;

import javax.validation.ConstraintViolation;
import java.util.*;
import java.util.function.BiConsumer;

public abstract class Divers {

    public static final String jsonInvalid = "{\"error\":\"Invalid JSON. %s required/invalid.\"}";
    public static final String idNotFound = "{\"error\": \"no %s found with id %s\"}";
    public static final String noMatch = "{\"error\":\"Id in URL doesn't match Id in JSON.\"}";
    public static final String unauthorized = "{\"error\": \"forbidden: you don't have rights to access this resource\"}";

    public static Map<String, String> constraintsViolationToMap(Set<ConstraintViolation<Model>> constraintSet){
        Map<String, String> resMap = new HashMap<String, String>();
        for(ConstraintViolation<?> c: constraintSet){
            resMap.put(c.getPropertyPath().toString(), c.getMessage());
        }
        return resMap;
    }

    public static Map<String, String> exceptionCatch(Exception e){
        Map<String, String> error = new HashMap<>();
        error.put("error", e.getMessage());
        return error;
    }

    public static JsonWriteOptions fieldsProperties(String fields){
        PathProperties properties = PathProperties.parse(fields);
        JsonWriteOptions writeOptions = new JsonWriteOptions();
        writeOptions.setValueAdapter(new JsonDateAdapter());
        writeOptions.setPathProperties(properties);

        return writeOptions;
    }

    public static String sortProperties(String s){
        String[] rules = s.split(",");
        List<String> result = new ArrayList<>();
        for(String r: rules){
            if(r.startsWith("-")){
                r= r.substring(1) + " DESC";
            }
            result.add(r);
        }
        return StringUtils.join(result, ",");
    }

    public static Integer formatLimit(Integer limit){
        return limit <= 0 ? 25 : limit > 100 ? 100 : limit;
    }

    public static Integer formatOffset(Integer offset){
        return offset < 0 ? 0 : offset;
    }

    public static String parametersString(Map<String, Object> parameters){
        String result = "";
        for(Map.Entry<String, Object> elem : parameters.entrySet()){
            if (elem.getValue() != null){
                result += (result.isEmpty() ? "" : "&") + elem.getKey() + "=" + elem.getValue();
            }

        }

       return "?" + result;
    }

    public static String routeHeader(Integer limit, Integer offset, String sort, Integer total_count, String path){

        String route = SyagesConfig.route;

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("limit", limit);
        parameters.put("offset", offset);
        parameters.put("sort", sort);

        Map<String, String> links = new HashMap<>();
        if (offset <(total_count/limit)){
            parameters.replace("offset", offset + 1);
            links.put("next", parametersString(parameters));
        }

        if(offset > 0){
            parameters.replace("offset", offset - 1);
            links.put("prev", parametersString(parameters));
        }

        parameters.replace("offset", 0);
        links.put("first", parametersString(parameters));

        parameters.replace("offset", total_count / limit);
        links.put("last", parametersString(parameters));


        String linksHeader = "";
        for (Map.Entry<String,String> e : links.entrySet()){
            if(!linksHeader.isEmpty()){
                linksHeader += ",";
            }
            linksHeader += "<" +  route + path + e.getValue() + ">; rel=\"" + e.getKey() + "\"";
        }

        return linksHeader + ";";
    }


    public static void confirmationEmail(User user, PasswordResetToken token){
        try {
            HtmlEmail email = MailFactory.htmlEmail();
            String url = "https://" + SyagesConfig.domain + "#iforgot/" + token.getKey();

            email.setFrom(SyagesConfig.smtpFrom);
            email.setSubject("Syages - Confirmation de votre inscription");
            email.setHtmlMsg(views.html.mail.confirm_signup.render(user, url).body());
            email.addTo(user.getEmail());
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
}
