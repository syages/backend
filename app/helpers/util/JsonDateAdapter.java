package helpers.util;

import com.avaje.ebean.text.json.JsonValueAdapter;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Duration;


public class JsonDateAdapter implements JsonValueAdapter {

    public String jsonFromDate(Date date){
        return String.valueOf(date.getTime());
    }

    public String jsonFromTimestamp(Timestamp timestamp){
        return String.valueOf(timestamp.getTime());
    }

    public Date jsonToDate(String jsonDate){
        return Date.valueOf(jsonDate);
    }

    public Timestamp jsonToTimestamp(String jsonDateTime){
        return Timestamp.valueOf(jsonDateTime);
    }
}
