package helpers.util;

import helpers.util.CORS.CORSActionInterceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

/**
 * Cette annotation Java intercepte les requêtes HTTP reçue par play! et
 * ajoute l'en-tête 
 */
@With(CORSActionInterceptor.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CORS {
	public static class CORSActionInterceptor extends Action<CORS>
	{
		@Override
		public F.Promise<Result> call(Http.Context ctx) throws Throwable {
			if (configuration.methods().trim().length() != 0) {

//				System.out.println("Ajout de l'en-tête Access-Control-Allow-Methods: " + configuration.methods());
				ctx.response().setHeader("Access-Control-Allow-Methods", configuration.methods());
			}
			if (configuration.origin().trim().length() != 0) {
//				System.out.println("Ajout de l'en-tête Access-Control-Allow-Origin: " + configuration.origin());
				ctx.response().setHeader("Access-Control-Allow-Origin", configuration.origin());
			}
			return delegate.call(ctx);
		}
    }
	
	String methods() default "";
	String origin() default "*";
}
