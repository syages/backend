package helpers.mail;

import helpers.syages.SyagesConfig;

import org.apache.commons.mail.*;

public abstract class MailFactory {

    public static HtmlEmail htmlEmail()
    {
        HtmlEmail email = new HtmlEmail();
        email.setHostName(SyagesConfig.smtpDomain);
        email.setSmtpPort(SyagesConfig.smtpPort);
        email.setAuthenticator(new DefaultAuthenticator(SyagesConfig.smtpUsername, SyagesConfig.smtpPassword));
        email.setSSLOnConnect(true);
        return email;
    }

}
