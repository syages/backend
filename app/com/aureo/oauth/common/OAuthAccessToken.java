package com.aureo.oauth.common;

import java.sql.Date;

/**
 * Cette interface spécifie l'ensemble des propriétées (getters)
 * et comportements communs aux jetons d'accès (access tokens).
 * @see OAuthAuthorizationCode
 */
public interface OAuthAccessToken {
	/**
	 * Retourne l'application tierce responsable de ce jeton d'accès.
	 * Une unique application peut être liée a un même jeton. C'est
	 * la seule application qui sera autorisée à utiliser ce jeton.
	 * @return l'application tierce responsable du jeton
	 * @see OAuthConsumer
	 */
	OAuthConsumer getConsumer();
	
	/**
	 * Retourne la date à laquelle le jeton aura expiré. Cette date peut
	 * être rallongée ou non en implémentant la méthode refresh.
	 * @return la date à laquelle le jeton ne sera plus utilisable
	 */
	Date getExpirationDate();
	
	/**
	 * Retourne le propriétaire des resources exploitées (l'utilisateur qui
	 * c'est connecté à l'application tierce). Un unique utilisateur peut
	 * être lié a un même jeton d'accès.
	 * @return le propriétaire des resources.
	 * @see OAuthResourceOwner
	 */
	OAuthResourceOwner getOwner();
	
	/**
	 * Vérifie la validité du jeton
	 * @return true si le jeton est valide, false sinon 
	 */
	boolean isValid();
	
	/**
	 * Rafraichit la date d'expiration d'un jeton d'accès lorsque celui-ci
	 * est encore valide. Libre à l'implémentation de choisir la durée
	 * de validité additionnelle (celle-ci peut être nulle).
	 */
	void refresh();
	
	/**
	 * Retourne la représentation littérale du jeton d'accès.
	 * Il est conseillé de choisir une représentation n'incluant pas de
	 * caractères spéciaux, majoritairement ceux utilisés comme délimiteurs
	 * dans les URLs (signe d'égalité, esperluette, dièse, ...). 
	 * @return la représentation littérale du jeton d'accès.
	 */
	String stringRepresentation();
}
