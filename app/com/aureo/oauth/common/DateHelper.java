package com.aureo.oauth.common;

import java.sql.Date;

import org.joda.time.Instant;

/**
 * Fonctions d'aide au développement relatives au temps.
 */
public abstract class DateHelper {
	/**
	 * Permet de récupérer les propriétées temporelles de l'instant présent.
	 * @return la Date courante.
	 */
	public static Date now()
	{
		return new Date(new Instant().getMillis());
	}
	
	/**
	 * Calcule le nombre de secondes qui séparent deux dates.
	 * @param d1 la premiere date
	 * @param d2 la deuxième date
	 * @return le nombre de secondes qui sépare d2 de d1.
	 */
	public static long secondsBetween(Date d1, Date d2)
	{
		if (d1 == null || d2 == null) return Long.MAX_VALUE;
		return (d2.getTime()-d1.getTime())/1000;
	}
	
	/**
	 * Calcule le nombre de secondes qui séparent une date de l'instant présent.
	 * @param d1 une date
	 * @return le nombre de secondes qui sépare l'instant présent de d1.
	 */
	public static long secondsFromNow(Date d1)
	{
		return secondsBetween(d1, now());
	}
}
