package com.aureo.oauth.common;

/**
 * Cette énumération définit les différents mécanismes
 * d'obtention d'un jeton d'accès par l'endpoint /token.
 */
public enum OAuthGrantType {
	authorization_code, client_credentials, password;
}
