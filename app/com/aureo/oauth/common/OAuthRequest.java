package com.aureo.oauth.common;

/**
 * Cette interface définit la liste des propriétés dont libOAuth à besoin
 * pour traiter une requete.
 */
public interface OAuthRequest {
	/**
	 * Récupère le contenu d'un en-tête par son nom.
	 * @param name le nom de l'en-tête
	 * @return le contenu de l'en-tête, ou null si celui-ci n'est pas représenté
	 */
	String getHeader(String name);
	/**
	 * Récupère le nom ou l'IPv4/v6 de l'hote, destinataire de la requète
	 */
	String getHost();
	/**
	 * Récupère le verbe HTTP de la requête
	 * @return le verbe HTTP
	 */
	String getMethod();
	/**
	 * Retourne le contenu d'un champ du corps de la requete par son nom.
	 * @param name le nom du champ body
	 * @return le tableau des valeurs associées a ce champ
	 */
	String[] getParsedBody(String name);
	/**
	 * Retourne le chemin de la requête
	 * @return le chemin de la requête
	 */
	String getPath();
	/**
	 * Retourne le contenu d'un champ de la query par son nom. 
	 * @param name le nom du champ query
	 * @return le contenu du champ, ou null si celui-ci n'est pas représenté
	 */
	String getQueryParameter(String name);
	/**
	 * Récupère l'IP de la machine effectuant la requete.
	 * @return L'IP au format v4 ou v6
	 */
	String getRemoteAddress();
	/**
	 * Récupère l'URI complète de la requête, incluant le schéma utilisé.
	 * @return l'URI complète de la requete
	 */
	String getUri();
}
