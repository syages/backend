package com.aureo.oauth.common;

/**
 * Cette interface définit la liste des propriétés communes
 * aux propriétaires des ressources (utilisateurs finaux)
 */
public interface OAuthResourceOwner {
	
}
