package com.aureo.oauth.common;

/**
 * Cette interface spécifie l'ensemble des propriétées (getters)
 * et comportements communs aux code d'autorisation (auth codes).<br/>
 * Avant de pouvoir accéder aux ressources, l'application tierce
 * doit transformer ce code d'autorisation en jeton d'accès.
 * @see OAuthAccessToken
 */
public interface OAuthAuthorizationCode {
	/**
	 * Retourne l'application tierce responsable de ce jeton
	 * d'autorisation. Une unique application peut être liée
	 * a un même jeton. C'est la seule application qui sera
	 * autorisée à transformer ce code en jeton d'accès.
	 * @return l'application tierce responsable du jeton
	 * @see OAuthConsumer
	 * @see OAuthAccessToken
	 */
	OAuthConsumer getConsumer();
	
	/**
	 * Retourne le propriétaire des resources exploitées (l'utilisateur qui
	 * c'est connecté à l'application tierce). Un unique utilisateur peut
	 * être lié a un même code d'autorisation.
	 * @return le propriétaire des resources.
	 * @see OAuthResourceOwner
	 */
	OAuthResourceOwner getLoggedOwner();

	/**
	 * Vérifie la validité du jeton
	 * @return true si le jeton est valide, false sinon 
	 */
	boolean isValid();
	
	/**
	 * Retourne la représentation littérale du code d'autorisation.
	 * Il est conseillé de choisir une représentation n'incluant pas de
	 * caractères spéciaux, majoritairement ceux utilisés comme délimiteurs
	 * dans les URLs (signe d'égalité, esperluette, dièse, ...).
	 * @return la représentation littérale du code d'autorisation.
	 */
	String stringRepresentation();
}
