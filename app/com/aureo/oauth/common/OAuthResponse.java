package com.aureo.oauth.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Cette classe contient les éléments d'une réponse HTTP.
 * libOauth laisse la responsabilité au développeur de convertir cette réponse dans
 * le format attendu par le framework web ou librarie utilisée.
 */
public class OAuthResponse {
	public static final String jsonContentType = "application/json";
	public static final String textContentType = "text/plain";

	/**
	 * Retourne une réponse contenant un Authorization Code
	 * @param code le code à délivrer
	 * @param state l'etat passé lors de la requete, null si aucun n'état n'a été transmi. 
	 * @return La réponse contenant le jeton d'autorisation au format JSON.
	 */
	public static OAuthResponse deliverCode(OAuthAuthorizationCode code, String state)
	{
		String response = "{\"code\": \""+ code.stringRepresentation() +"\"";
		if (state != null)
			response += ", \"state\": \""+ state +"\"";
		response += "}";
		
		return new OAuthResponse(202, response, jsonContentType);
	}

	/**
	 * Retourne une réponse contenant un Access Token
	 * @param token le jeton à délivrer 
	 * @return La réponse contenant le jeton d'accès au format JSON.
	 */
	public static OAuthResponse deliverToken(OAuthAccessToken token)
	{
		return new OAuthResponse(202, "{\"access_token\": \""+ token.stringRepresentation() + "\", \"token_type\": \"Bearer\", \"expires_in\": "+DateHelper.secondsFromNow(token.getExpirationDate())+"}", jsonContentType);
	}
	
	/**
	 * Retourne une réponse 500 (Internal Server error), lorsque le serveur n'a pas été en mesure de traiter la requête.
	 * @return Un code d'erreur 500 accompagné d'un message explicatif (JSON)
	 */
	public static OAuthResponse internalServerError() {
		return new OAuthResponse(500, "{\"error\": \"internal error: please try again\"}", jsonContentType);
	}
	
	/**
	 * Retourne une réponse 401, synonyme de mauvais identifiants de connexion. 
	 * @return Un code d'erreur 401 accompagné d'un message explicatif (JSON)
	 */
	public static OAuthResponse invalidCredentials()
	{
		return new OAuthResponse(401, "{\"error\": \"unauthorized: we have been unable to sign you in with the following credentials\"}", jsonContentType);
	}
	
	/**
	 * Retourne une réponse signalant la valeur incorrecte d'un paramètre.
	 * @param paramName le nom du paramètre incorrect
	 * @return Un code 400 ainsi que le nom du paramètre dont la valeur est incorrecte.
	 * @see #invalidParameter(String, Object[])
	 * @see #missingParameters()
	 */
	public static OAuthResponse invalidParameter(String paramName)
	{
		return invalidParameter(paramName, null);
	}
	
	/**
	 * Retourne une réponse signalant la valeur incorrecte d'un paramètre.
	 * @param paramName le nom du paramètre incorrect
	 * @param possibleValues un ensemble de valeurs acceptées, ou null
	 * @return Un code 400 ainsi que le nom du paramètre dont la valeur est incorrecte.
	 * @see #invalidParameter(String)
	 * @see #missingParameters()
	 */
	public static OAuthResponse invalidParameter(String paramName, Object[] possibleValues)
	{
		String content = "{\"error\": \"bad request: parameter " + paramName + " is invalid.";
		if (possibleValues != null && possibleValues.length >= 0)
		{
			if (possibleValues.length == 1)
				content += " acceptable value is: ";
			else 
				content += " acceptable values are: ";
			
			for (int i = 0; i < possibleValues.length; i++)
			{
				content += (i == 0 ? "" : ", ") + possibleValues[i];
			}
		}
		content += "\"}";
		
		return new OAuthResponse(400, content, jsonContentType);
	}

	/**
	 * Retourne une réponse signalant l'oubli de paramètres obligatoire lors de la requète.
	 * @return Un code d'erreur 400 ainsi qu'un message explicatif
	 * @see #invalidParameter(String)
	 * @see #invalidParameter(String, Object[])
	 */
	public static OAuthResponse missingParameters()
	{
		return new OAuthResponse(400, "{\"error\": \"bad request: missing one or more parameters\"}", jsonContentType);
	}
	
	private String body;
	
	private int code;
	
	private String contentType;
	
	private Map<String, String> headers;

	/**
	 * Construit une réponse HTTP à partir du code de retour
	 * @param code le code HTTP de retour
	 */
	public OAuthResponse(int code) {
		this(code, "");
	}

	/**
	 * Construit une réponse HTTP à partir du code de retour et du corps
	 * @param code le code HTTP de retour
	 * @param body le corps de la réponse
	 */
	public OAuthResponse(int code, String body) {
		this(code, body, "text/plain");
	}
	
	/**
	 * Construit une réponse HTTP à partir du code de retour et du corps
	 * @param code le code HTTP de retour
	 * @param body le corps de la réponse
	 * @param contentType le type MIME de retour de la requete
	 */
	public OAuthResponse(int code, String body, String contentType) {
		this(code, body, contentType, null);
	}
	
	/**
	 * Construit une réponse HTTP à partir du code de retour, du corps et des en-têtes
	 * @param code le code HTTP de retour
	 * @param body le corps de la réponse
	 * @param contentType le type MIME de retour de la requete
	 * @param headers les en-têtes personnalisés et leur valeur
	 */
	public OAuthResponse(int code, String body, String contentType, Map<String, String> headers) {
		this.setBody(body);
		this.setCode(code);
		this.setContentType(contentType);
		this.setHeaders(headers != null ? headers : new HashMap<String, String>());
	}
	
	/**
	 * Retourne le corps de la réponse
	 * @return le corps de la réponse
	 * @see #setBody(String)
	 */
	public String body()
	{
		return this.body;
	}
	
	/**
	 * Retourne le code HTTP de retour
	 * @return le code HTTP de la réponse
	 * @see #setCode(int)
	 */
	public int code()
	{
		return this.code;
	}
	
	/**
	 * Retourne le type MIME de la réponse
	 * @return le type MIME de la réponse
	 * @see #setContentType(String)
	 */
	public String contentType()
	{
		return this.contentType;
	}
	
	/**
	 * Retourne les en-têtes de la réponse
	 * @return les en-têtes de la réponse
	 * @see #setHeader(String, String)
	 * @see #setHeaders(Map)
	 */
	public Map<String, String> headers()
	{
		return this.headers;
	}
	
	/**
	 * Affecte un nouveau corps à la réponse
	 * @param newValue le nouveau corps de la réponse
	 * @see #body()
	 */
	public void setBody(String newValue)
	{
		this.body = newValue == null ? "" : newValue;
	}

	/**
	 * Affecte un nouveau code de retour à la réponse
	 * @param newValue le nouveau code de la réponse
	 * @see #code()
	 */
	public void setCode(int newValue)
	{
		this.code = newValue;
	}

	/**
	 * Affecte un nouveau type MIME à la réponse
	 * @param newValue le nouveau type MIME de la réponse
	 * @see #contentType()
	 */
	public void setContentType(String newValue)
	{
		this.contentType = newValue == null ? "" : newValue;
	}

	/**
	 * Ajoute un en-tête à la réponse
	 * @param key le nom de l'en-tête
	 * @param value la valeur associée à cet en-tête
	 * @see #setHeaders(Map)
	 * @see #headers()
	 */
	public void setHeader(String key, String value)
	{
		this.headers.put(key, value);
	}

	/**
	 * Remplace les en-têtes de la réponse
	 * @param newValue le nouvel ensemble d'en-tête de la réponse
	 * @see #setHeader(String, String)
	 * @see #headers()
	 */
	public void setHeaders(Map<String, String> newValue)
	{
		this.headers = newValue == null ? new HashMap<String,String>() : newValue;
	}
}
