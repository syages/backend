package com.aureo.oauth.common;

/**
 * Cette énumération définit les différents mécanismes
 * d'obtention d'un jeton d'accès peut importe l'endpoint.
 * @see OAuthConsumer#getWorkflows()
 */
public enum OAuthWorkflow {
	CLIENT_CREDENTIALS, IMPLICIT, NATIVE, PASSWORD, SERVER_SIDE;
}
