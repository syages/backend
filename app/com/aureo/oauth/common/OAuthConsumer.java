package com.aureo.oauth.common;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Cette interface définit les propriétés (getters) communes aux
 * applications tierces (consumers).
 */
public interface OAuthConsumer {
	/**
	 * Teste l'égalité de deux applications tierces
	 * @return true si les deux applications sont les mêmes, false sinon
	 */
	boolean equals(OAuthConsumer consumer);
	
	/**
	 * Retourne l'auteur de l'application tierce. Cettre propriétée
	 * est utilisée lorsqu'un ecran d'autorisation est présenté à
	 * un utilisateur, afin d'identifier l'application pour laquelle il
	 * s'apprête à donner un droit d'accès.
	 * @return le nom de l'auteur de l'application tierce.
	 */
	String getAuthor();
	
	/**
	 * Retourne la liste des différents schémas d'URI propres à
	 * l'application tierce, notamment pour les applications mobiles.<br/>
	 * Elle permet d'éviter le vol de jeton par des applications
	 * tierces. Par mesure de sécurité, toute requête ne se
	 * conformant pas à l'un de ces schémas (http et https exclus)
	 * sera ignorée.
	 * @return un set de schémas utilisés par cette application.
	 */
	default Set<String> getCustomSchemesList() {
		return new HashSet<String>();
	};
	
	/**
	 * Retourne la liste des différents mecanismes d'obtentions de jeton
	 * autorisés pour cette application tierce.<br/>
	 * Elle permet de restreindre les applications à un ou plusieurs
	 * mecanisme(s) afin de limiter les failles de sécurité.
	 * Par mesure de sécurité, toute requête ne se conformant pas à l'un
	 * de ces mécanismes sera ignorée.
	 * @return un set de mécanismes autorisés pour cette application.
	 */
	EnumSet<OAuthWorkflow> getWorkflows();
	
	/**
	 * Retourne le nom d'hôte de l'application tierce.
	 * Il permet d'éviter le vol de jeton par des applications
	 * tierces. Par mesure de sécurité, toute requête ne se
	 * conformant pas à ce nom d'hôte sera ignorée. 
	 * @return le nom d'hôte de l'application tierce
	 */
	String getHost();
	
	/**
	 * Retourne la clé de l'application tierce.
	 * @return la clé de l'application tierce, sous forme littérale.
	 */
	String getKey();
	
	/**
	 * Retourne le nom de l'application tierce. Cettre propriétée
	 * est utilisée lorsqu'un ecran d'autorisation est présenté à
	 * un utilisateur, afin d'identifier l'application pour laquelle il
	 * s'apprête à donner un droit d'accès.
	 * @return le nom de l'auteur de l'application tierce.
	 */
	String getName();
	
	/**
	 * Retourne le secret de l'application tierce.
	 * Ce secret doit rester confidentiel.
	 * @return le secret de l'application tierce, sous forme littérale.
	 */
	String getSecret();
}
