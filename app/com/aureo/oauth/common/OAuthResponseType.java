package com.aureo.oauth.common;

/**
 * Cette énumération définit les différents mécanismes
 * d'obtention d'un jeton par l'endpoint /auth.
 */
public enum OAuthResponseType {
	code, token;
}
