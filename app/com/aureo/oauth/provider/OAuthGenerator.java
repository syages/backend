package com.aureo.oauth.provider;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthResourceOwner;
import com.aureo.oauth.common.OAuthResponse;
import com.aureo.oauth.common.OAuthWorkflow;

/**
 * Cette insterface définit les méthodes utilisées par libOAuth pour
 * la génération de codes, jeton et pages d'autorisation. 
 */
public interface OAuthGenerator {
	/**
	 * Génère un code d'autorisation
	 * @param consumer l'application tierce demandant l'accès
	 * @param owner le propriétaire des ressources donnant l'accès
	 * @param state l'état de l'application tierce (optionel) 
	 * @return un code d'accès
	 */
	OAuthAuthorizationCode generateCode(OAuthConsumer consumer, OAuthResourceOwner owner, String state);
	
	/**
	 * Génère une page web d'autorisation (/auth endpoint)
	 * @param consumer l'application tierce demandant l'accès
	 * @param redirectUrl l'URL de redirection
	 * @param usesFragment précise si l'URL de redirection (lors d'un refus d'accès) doit utiliser un fragments ou une query
	 * @param state (optionnel, peut être null) l'état de l'application tierce
	 * @return une réponse HTTP encapsulée dans un OAuthResponse
	 * @see OAuthResponse
	 * @see #generateOAuthLoginPage(OAuthConsumer, String, boolean, String, String[])
	 */
	default OAuthResponse generateOAuthLoginPage(OAuthConsumer consumer, String redirectUrl, boolean usesFragment, String state)
	{
		return generateOAuthLoginPage(consumer, redirectUrl, usesFragment, state, null);
	}
	
	/**
	 * Génère une page web d'autorisation (/auth endpoint)
	 * @param consumer l'application tierce demandant l'accès
	 * @param redirectUrl l'URL de redirection
	 * @param usesFragment précise si l'URL de redirection (lors d'un refus d'accès) doit utiliser un fragments ou une query
	 * @param state (optionnel, peut être null) l'état de l'application tierce
	 * @param errors (optionnel, peut être null) un ensemble d'erreurs si l'authentification à déjà échoué.
	 * @return une réponse HTTP encapsulée dans un OAuthResponse
	 * @see OAuthResponse
	 */
	OAuthResponse generateOAuthLoginPage(OAuthConsumer consumer, String redirectUrl, boolean usesFragment, String state, String[] errors);
	
	/**
	 * Génère un jeton d'accès
	 * @param consumer l'application tierce demandant l'accès
	 * @param owner le propriétaire des ressources donnant l'accès
	 * @param type le workflow de connexion utilisé 
	 * @return un jeton d'accès
	 */
	OAuthAccessToken generateToken(OAuthConsumer consumer, OAuthResourceOwner owner, OAuthWorkflow workflow);
}
