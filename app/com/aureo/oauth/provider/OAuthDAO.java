package com.aureo.oauth.provider;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthResourceOwner;

/**
 * Cette interface définit les méthodes d'accès aux données dont
 * libOAuth à besoin si vous souhaitez implémenter un Provider. 
 */
public interface OAuthDAO {
	/**
	 * Détruit un code d'autorisation
	 * @param code le code d'autorisation à détruire
	 */
	void deleteCode(OAuthAuthorizationCode code);

	/**
	 * Détruit un jeton d'ccès
	 * @param token le jeton d'accès à détruire
	 */
	void deleteToken(OAuthAccessToken token);
	
	/**
	 * Récupère un code d'autorisation par sa représentation litérale.
	 * @param code la représentation litérale du code
	 * @return le code d'autorisation correspondant, ou null si aucun ne correspond
	 */
	OAuthAuthorizationCode getCode(String code);
	
	/**
	 * Récupère l'application tierce responsable de la délivrance d'un jeton d'accès
	 * @param token le jeton d'accès
	 * @return les informations de l'application tierce ayant demandée ce jeton d'accès
	 */
	OAuthConsumer getConsumer(OAuthAccessToken token);
	
	/**
	 * Récupère l'application tierce à partir de sa clé.
	 * @param key la clé de l'application tierce
	 * @param secret (optionel) le secret de l'application tierce
	 * @return Si le secret est présent retourne l'application associée au couple (clée, secret),
	 * si le secret est égal à null, retourne l'application associée à la clée.
	 */
	OAuthConsumer getConsumer(String key, String secret);
	
	/**
	 * Récupère un utilisateur par son username et son password
	 * @param username le username de l'utilisateur
	 * @param password le password de l'utilisateur
	 * @return l'utilisateur si les identifiants sont valides, null sinon
	 */
	OAuthResourceOwner getResourceOwner(String username, String password);
	
	/**
	 * Récupère un jeton d'accès par sa représentation litérale
	 * @param token la représentation litérale du jeton
	 * @return le jeton d'accès correspondant, ou null si aucun ne correspond
	 */
	OAuthAccessToken getToken(String token);
	
	/**
	 * Vérifie la présence d'un jeton d'accès par sa représentation litérale
	 * @param token la représentation litérale du jeton
	 * @return true si le jeton existe dans la base de donnée, false sinon
	 */
	default boolean hasToken(String token)
	{
		return getToken(token) != null;
	}

	/**
	 * Sauvegarde un code d'autorisation
	 * @param code le code d'autorisation à sauvegarder
	 * @return true si la sauvegarde c'est déroulée avec succès, false sinon
	 */
	boolean saveCode(OAuthAuthorizationCode code);
	
	/**
	 * Sauvegarde un jeton d'accès
	 * @param token le jeton d'accès à sauvegarder
	 * @return true si la sauvegarde c'est déroulée avec succès, false sinon
	 */
	boolean saveToken(OAuthAccessToken token);
}
