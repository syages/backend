package com.aureo.oauth.provider;

import java.util.HashSet;
import java.util.Set;

import com.aureo.oauth.common.OAuthConsumer;

/**
 * Permet de vérifier l'existance des scopes demandés ainsi que le
 * bridage de certains scopes pour certaines applications tierces.
 * @category Unimplemented 
 */
public interface OAuthScopeValidator {
	/**
	 * Retourne la liste des scopes valide pour l'API implémentée
	 * @return la liste des scopes valide pour l'API implémentée
	 */
	default Set<String> acceptedScopesList()
	{
		return new HashSet<String>();
	}
	
	/**
	 * Vérifie la validité d'un scope
	 * @param scope le scope à tester
	 * @return true si le scope existe, false sinon
	 */
	default boolean isScopeValid(String scope)
	{
		return scope.toLowerCase().equals("all") || acceptedScopesList().contains(scope.toLowerCase());
	}
	
	/**
	 * Vérifie la validité d'un scope pour une application tierce donnée
	 * @param scope le scope à tester
	 * @param client l'application tierce pour laquelle le scope doit être autorisé.
	 * @return true si le scope existe, false sinon
	 */
	default boolean isScopeValidForClient(String scope, OAuthConsumer client)
	{
		return isScopeValid(scope);
	}
}
