package com.aureo.oauth.provider;

import java.net.URI;
import java.util.Arrays;
import java.util.Set;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthAuthorizationCode;
import com.aureo.oauth.common.OAuthConsumer;
import com.aureo.oauth.common.OAuthGrantType;
import com.aureo.oauth.common.OAuthRequest;
import com.aureo.oauth.common.OAuthResourceOwner;
import com.aureo.oauth.common.OAuthResponse;
import com.aureo.oauth.common.OAuthResponseType;
import com.aureo.oauth.common.OAuthWorkflow;
import com.google.common.base.Joiner;

/**
 * Cette classe est responsable du traitement des requêtes 
 * sécurisées par le protocole OAuth. 
 */
public class OAuthProvider {
	
	private static boolean isParamSet(OAuthRequest req, String name)
	{
		String value = parameter(req, name);
		if (value == null) return false;
		if (value.trim().length() == 0) return false;
		return true;
	}
	private static String parameter(OAuthRequest req, String name)
	{
		switch(req.getMethod().toUpperCase())
		{
		case "GET":
			return req.getHeader(name) != null ? req.getHeader(name) : req.getQueryParameter(name);
		default:
			return req.getHeader(name) != null ? req.getHeader(name) : Joiner.on(";").join(req.getParsedBody(name));
		}
	}
	private OAuthDAO dao;
	
	private OAuthGenerator generator;
	
	private OAuthScopeValidator scopeValidator;
	
	/**
	 * Constructeur
	 * @param dao le DAO à utiliser
	 * @param generator le générateur à utiliser
	 * @param scopeValidator le validateur de scope à utiliser
	 */
	public OAuthProvider(OAuthDAO dao, OAuthGenerator generator, OAuthScopeValidator scopeValidator)
	{
		this.dao = dao;
		this.generator = generator;
		this.scopeValidator = scopeValidator;
	}
	
	/**
	 * Endpoint GET /auth
	 * @param req la requete HTTP
	 * @return Se référer à la documentation de l'endpoint GET /auth
	 */
	public OAuthResponse authenticate(OAuthRequest req)
	{
		if(isParamSet(req, "response_type") == false || isParamSet(req, "client_id") == false
				|| isParamSet(req, "redirect_uri") == false)
			return OAuthResponse.missingParameters();
		
		// Retrieve response type
		OAuthResponseType type;
		try {
			type = OAuthResponseType.valueOf(parameter(req, "response_type").toLowerCase());			
		} catch(IllegalArgumentException e)
		{
			return OAuthResponse.invalidParameter("response_type", OAuthResponseType.values());
		}
		
		OAuthConsumer client = this.dao.getConsumer(parameter(req, "client_id"), null);
		if (client == null || client.getWorkflows().contains(type.equals(OAuthResponseType.token) ? OAuthWorkflow.IMPLICIT : OAuthWorkflow.SERVER_SIDE) == false)
			return OAuthResponse.invalidParameter("client_id");

		Set<String> authorizedSchemes = client.getCustomSchemesList();
		authorizedSchemes.add("http");
		authorizedSchemes.add("https");
		
		URI uri = URI.create(parameter(req, "redirect_uri"));
		if (authorizedSchemes.contains(uri.getScheme()) == false) {
			return OAuthResponse.invalidParameter("redirect_uri", authorizedSchemes.toArray());
		}
		
		if ((uri.getScheme().toLowerCase().equals("http") || uri.getScheme().toLowerCase().equals("https"))
				&& uri.getHost().toLowerCase().contains(client.getHost()) == false)
		{
			return OAuthResponse.invalidParameter("redirect_uri", Arrays.asList(client.getHost()).toArray());
		}
		
		return this.generator.generateOAuthLoginPage(client, parameter(req, "redirect_uri"), type.equals(OAuthResponseType.token), parameter(req, "state"));
	}
	
	/**
	 * Endpoint POST /auth
	 * @param req la requete HTTP
	 * @return Se référer à la documentation de l'endpoint POST /auth
	 */
	public OAuthResponse authenticateCallback(OAuthRequest req)
	{
		if(isParamSet(req, "response_type") == false || isParamSet(req, "client_id") == false
				|| isParamSet(req, "redirect_uri") == false)
			return OAuthResponse.missingParameters();
		
		// Retrieve response type
		OAuthResponseType type;
		try {
			type = OAuthResponseType.valueOf(parameter(req, "response_type").toLowerCase());			
		} catch(IllegalArgumentException e)
		{
			return OAuthResponse.invalidParameter("response_type", OAuthResponseType.values());
		}
		
		OAuthConsumer client = this.dao.getConsumer(parameter(req, "client_id"), null);
		if (client == null || client.getWorkflows().contains(type.equals(OAuthResponseType.token) ? OAuthWorkflow.IMPLICIT : OAuthWorkflow.SERVER_SIDE) == false)
			return OAuthResponse.invalidParameter("client_id");
		
		if (isParamSet(req, "email") == false || isParamSet(req, "password") == false)
			return this.generator.generateOAuthLoginPage(client, parameter(req, "redirect_uri"), type.equals(OAuthResponseType.token),
					parameter(req, "state"), Arrays.asList("missing_parameters").toArray(new String[0]));
		
		Set<String> authorizedSchemes = client.getCustomSchemesList();
		authorizedSchemes.add("http");
		authorizedSchemes.add("https");
		
		URI uri = URI.create(parameter(req, "redirect_uri"));
		if (authorizedSchemes.contains(uri.getScheme()) == false) {
			return OAuthResponse.invalidParameter("redirect_uri", authorizedSchemes.toArray());
		}
		
		if ((uri.getScheme().toLowerCase().equals("http") || uri.getScheme().toLowerCase().equals("https"))
				&& uri.getHost().toLowerCase().contains(client.getHost()) == false)
		{
			return OAuthResponse.invalidParameter("redirect_uri", Arrays.asList(client.getHost()).toArray());
		}
		
		OAuthResourceOwner owner = this.dao.getResourceOwner(parameter(req, "email"), parameter(req, "password"));
		if (owner == null) {
			return this.generator.generateOAuthLoginPage(client, parameter(req, "redirect_uri"), type.equals(OAuthResponseType.token),
					parameter(req, "state"), Arrays.asList("invalid_credentials").toArray(new String[0]));
		}
		
		String state = parameter(req, "state");
		if (state.trim().length() == 0)
			state = null;
		
		OAuthResponse response = new OAuthResponse(302);
		if (type.equals(OAuthResponseType.code)) {
			OAuthAuthorizationCode code = this.generator.generateCode(client, owner, state);
			if (this.dao.saveCode(code) == false) {
				return OAuthResponse.internalServerError();
			}
			response.setHeader("Location", parameter(req, "redirect_uri")+"?code=" + code.stringRepresentation());
		}
		else {
			OAuthAccessToken token= this.generator.generateToken(client, owner, OAuthWorkflow.IMPLICIT);
			if (this.dao.saveToken(token) == false) {
				return OAuthResponse.internalServerError();
			}
			response.setHeader("Location", parameter(req, "redirect_uri")+"#token=" + token.stringRepresentation());
		}
		return response;
	}
	
	/**
	 * Getter du DAO associé au provider
	 * @return le DAO associé au provider
	 * @see OAuthDAO
	 */
	public OAuthDAO getDAO()
	{
		return this.dao;
	}

	
	/**
	 * Getter du générateur associé au provider
	 * @return le générateur associé au provider
	 * @see OAuthGenerator
	 */
	public OAuthGenerator getGenerator()
	{
		return this.generator;
	}

	
	/**
	 * Getter du validateur de scope associé au provider
	 * @return le validateur de scope associé au provider
	 * @see OAuthScopeValidator
	 */
	public OAuthScopeValidator getScopeValidator()
	{
		return this.scopeValidator;
	}
	
	/**
	 * Endpoint GET /token
	 * @param req la requete HTTP
	 * @return Se référer à la documentation de l'endpoint GET /token
	 */
	public OAuthResponse token(OAuthRequest req)
	{
		if (!isParamSet(req, "grant_type"))
			return OAuthResponse.missingParameters();
		
		OAuthGrantType type;
		try {
			type = OAuthGrantType.valueOf(parameter(req, "grant_type"));
		} catch(IllegalArgumentException e)
		{
			return OAuthResponse.invalidParameter("grant_type", OAuthGrantType.values());
		}
		
		switch (type)
		{
			case authorization_code:
			{
				return tokenWithCode(req);
			}
			case password:
			{
				return tokenWithPassword(req);
			}
			default:
			{
				return new OAuthResponse(501, "TODO");
			}
		}
	}
	
	/**
	 * Permet de valider l'authentification présente dans une requête
	 * @param req la requete à valider
	 * @see com.aureo.oauth.play.OAuthRequire
	 */
	public String validateRequestToken(OAuthRequest req)
	{
		String authHeader = req.getHeader("Authorization");
		if (authHeader == null) return null;
		
		String[] parts = authHeader.split(" ");
		if (parts.length != 2) return null;
		if (parts[0].equals("Bearer") == false) return null;

        OAuthAccessToken t = getDAO().getToken(parts[1]);
		if (t != null && t.isValid())
		{
			return parts[1];
		} else return null;
	}
	
	private OAuthResponse tokenWithCode(OAuthRequest req)
	{
		if (!isParamSet(req, "client_id") || !isParamSet(req, "client_secret") || !isParamSet(req, "code") || !isParamSet(req, "redirect_uri"))
		{
			return OAuthResponse.missingParameters();
		}
		
		OAuthConsumer client = this.dao.getConsumer(parameter(req, "client_id"), parameter(req, "client_secret"));
		if (client == null || client.getWorkflows().contains(OAuthWorkflow.SERVER_SIDE) == false)
			return OAuthResponse.invalidParameter("client");
		
		OAuthAuthorizationCode code = this.dao.getCode(parameter(req, "code"));
		if (code == null)
			return OAuthResponse.invalidParameter("code");
		
		if (code.isValid() == false || client.equals(code.getConsumer()) == false)
		{
			this.dao.deleteCode(code);
			return OAuthResponse.invalidParameter("code");
		}
		
		OAuthAccessToken token = this.generator.generateToken(code.getConsumer(), code.getLoggedOwner(), OAuthWorkflow.SERVER_SIDE);
		this.dao.deleteCode(code);
		if (this.dao.saveToken(token) == false)
			return OAuthResponse.internalServerError();
		
		return OAuthResponse.deliverToken(token);
	}
	
	private OAuthResponse tokenWithPassword(OAuthRequest req)
	{
		if (!isParamSet(req, "client_id") || !isParamSet(req, "password") || !isParamSet(req, "username"))
		{
			return OAuthResponse.missingParameters();
		}
		
		OAuthConsumer client = getDAO().getConsumer(parameter(req, "client_id"), null);
		if (client == null || client.getWorkflows().contains(OAuthWorkflow.PASSWORD) == false)
			return OAuthResponse.invalidParameter("client_id");
		
		OAuthResourceOwner owner = getDAO().getResourceOwner(parameter(req, "username"), parameter(req, "password"));
		if (owner == null)
			return OAuthResponse.invalidCredentials();
		
		OAuthAccessToken token;
		do {
			token = getGenerator().generateToken(client, owner, OAuthWorkflow.PASSWORD);
		} while (getDAO().hasToken(token.stringRepresentation()));
		
		if (getDAO().saveToken(token))
		{
			return OAuthResponse.deliverToken(token);			
		} else {
			return OAuthResponse.internalServerError();
		}
	}
	
}
