package com.aureo.oauth.play;

import java.util.Map;

import com.aureo.oauth.common.OAuthRequest;

/**
 * Wrapper de requêtes HTTP pour play! Framework v2.x
 * @see OAuthRequest
 * @see <a href="https://www.playframework.com">play! Framework</a>
 * @category Unstable
 */
public class RequestWrapper implements OAuthRequest {
	
	/**
	 * Retourne la requête encapsulée à partir du contexte courant.
	 * @return la requête du contexte courant encapsulée.
	 */
	public static RequestWrapper fromContext()
	{
		return new RequestWrapper(play.mvc.Http.Context.current().request());
	}
	
	private play.mvc.Http.Request wrapped;
	
	/**
	 * Construit un OAuthRequest
	 * @param req la requête play! v2.x
	 */
	public RequestWrapper(play.mvc.Http.Request req)
	{
		this.wrapped = req;
	}
	
	@Override
	public String getHeader(String name) {
		return this.wrapped.getHeader(name);
	}

	@Override
	public String getHost() {
		return this.wrapped.host();
	}

	@Override
	public String getMethod() {
		return this.wrapped.method();
	}
	
	@Override
	public String[] getParsedBody(String name)
	{
		Map<String, String[]> body = this.wrapped.body().asFormUrlEncoded();
		return body.get(name);
	}

	@Override
	public String getPath() {
		return this.wrapped.path();
	}

	@Override
	public String getQueryParameter(String name) {
		return this.wrapped.getQueryString(name);
	}
	
	@Override
	public String getRemoteAddress() {
		return this.wrapped.remoteAddress();
	}

	@Override
	public String getUri() {
		return this.wrapped.uri();
	}

}
