package com.aureo.oauth.play;

import com.aureo.oauth.common.OAuthResponse;

/**
 * Converti les réponses OAuth afin de les utiliser avec
 * play! Framework v2.x.
 * @see OAuthResponse
 * @see <a href="https://www.playframework.com">play! Framework</a>
 * @category Unstable
 */
public class ResponseConverter {
	
	/**
	 * Converti une réponse de libOAuth vers une réponse HTTP
	 * au format play! Framework v2.x.
	 * @param res la réponse HTTP de libOAuth
	 * @return une réponse HTTP pour play!
	 * @see <a href="https://www.playframework.com">play! Framework</a>
	 */
	public static play.mvc.Result playResponse(OAuthResponse res)
	{
		if (res.code() == 302)
		{
			return play.mvc.Results.redirect(res.headers().get("Location"));
		}
		
		play.mvc.Results.Status response = play.mvc.Results.status(res.code(), res.body());
		response = response.as(res.contentType());
		return response;
	}
}
