package com.aureo.oauth.play;

import static play.libs.F.Promise.pure;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.Arrays;

import play.Play;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.With;

import com.aureo.oauth.common.OAuthAccessToken;
import com.aureo.oauth.common.OAuthRequest;
import com.aureo.oauth.common.OAuthResourceOwner;
import com.aureo.oauth.play.OAuthRequire.OAuthRequireActionInterceptor;
import com.aureo.oauth.provider.OAuthProvider;

/**
 * Cette annotation Java intercepte les requêtes HTTP reçue par play! et
 * vérifie la présence d'une authentification valide (jeton d'accès en en-tête).<br/><br/>
 * Veuillez vous référer à la documentation de libOAuth afin de procéder à
 * la configuration de votre projet avant d'utiliser cette annotation.
 */
@With(OAuthRequireActionInterceptor.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OAuthRequire {
	public static class OAuthRequireActionInterceptor extends Action<OAuthRequire>
	{
		private static OAuthProvider getProviderInstance() throws Exception
		{
			String className = Play.application().configuration().getString("oauth.bridge", null);
			if (className == null)
			{
				throw new Exception("com.aureo.oauth.play: oauth.bridge isn't set in your conf/application.conf file");
			}
			
			Class<?> bridgeClass;
			try {
				bridgeClass = Play.application().classloader().loadClass(className);
			} catch(ClassNotFoundException e)
			{
				throw new Exception("com.aureo.oauth.play: the specified oauth.bridge class can't be loaded");
			}
			
			try {
				Method meth = bridgeClass.getMethod("provider");
				Object ret = meth.invoke(null);
				if ((ret instanceof OAuthProvider) == false)
				{
					throw new Exception("com.aureo.oauth.play: the provider() method should return an OAuthProvider instance");
				}
				return (OAuthProvider)ret;
			} catch(NoSuchMethodException e)
			{
				throw new Exception("com.aureo.oauth.play: the bridge class should have a \"public static provider()\" method");
			} catch(SecurityException e)
			{
				throw new Exception("com.aureo.oauth.play: the bridge class should have a \"public static provider()\" method");
			}
		}
		
		@Override
		public F.Promise<Result> call(Http.Context ctx) throws Throwable {
            OAuthRequest req = new RequestWrapper(ctx.request());

            String token = getProviderInstance().validateRequestToken(req);
            if (token == null && configuration.bypass() == false) {
                return pure(Results.status(configuration.onFailureStatusCode(), configuration.onFailure()));
            }
            OAuthAccessToken tokenInst = getProviderInstance().getDAO().getToken(token);
            if (configuration.bypass() == false && configuration.refreshToken() == true) {
                tokenInst.refresh();
                getProviderInstance().getDAO().saveToken(tokenInst);
            }

            boolean accessible = configuration.accessibleBy().length == 0;
            for (int i = 0; i < configuration.accessibleBy().length; i++) {
                if (configuration.bypass() == false && configuration.accessibleBy()[i].isAssignableFrom(tokenInst.getOwner().getClass()) == true) {
                    accessible = true;
                    break;
                }
            }
            if (configuration.bypass() == false && configuration.accessibleByMethod().equals("") == false) {
                OAuthResourceOwner owner = tokenInst.getOwner();
                Method method = owner.getClass().getMethod(configuration.accessibleByMethod());
                Object value = method.invoke(owner);
                accessible &= Arrays.asList(configuration.accessibleByValues()).contains(configuration.accessibleByUsingToString() ? value.toString() : value);
            }

            if (accessible == false && configuration.bypass() == false)
                return pure(Results.status(configuration.onFailureStatusCode(), configuration.onFailure()));

            ctx.args.put("oauth_token", tokenInst);
            return delegate.call(ctx);
        }
    }
	
	Class<?>[] accessibleBy() default {Object.class};
	String accessibleByMethod() default "getFlagType";
	String[] accessibleByValues() default {};
	boolean accessibleByUsingToString() default true;
    boolean bypass() default false;
	String onFailure() default "{\"error\": \"forbidden: you don't have rights to access this resource\"}";
	int onFailureStatusCode() default 401;
	boolean refreshToken() default true;
	String scope() default "all";
	boolean token() default true;
}
